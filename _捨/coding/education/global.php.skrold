<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <title>グローバル教育｜駒込中学・高等学校</title>
    <meta content="駒込中学・高等学校" name="description">
    <meta http-equiv="Pragma" content="no-store">
    <meta http-equiv="Cache-Control" content="no-store">
    <meta http-equiv="Expires" content="0">
		<meta name="format-detection" content="telephone=no">

    <link href="../images/common/favicon.ico" rel="shortcut icon">
    <link href="../images/common/favicon.ico" rel="apple-touch-icon">
    <link href="../css/common.css" rel="stylesheet" type="text/css">
<?php //    <link href="../css/sub.css" rel="stylesheet" type="text/css"> ?>
    <link href="../css/sub2.css" rel="stylesheet" type="text/css">
    <link href="../owl/owl.carousel.min.css" rel="stylesheet" type="text/css">

  </head>

  <body id="global">
    <?php include '../header.php'; ?>

    <main>
      <section class="mv header-title">
				<h1>グローバル教育</h1>
        <p>Global</p>
      </section>

			<section class="article-main">
				<article>
					<h2>世界という教室でしか、学べないことがある</h2>
          <p>真の国際人とは国内外どこにいようとも、様々な国の文化・価値観を受け容れながら、<br class="pc">自分の考えや自国の文化を正しく発信できる人材。<br>駒込学園では、オセアニアを含めた、日本及びアジア・太平洋地域での教育を強化。<br>経済成長の著しいアジアでの経験を糧に、日本を世界に発信できる人材を育みます。</p>
					<div class="sub-menu">
            <a href="#program">校内プログラム</a>
            <a href="#language">語学研修</a>
            <a href="#abroad">留学制度</a>
          </div>
				</article>
			</section>

<section class="article-main" id="program">
  <div class="wrp">
  <div>
    <p class="see fr"><a href="#movie1" class="modal">Watch the Movie</a></p>
    <h3>校内プログラム<span>英語4技能をバランスよく修得する。</span></h3>
  </div>
  <div class="two-column">

		<div class="box">
      <div class="text">
  			<h4>英会話授業</h4>
  			<p>一人でも多くの生徒のアクティビティを観察できるように、1クラスを2分割して、ネイティブの先生とのティームティーチングを行っています。<br>4技能の中の特にスピーキングとリスニングの力を伸ばすことを主軸に、キーフレーズやキーセンテンスを使いながら少しずつレベルアップしていきます。<br>そして一年間の総決算として英語スピーチコンテストにチャレンジします。</p>
      </div>

			<div class="image">
      	<img src="../images/education/global1.jpg">
			</div>
		</div>

    <div class="box">
      <div class="text">
  			<h4>イマージョン講座<span>オールイングリッシュ授業</span></h4>
  			<p>英語のみで、「数学」「理科」「社会」「音楽」「地理」などの教科を学習する講座です。<br>ネイティブの教員と本校教員とのティームティーチングで、週に1回、25人程度の少人数で実施しています。<br>英語を勉強の対象としてではなく、手段として使いながら他教科を学んでいく画期的な「教室内留学」と呼べる講座です。</p>
      </div>
      <div class="image">
        <img src="../images/education/global2.jpg">
      </div>
		</div>

    <div class="box">
      <div class="text">
  			<h4>オンライン英会話教室<span>中2・3・高校生（希望者）</span></h4>
  			<p>高校生は毎日30分好きな時間に、中学2・3年生は放課後週1回30分間、海外にいるネイティブとマンツーマンオンライン英会話授業を行います。<br>自分のレベルに合わせてスタートが切れるのがマンツーマンのメリットです。<br>講師も厳しい試験をパスした指導経験豊かで親切な先生ばかりなので、楽しい会話を通じて無理なく英語4技能の力を伸ばすことができます。<br>会話力の向上だけでなく、英検二次対策としても役に立つ教室です。</p>
      </div>
      <div class="image">
        <img src="../images/education/global3.jpg">
      </div>
		</div>

    <div class="box">
      <div class="text">
  			<h4>英語スピーチコンテスト<span>中1～3</span></h4>
  			<p>日常の英語学習の成果を発表する場として、年度末にスピーチコンテストを行います。<br>全学年そのレベルにあったテーマについて<br class="pc">英語でスピーチし「発信できる力」を養います。<br>コンテスト優秀者は、他の私学との対抗戦となる<br class="pc">レシテーションコンテスト（本校が会場校）に参加します。</p>
      </div>
      <div class="image">
        <img src="../images/education/global4.jpg">
      </div>
		</div>

  </div>
</div>

  <div class="voice">
    <h2>Student&rsquo;s Voice</h2>
    <div class="two-column">
      <div class="box">
        <div class="text">
          <h3>イマージョン講座を受講して</h3>
          <p>私はイマージョン講座がとても好きです。もちろん授業は全て英語で行われるため、正直難しいところもあります。でも私はこの講座を毎週とても楽しみにしています。その理由はいくつかありますが、一番の理由としてはALTの先生の授業がとても楽しいということです。クイズなどを通していろいろなことを学べるほか、たくさん友達ができたりもします。私はこの講座を受講して、英語がいままでよりももっと好きになりました。自分の英語への知識や関心が増えていくことに満足感や達成感を覚えます。イマージョン講座を受講して本当によかったです。</p>
        </div>
 <?php /*        <div class="detail">
          <p class="name">中学二年生<br>○○　○○</p>
        </div> */?>
        <div class="image">
          <img src="../images/education/voice1.png">
        </div>
      </div>
    </div>
  </div>
</section>



<section class="pagetop">
  <p>Page Top</p>
  <span class="arrow"></span>
</section>




<section class="article-main" id="language">
  <div class="wrp">
    <div>
      <p class="see fr"><a href="#movie2" class="modal">Watch the Movie</a></p>
      <h3>語学研修<span>高度な語学力と国際感覚をともに養う。</span></h3>
    </div>
  <div class="two-column">

		<div class="box">
      <div class="text">
  			<h4>セブ島語学研修<span>中3（希望者）</span></h4>
  			<p>フィリピンのリゾート地セブ島にて英語語学研修を行う、短期留学制度です。<br>中学時代の節目として、自分の英語力とコミュニケーション能力を一気に伸ばすことが狙いです。<br>また、担当の講師一人ひとりがとても親切でわかりやすく指導してくれるため、<br class="pc">一日50分×8コマ連続の1対1オールイングリッシュ授業であっても楽しく積極的に取り組んでいます。<br>様々な国籍の人と楽しく触れ合うプログラムもあります。
  			</p>
      </div>
      <div class="image">
        <img src="../images/education/global6.jpg">
      </div>
		</div>

    <div class="box">
      <div class="text">
  			<h4>英語キャンプ<span>高1（希望者）</span></h4>
<?php //  			<p>2泊3日期間中、英語のみで生活するという完全な英語漬けの環境下で、<br class="pc">演劇、スピーチ、日本文化の紹介、自然探索、宝探しゲーム、カラオケ大会、<br class="pc">イギリスの伝統的なスポーツ（クリケットやラグビー）、<br class="pc">料理実習（ニュージーランドのクッキー作り）などを行いながら、<br class="pc">生活の一部として使える英語を身につけていきます。</p> ?>
        <p>夏休み8月下旬に国内で行われる2泊3日の英語漬け合宿です。バスに乗ってからは日本語禁止。<br>英語で海外生活を疑似体験します。完全な英語漬け環境において、自然体験、料理実習、スポーツ（ラグビーやクリケット）などを行って生活の一部として使える英語を身につけていきます。</p>
 <?php //       <p>国内で行われる2泊3日の英語漬け合宿です。バスに乗ってからは日本語禁止。<br>英語で海外生活を疑似体験します。完全な英語漬け環境において、自然体験・料理実習・スポーツ（ラグビーやクリケット）などを行って生活の一部として使える英語を身につけていきます。夏休み8月下旬に行います。</p> ?>
      </div>
      <div class="image">
        <img src="../images/education/global7.jpg">
      </div>
		</div>

    <div class="box">
      <div class="text">
  			<h4 class="ls-0.07">シンガポール･マレーシア修学旅行<span>高2</span></h4>
  			<p>都市国家シンガポールとマレーシアの2カ国を訪れます。<br>現地校との英語を駆使した文化交流、マレーシアにある伝統的な農村（カンポン）でのホームビジットなど、アジアの別の顔を知る旅です。また、彼らとの<br>コミュニケーションを通じて、同じアジアに暮らす私たちの生活を客観的に見つめ、グローバル社会の一員として何ができるかを考える旅でもあります。</p>
      </div>
      <div class="image">
        <img src="../images/education/global8.jpg">
      </div>
		</div>

    <div class="box">
      <div class="text">
  			<h4 class="ls-0.07">ハワイセミナー短期語学研修<span>中1～3、高1・2<?php //（希望者）?></span></h4>
  			<p>ハワイで、「生きた英語」を学び、現地の独特な文化に触れる体験をします。<br>ハワイ大学のドミトリー（学生寮）に泊まり、ハワイ大学大学院生による英会話の語学レッスンがレベル別に行われます。また市内での「英語お買い物体験」、スーパーマーケットでの食材見学、そして日本とハワイの歴史の勉強として真珠湾を見学するなど、ハワイに来たからこそできるセミナーを提供します。</p>
      </div>
      <div class="image">
        <img src="../images/education/global9.jpg">
      </div>
		</div>

    <div class="box">
      <div class="text">
  			<h4>マルタ島語学研修<span>高1・2（希望者）</span></h4>
<?php //  			<p>地中海に浮かぶマルタ島も語学研修で注目を浴びている地です。<br>ここは英語を母国語としないヨーロッパ各地から英語を学びに学生が集まります。<br>複数の異なった言語を話す者同士が英語という共通語でコミュニケーションをとるグローバルな経験をすることができます。<br>そのために、この研修も様々なレベルに対応することができますが、この語学研修に参加する生徒には敢えて一定の語学力（英検準2級以上のレベル）を求めています。</p> ?>
        <p>地中海に浮かぶマルタ島（マルタ共和国）で英語を学ぶ上級プログラム。治安が良くヨーロッパ各地からの留学生が多いので、多国籍の学生と交流できます。午後はアクティビティが用意され、地中海文化を肌で感じ取ることができます。高校1・2年で英検準2級以上の力がある生徒を対象とします。</p>
      </div>
      <div class="image">
        <img src="../images/education/global10.jpg">
      </div>
		</div>


  </div>
</div>
  <div class="voice">
    <h2>Student&rsquo;s Voice</h2>
    <div class="two-column">
      <div class="box">
        <div class="text">
          <h3>セブ島　生徒感想</h3>
          <p>3月24日から30日という春休みに、高校進学を控えた中学3年生がセブ島へ語学研修に行きました。異国で一週間の研修というと少し長く感じますが、実際に過ごしてみるとあっという間でとても短いものでした。研修では、マンツーマン授業が6コマ、そして希望性のナイトレッスンもあり最大で10コマの授業を取ることができました。マンツーマン授業では、初日に受けた英語4技能のテストの結果を参考に、自分に必要な分野を実力に応じたグレードで受けました。研修中は授業も休み時間も英語漬けで、日本では経験できないような充実した外国語の学習をすることができました。また、モンゴルや台湾の方とも仲良くなることができて嬉しかったです。終始楽しく充実していて、本当に行って良かったと思いました!</p>
        </div>
  <?php /*       <div class="detail">
          <p class="name"></p>
        </div> */?>
        <div class="image">
          <img src="../images/education/voice2.png">
        </div>
      </div>
    </div>
  </div>
</div>
</section>


<section class="pagetop">
  <p>Page Top</p>
  <span class="arrow"></span>
</section>


<section class="article-main" id="abroad">
  <div class="wrp">
  <div>
    <h3 class="grow2">留学制度<span>高度な語学力と国際感覚をともに養う。</span></h3>
  </div>
  <div class="two-column">

		<div class="box">
      <div class="text">
  			<h4 class="ls-0.1">オーストラリア・ニュージーランド<br>海外中・長期留学制度(3ヵ月・1年)<span>高1.1月～<?php //（希望者）?></span></h4>
  			<p>オーストラリア･ニュージーランドへの高校留学制度があります。<br>中期は3ヵ月、長期は1年間です。ホームステイでは、家族の一員として<br class="pc">異文化での生活を経験します。学校生活では、現地の生徒と一緒に<br class="pc">授業を受けます。なお、留学中の単位の取得を認めていますので、<br class="pc">帰国後は同学年で進級・卒業が可能です。</p>
      </div>
      <div class="image">
        <img src="../images/education/global11.jpg">
      </div>
		</div>
  </div>

  </div>
  <div class="voice">
    <h2>Student&rsquo;s Voice</h2>
    <div class="two-column">
      <div class="box">
        <div class="text">
          <h3>留学</h3>
          <p>両親の勧めで留学に参加したため、はじめの動機としては消極的なところもありました。<br>現地では、文法や単語を正しく使って上手に話そうとする僕を、ホストファミリーや学校のクラスメイトたちが一生懸命聞こうとしてくれ、じっと待ってくれたのです。その時に、上手に話すことよりも、話そうとする姿勢が大事だと気付かされ、自分から積極的に行動することが何より大切だと学びました。日本に戻ってからは、英語だけでなくプレゼンテーションも苦手ではなくなりました。自分自身を変えるきっかけを与えてくれたことを今はとても感謝しています。</p>
        </div>
 <?php /*       <div class="detail">
          <p class="name"></p>
        </div> */?>
        <div class="image">
          <img src="../images/education/voice3.png">
        </div>
      </div>
    </div>
  </div>
</section>



<section class="pagetop">
  <p>Page Top</p>
  <span class="arrow"></span>
</section>

    </main>

    <?php include '../footer.php'; ?>
<?php /*    <script src="../js/jquery.min.js"></script>
    <script src="../js/flexibility.js"></script>
    <script src="../js/common2.js" type="text/javascript"></script>*/?>
    <div class="modal_contents">
      <!- modal表示内容2（離れた場所に置くバージョン） -->
      <div id="movie1"><video src="../images/education/global_movie1.mp4"></video><p>イマージョン講座</p></div>
      <div id="movie2"><video src="../images/education/global_movie2.mp4"></video><p>セブ島語学研修2019</p></div>
      <!- /modal表示内容2（離れた場所に置くバージョン） -->
    </div>

  </body>
</html>
