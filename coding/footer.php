<footer>
	<div class="container">
		<div class="info">
			<div class="left">
				<h3>学校法人　駒込学園　<br class="sp">駒込中学校・高等学校</h3>
				<p>〒113-0022　東京都文京区千駄木5-6-25</p>
				<p>TEL 03-3828-4141　FAX 03-3824-5685</p>
			</div>
			<div class="right">
				<a href="https://www.facebook.com/komagomegakuen/" class="fb">SNS:<img src="/images/common/fb.png"></a>
				<a href="<?=$root;?>/students-guardian/emergency.php"><div>在校生・保護者の方へ</div></a>
				<a href="<?=$root;?>/graduates/office.php#graduates"><div>卒業生の方へ</div></a>
			</div>
		</div>
		<div class="sitemap">
			<div class="container">
				<div class="box">
					<h4>学園の紹介</h4>
					<a href="<?=$root;?>/introduction/philosophy.php">教育理念・教育方針</a>
					<a href="<?=$root;?>/introduction/message.php">校長挨拶</a>
					<a href="<?=$root;?>/introduction/history.php">学校沿革</a>
				</div>

				<div class="box">
					<h4>駒込の教育</h4>
					<a href="<?=$root;?>/education/global.php">グローバル教育</a>
					<a href="<?=$root;?>/education/ict.php">ICT教育</a>
					<a href="<?=$root;?>/education/support.php">学習支援</a>
					<a href="<?=$root;?>/education/stem.php">STEM教育</a>
					<a href="<?=$root;?>/education/human.php">人間教育</a>
				</div>

				<div class="box">
					<h4>中学校</h4>
					<a href="<?=$root;?>/junior-high-school/program.php">6ヵ年一貫教育プログラム</a>
				</div>

				<div class="box">
					<h4>高等学校</h4>
					<a href="<?=$root;?>/high-school/course.php">3コース制</a>
				</div>

				<div class="box">
					<h4>学校生活</h4>
					<a href="<?=$root;?>/school-life/uniform.php">制服紹介</a>
					<a href="<?=$root;?>/school-life/facility.php">施設紹介</a>
					<a href="<?=$root;?>/school-life/event.php">学校行事</a>
					<a href="<?=$root;?>/school-life/club.php">クラブ紹介</a>
				</div>

				<div class="box">
					<h4>進路情報</h4>
					<a href="<?=$root;?>/course/data.php">進路データ</a>
					<a href="<?=$root;?>/course/succeed.php">卒業生の活躍（準備中）</a>
				</div>

				<div class="box">
					<h4>受験生の方へ</h4>
					<a href="<?=$root;?>/examinee/guidance.php">入試説明会案内</a>
					<a href="<?=$root; ?>/examinee/j-application.php">中学募集要項</a>
					<a href="<?=$root; ?>/examinee/h-application.php">高校募集要項</a>
					<a href="<?=$root;?>/examinee/internet.php">インターネット出願</a>
					<a href="<?=$root;?>/examinee/movie.php">KOMAGOME CHANNEL</a>
					<a href="<?=$root;?>/examinee/kakomon.php">過去問題紹介（準備中）</a>
				</div>
			</div>
		</div>

		<div class="links">
			<ul>
				<a href="<?=$root;?>/graduates/office.php"><li>事務室</li></a>
				<a href="<?=$root;?>/students-guardian/dispensary.php"><li>保健室</li></a>
				<a href="<?=$root;?>/employment//"><li>採用情報</li></a>
<?php /*				<a href="<?=$root;?>/students-guardian/access.php"><li>アクセス</li></a>*/?>
				<a href="<?=$root;?>/access.php"><li>アクセス</li>
			</ul>
			<ul>
<?php /*				<a href="<?=$root;?>"><li>プライバシーポリシー</li></a>*/?>
				<a href="<?=$root;?>/sitemap.php""><li>サイトマップ</li></a>
			</ul>
		</div>
	</div>
	<div class="colophon">
		<p>©KOMAGOME GAKUEN All Rights Reserved.</p>
  </div>
		<a href="#top" class="anchor">
			<div class="gotop"><span></span></div>
		</a>
</footer>
<script src="/js/jquery.min.js"></script>
<script src="/js/flexibility.js"></script>
<script src="/js/common2.js" type="text/javascript"></script>

<link href="../css/uwagaki.css" rel="stylesheet" type="text/css">
