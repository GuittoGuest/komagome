<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <title>駒込中学校・高等学校</title>
    <meta content="駒込中学校・高等学校" name="description">
    <meta http-equiv="Pragma" content="no-store">
    <meta http-equiv="Cache-Control" content="no-store">
    <meta http-equiv="Expires" content="0">
		<meta name="format-detection" content="telephone=no">

    <link href="/images/common/favicon.ico" rel="shortcut icon">
    <link href="/images/common/favicon.ico" rel="apple-touch-icon">
    <link href="/css/common.css" rel="stylesheet" type="text/css">
<?php //    <link href="/css/sub.css" rel="stylesheet" type="text/css"> ?>
    <link href="/css/sub2.css" rel="stylesheet" type="text/css">

  </head>
<?php /*	<style>
		strong{
			font-weight: bold;
	    display: block;
	    font-size: 16px;
	    padding-bottom: 5px;
			padding-top: 15px;
		}
		em{
	    font-weight: bold;
		}
	</style> */?>
  <body id="access">
    <?php include './header.php'; ?>

    <main>
    <section class="mv header-title">
				<h1>アクセス</h1>
        <p>Access</p>
      </section>

			<section class="article-main">
        <article>
					<h3>学校法人 駒込学園 駒込中学校・高等学校</h3>
					<p>〒113-0022　東京都文京区千駄木5-6-25<br>
					TEL：03-3828-4141　FAX：03-3824-5685</p>
        </article>
      </section>

      <section class="article-main">
          <div class="two-column">
          <h3>電車</h3>
          <div class="two-column">
            <div class="box">
              <div class="flex_pc column2 text">
                <div>
                  <h4>東京メトロ</h4>
                  <ul class="disc">
                    <li>南北線<br> 『本駒込駅』 下車・徒歩５分（１番出口もしくはエレベーターをご利用下さい）</li>
                    <li>千代田線<br>『千駄木駅』 下車・徒歩７分（１番出口をご利用下さい）</li>
                  </dl>
                </div>
                <div>
                  <h4>都営地下鉄</h4>
                  <ul class="disc">
                    <li>三田線<br>『白山駅』 下車・徒歩７分（Ａ３出口をご利用下さい）</li>
                  </dl>
                </div>
              </div>
            </div>

            <h3>バス</h4>
            <div class="box">
              <div class="text">
                  <ul class="disc">
                    <li>都バス<br>草63－駒込千駄木町（駒込学園前下車）</li>
                  </ul>
              </div>
            </div>
          </div>

      <div class="map-iamges">
        <img src="../images/access/komagome_map2.jpg" alt=""/>
        <img src="../images/access/komagome_map.jpg" alt=""/>
        <div class="gmap"><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3239.052514709459!2d139.7549444512099!3d35.724926980086934!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x22e9cd17247a5be7!2z5a2m5qCh5rOV5Lq66aeS6L685a2m5ZySIOmnkui-vOS4reWtpuagoeODu-mnkui-vOmrmOetieWtpuagoQ!5e0!3m2!1sja!2sjp!4v1507003044532" frameborder="0" style="border:0" allowfullscreen></iframe></div>
      </div>

			</section>

<section class="pagetop">
  <p>Page Top</p>
  <span class="arrow"></span>
</section>

    </main>

    <?php include './footer.php'; ?>
 <?php /*   <script src="./js/jquery.min.js"></script>
    <script src="./js/flexibility.js"></script>
    <script src="./js/common.js" type="text/javascript"></script>
    <script>
    $(document).ready(function(){
      $(function(){
        var ua = navigator.userAgent;
        if((ua.indexOf('iPhone') > 0) || ua.indexOf('iPod') > 0 || (ua.indexOf('Android') > 0 && ua.indexOf('Mobile') > 0)){
            $('head').prepend('<meta name="viewport" content="width=device-width,initial-scale=1">');
        } else {
            $('head').prepend('<meta name="viewport" content="width=1320">');
        }
      });
    });
    </script>*/?>
  </body>
</html>
