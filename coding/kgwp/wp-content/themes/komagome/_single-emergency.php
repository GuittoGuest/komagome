<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <title>緊急連絡 | 駒込中学校・高等学校</title>

    <meta content="" name="description">
    <meta http-equiv="Pragma" content="no-store">
    <meta http-equiv="Cache-Control" content="no-store">
    <meta http-equiv="Expires" content="0">

    <link href="/images/common/favicon.ico" rel="shortcut icon">
    <link href="/images/common/favicon.ico" rel="apple-touch-icon">
    <link href="/css/common.css" rel="stylesheet" type="text/css">
    <link href="/css/sub.css" rel="stylesheet" type="text/css">
    <link href="/css/news.css" rel="stylesheet" type="text/css">
    <style>
    .single-news .news{
      margin: 8rem auto;
    }
    </style>
  </head>

  <body class="single-news">
  <?php include './header.php'; ?>

    <main>

      <section class="mv header-title">
      	<h1>緊急連絡</h1>
        <p>News List</p>
      </section>

      <div class="body-wrapper">
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			<section class="news">
        <div class="head">
          <p class="date"><?php the_time('Y.m.d'); ?></p>

          <div class="cat"></div>
          <h3 class="title"><?php the_title(); ?></h3>
        </div>

        <article>

        <?php the_content(); ?>
<?php endwhile;endif; ?>
        </article>

      </section>
    </div>

    </main>

  <?php include './footer.php'; ?>
</body>
</html>
