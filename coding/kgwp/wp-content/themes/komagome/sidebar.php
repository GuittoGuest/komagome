<!--　ニュースサイドバー -->
<aside>
  <div class="block category">
    <p class="head">カテゴリ</p>
    <ul>
      <a href="<?php echo home_url( '/' ); ?>news-cat/school/"><li class="cat-school">学校行事</li></a>
      <a href="<?php echo home_url( '/' ); ?>news-cat/club/"><li class="cat-club">クラブ</li></a>
      <a href="<?php echo home_url( '/' ); ?>news-cat/exam/"><li class="cat-exam">入試情報</li></a>
      <a href="<?php echo home_url( '/' ); ?>news-cat/blog/"><li class="cat-blog">校長ブログ</li></a>
			<a href="<?php echo home_url( '/' ); ?>news-cat/education/"><li class="cat-education">新しい教育</li></a>
			<a href="<?php echo home_url( '/' ); ?>news-cat/other/"><li class="cat-other">その他</li></a>
    </ul>
  </div>

<!--新着記事----->
  <div class="block latest">
    <p class="head">新着記事</p>
    <ul>
<?php
  $args = array('posts_per_page' => 6, 'post_type' => 'news');
  $posts = get_posts( $args );
  foreach ( $posts as $post ):
    setup_postdata( $post );
    $title = get_the_title();
    $titletrim = mb_strimwidth($title, 0, 74, "...");
?>
    <a href="<?php the_permalink();?>">
      <li class="flex">
        <div class="left">
          <img src="<?php echo catch_first_image(); ?>" alt="<?php the_title(); ?>" />
          <p class="date"><?php the_time('Y.m.d'); ?></p>
        </div>
        <div class="right">
          <p><?= $titletrim; ?></p>
        </div>
      </li>
    </a>
<?php endforeach; wp_reset_postdata(); ?>
    </ul>
  </div>

  <div class="block archive">
    <p class="head">アーカイブ</p>
    <ul>
      <?php
$year_prev = null;
$months = $wpdb->get_results("SELECT DISTINCT MONTH( post_date ) AS month ,
                                          YEAR( post_date ) AS year,
                                          COUNT( id ) as post_count FROM $wpdb->posts
                                          WHERE post_status = 'publish' and post_date <= now( )
                                          and post_type = 'news'
                                          GROUP BY month , year
                                          ORDER BY post_date DESC");
      foreach($months as $month):
          $year_current = $month->year;
          if ($year_current != $year_prev) { ?>
              <?php if($year_prev != null): ?>
                  </ul>
              <?php endif; ?>

              <li class="pulldown"><?php echo $month->year; ?>年</li>
              <li class="sub">
          <?php } ?>

                      <a href="<?php echo esc_url(home_url()); ?>/<?php echo $month->year; ?>/<?php echo date("m", mktime(0, 0, 0, $month->month, 1, $month->year)) ?>?post_type=news">
                          <span><?php echo date("n", mktime(0, 0, 0, $month->month, 1, $month->year)) ?>月</span>
                      </a>
                  <?php $year_prev = $year_current; ?>
      <?php endforeach; ?>
                </li>
              </ul>

    </ul>
  </div>
</aside>
