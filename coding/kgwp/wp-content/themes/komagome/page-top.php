<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <title>駒込中学校・高等学校</title>
    <meta content="駒込中学校・高等学校" name="description">
    <meta http-equiv="Pragma" content="no-store">
    <meta http-equiv="Cache-Control" content="no-store">
    <meta http-equiv="Expires" content="0">
		<meta name="format-detection" content="telephone=no">

    <link href="/images/common/favicon.ico" rel="shortcut icon">
    <link href="/images/common/favicon.ico" rel="apple-touch-icon">
    <link href="/css/common.css" rel="stylesheet" type="text/css">
    <link href="/css/top.css" rel="stylesheet" type="text/css">
    <link href="/owl/owl.carousel.min.css" rel="stylesheet" type="text/css">
  </head>

  <body>
    <?php include './header.php'; ?>

    <main>
      <section class="mv">
        <img src="/images/top/mv-circle.png">
      </section>

<?php
$args = array('posts_per_page' => 2,
            'post_type' => 'news',
           'post_status' => array('publish'),
           'meta_key'=>'pickup',
           'meta_value'=>true
            );

$m_query = new WP_Query($args);
?>
      <section class="news">
      	<div class="index">
      		<p>News</p>
      	</div>
      	<div class="list">
      		<ul>
      <?php
            if ($m_query->have_posts()) : while ($m_query->have_posts()) : $m_query->the_post();
      ?>
      	<li><a href="<?php the_permalink(); ?>">》<span class="date"><?php the_time('Y/m/d'); ?></span><span class="title"><?php the_title(); ?></span></a></li>
      <?php
      	endwhile;
        endif;
        wp_reset_postdata();
      ?>
      </div>

      </section>

      <section class="news-topic">
        <h2>News & Topics </h2>
        <div class="container">
          <div class="tab">
            <ul>
              <li id="tab-all" class="-selected">すべて</li>
              <li id="tab-school">学校行事</li>
              <li id="tab-club">部活動</li>
              <li id="tab-exam">入試情報</li>
              <li id="tab-blog">校長ブログ</li>
              <li id="tab-education">新しい教育</li>
              <li id="tab-other">その他</li>
            </ul>
            <a href="/news"><img src="/images/top/ico-list.svg">一覧へ</a>
          </div>
<!--ニュース全て-->
    <div class="news-list">
          <ul class="list-all -active">
            <?php
              $args = array('posts_per_page' => 8, 'post_type' => 'news');
              $posts = get_posts( $args );
              foreach ( $posts as $post ):
                setup_postdata( $post );

              	$postid = get_the_ID();
              	$terms = get_the_terms( $postid, 'news-cat' );
              	$cname = $terms[0]->name;
              	$cslug = $terms[0]->slug;
              	$parent = (int)$terms[1]->parent;
              	if(isset($terms[1])&& $parent>=1){
              		$cattmp = $cname;
              		$cname = $cattmp.'－ '.$terms[1]->name;
              	}
                $title = get_the_title();
                $titletrim = mb_strimwidth($title, 0, 58, "...");
                $imgurl = catch_first_image();
                $height = '';
                if(strpos($imgurl,'noimage.png') !== false){
                  $height = "noimage";
                }
            ?>
            <?php if($post->ID==59): ?>
            <a href="/employment/">
            <?php else: ?>
            <a href="<?php the_permalink(); ?>">
            <?php endif; ?>
              <li>
                <span class="cat cat-<?= $cslug; ?>"><?= $cname; ?></span></span>
                  <div class="images"><img src="<?php echo catch_first_image(); ?>" class="<?=$height;?>" alt="<?php the_title(); ?>" /></div>
                  <div class="detail">
    								<p class="date"><?php the_time('Y.m.d'); ?></p>
    								<p class="post-title"><?= $titletrim; ?></p>
    							</div>
              </li>
						</a>
<?php endforeach; wp_reset_postdata(); ?>
          </ul>
          <!--お知らせend-->

<?php

  $category = array(
    'school',
    'club',
    'exam',
    'blog',
    'education',
    'other'
  );

  foreach($category as $c):
?>

<ul class="list-<?= $c; ?>">
  <?php
    $args = array('posts_per_page' => 8, 'post_type' => 'news', 'taxonomy'=>'news-cat', 'term'=>$c);
    $posts = get_posts( $args );
    foreach ( $posts as $post ):
      setup_postdata( $post );

      $postid = get_the_ID();
      $terms = get_the_terms( $postid, 'news-cat' );
      $cname = $terms[0]->name;
      $cslug = $terms[0]->slug;
      $parent = (int)$terms[1]->parent;
      if(isset($terms[1])&& $parent>=1){
        $cattmp = $cname;
        $cname = $cattmp.'－ '.$terms[1]->name;
      }
      $title = get_the_title();
      $titletrim = mb_strimwidth($title, 0, 62, "...");
      $imgurl = catch_first_image();
      $height = '';
      if(strpos($imgurl,'noimage.png') !== false){
        $height = "noimage";
      }
  ?>
  <?php if($postid=='59'): ?>
  <a href="/employment/">
  <?php else: ?>
  <a href="<?php the_permalink(); ?>">
  <?php endif; ?>
    <li>
      <span class="cat cat-<?= $c; ?>"><?= $cname; ?></span></span>
        <div class="images"><img src="<?php echo catch_first_image(); ?>" class="<?=$height;?>" alt="<?php the_title(); ?>" /></div>
        <div class="detail">
          <p class="date"><?php the_time('Y.m.d'); ?></p>
          <p class="post-title"><?= $titletrim; ?></p>
        </div>
    </li>
  </a>
<?php wp_reset_postdata();endforeach;  ?>
</ul>

<?php endforeach; ?>



      </section>
    </main>

    <?php include './footer.php'; ?>
    <script src="/owl/owl.carousel.min.js"></script>
    <script>
    $(document).ready(function(){
      /* タブレット用ビューポート */
      $(function(){
        var ua = navigator.userAgent;
        if((ua.indexOf('iPhone') > 0) || ua.indexOf('iPod') > 0 || (ua.indexOf('Android') > 0 && ua.indexOf('Mobile') > 0)){
            $('head').prepend('<meta name="viewport" content="width=device-width,initial-scale=1">');
        } else {
            $('head').prepend('<meta name="viewport" content="width=1320">');
        }
      });

        $('.news-topic .tab li').on('click',function(){
          var cl = $(this).attr('class');
          var tabid = $(this).attr('id');
          if(cl != "-selected"){
            $('.news-topic .tab li').removeClass('-selected');
            $(this).addClass('-selected');
          }

          $(".news-list ul li").hide();
          var timer =300;
          var list = tabid.replace( 'tab', 'list' );
          var listclass = "." + list + " li";
          $(listclass).each(function(index, element) {
            $(this).fadeIn(timer);
            timer=timer+300;
          });
        });

        /* 緊急連絡モーダル */
/*      	$('.news li').on('click',function(){
      		$('.modal div').removeClass('-current');
      		var li_id = $(this).attr('id');
      		var m_id = 'm-' + li_id;
      		$('.modal').addClass('-active');
      		$('#'+m_id).addClass('-current');
      	});
      	$('.modal').on('click',function(){
      		$('.modal').removeClass('-active');
      		$('.modal div').removeClass('-current');
      	});*/
    });
    </script>
  </body>
</html>
