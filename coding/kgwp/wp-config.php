<?php
/**
 * WordPress の基本設定
 *
 * このファイルは、インストール時に wp-config.php 作成ウィザードが利用します。
 * ウィザードを介さずにこのファイルを "wp-config.php" という名前でコピーして
 * 直接編集して値を入力してもかまいません。
 *
 * このファイルは、以下の設定を含みます。
 *
 * * MySQL 設定
 * * 秘密鍵
 * * データベーステーブル接頭辞
 * * ABSPATH
 *
 * @link http://wpdocs.osdn.jp/wp-config.php_%E3%81%AE%E7%B7%A8%E9%9B%86
 *
 * @package WordPress
 */

// 注意:
// Windows の "メモ帳" でこのファイルを編集しないでください !
// 問題なく使えるテキストエディタ
// (http://wpdocs.osdn.jp/%E7%94%A8%E8%AA%9E%E9%9B%86#.E3.83.86.E3.82.AD.E3.82.B9.E3.83.88.E3.82.A8.E3.83.87.E3.82.A3.E3.82.BF 参照)
// を使用し、必ず UTF-8 の BOM なし (UTF-8N) で保存してください。

// ** MySQL 設定 - この情報はホスティング先から入手してください。 ** //
/** WordPress のためのデータベース名 */
define( 'DB_NAME', 'fa1161002_hp' );

/** MySQL データベースのユーザー名 */
define( 'DB_USER', 'fa1161002' );

/** MySQL データベースのパスワード */
define( 'DB_PASSWORD', 'K0m@93475G' );

/** MySQL のホスト名 */
define( 'DB_HOST', '127.0.0.1' );

/** データベースのテーブルを作成する際のデータベースの文字セット */
define( 'DB_CHARSET', 'utf8mb4' );

/** データベースの照合順序 (ほとんどの場合変更する必要はありません) */
define('DB_COLLATE', '');

/**#@+
 * 認証用ユニークキー
 *
 * それぞれを異なるユニーク (一意) な文字列に変更してください。
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org の秘密鍵サービス} で自動生成することもできます。
 * 後でいつでも変更して、既存のすべての cookie を無効にできます。これにより、すべてのユーザーを強制的に再ログインさせることになります。
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'nTgtlN$MdUm+vX.G7eEo*9GVZW]Qm4zQS?fYH5Qup`KTk&VpCUWX@9QKVNrnbsqN' );
define( 'SECURE_AUTH_KEY',  'Ep[b>(EZ92_DXTP[I+Jd#$xs0RjeYu+rl>e<1ymaKvg*;-#1m;?l]H)j5:l5O%M`' );
define( 'LOGGED_IN_KEY',    '/PzF*&GEfHT(cXAuAcF39?)[ij?pzKXebI01:=CD[#/vzo0O.*?@F. Os,KL5*U8' );
define( 'NONCE_KEY',        'hzQNk0*Yw~f[F>)w{.]+r:ALn#ObnKl8=!lJ0OL/%?8ZVZbrA8PbtAYQcOYg>gaQ' );
define( 'AUTH_SALT',        'zqf=;{$cTb6yqhRZ4b~L7dPX)62b/s3}NpOj_+krxOn&u)},meD#M_0dXe$f>_Gg' );
define( 'SECURE_AUTH_SALT', 'ca]$-V&6CFV]Vn|9S{Lz!|(;|BaGk]r}N7:N22uQC:lI<E8(1Er*@H]a^*6KFGmR' );
define( 'LOGGED_IN_SALT',   'Iq!mzhP%#sR6` [H_.=jeZG[nEc5s5Fa63($ytQ,H|/+-mk[ITh^w@g6wadea]n7' );
define( 'NONCE_SALT',       'ycsjVI=(zg6^}0+Gp]XjiI}{$-o!I$ujJ%/<LW7Lwn>77Rt~VmL93:Av48oJa`#T' );

/**#@-*/

/**
 * WordPress データベーステーブルの接頭辞
 *
 * それぞれにユニーク (一意) な接頭辞を与えることで一つのデータベースに複数の WordPress を
 * インストールすることができます。半角英数字と下線のみを使用してください。
 */
$table_prefix = 'kgwpstg_';

/**
 * 開発者へ: WordPress デバッグモード
 *
 * この値を true にすると、開発中に注意 (notice) を表示します。
 * テーマおよびプラグインの開発者には、その開発環境においてこの WP_DEBUG を使用することを強く推奨します。
 *
 * その他のデバッグに利用できる定数については Codex をご覧ください。
 *
 * @link http://wpdocs.osdn.jp/WordPress%E3%81%A7%E3%81%AE%E3%83%87%E3%83%90%E3%83%83%E3%82%B0
 */
define('WP_DEBUG', false);

/* 編集が必要なのはここまでです ! WordPress でのパブリッシングをお楽しみください。 */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
