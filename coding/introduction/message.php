<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
    <meta 学校長挨拶="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <title>学校長挨拶｜駒込中学校・高等学校</title>
    <meta content="駒込中学校・高等学校" name="description">
    <meta http-equiv="Pragma" content="no-store">
    <meta http-equiv="Cache-Control" content="no-store">
    <meta http-equiv="Expires" content="0">
		<meta name="format-detection" content="telephone=no">

    <link href="../images/common/favicon.ico" rel="shortcut icon">
    <link href="../images/common/favicon.ico" rel="apple-touch-icon">
    <link href="../css/common.css" rel="stylesheet" type="text/css">
<?php //    <link href="../css/sub.css" rel="stylesheet" type="text/css"> ?>
    <link href="../css/sub2.css" rel="stylesheet" type="text/css">

  </head>

  <body>
    <?php include '../header.php'; ?>

    <main>
      <section class="mv header-title">
				<h1>学校長挨拶</h1>
        <p>Greeting</p>
      </section>

      <section class="article-main">
        <article>
          <h2>すばらしい「夜明け前の今の時代」に為すべき事</h2>
        </article>
      </section>

			<section class="article-main">
        <div class="two-column">
          <div class="box">
            <div class="text">
              <h4>今と言う時代！</h4>
              <p>受験生の皆さんは今、人類史上最も大きな変革の時代に高校・大学という学びの時間を持つことになります。ＡＩ＝人工知能があらゆる分野に進出してきます。それによって人類文化が大変革されます。当然、学びの意味も手段も方法も目的も大変革されます。この時大切なのは、自己の確立の仕方です。成績は二番手でも良いのです。求められるのは、「誰もがやらないからこそ自分がやる」という「大きな志」を持って「未来志向型の学び」にチャレンジすることなのです。学校で教わらなくても「プログラミング」や「投資技術」は同好会の仲間を作ってでも身につけるべきです。これらは来年から小学校に導入され、次の世代はまったく新しいＳＴＥＭ教育を受けて、ＩＣＴ時代の担い手として皆さんを追い越していきます。10年後の自分を想定できる歩みを開始してください。駒込が先駆者的に取り組んでいる学校改革の目的がここにあります。本校はすでに3年前からＳＴＥＭ教育やグローバル教育を導入して展開している学校なのです。駒込の未来の門を開いて新しい学びを始めてみませんか！</p>
            </div>
            <p class="image tac mincho"><img src="../images/about/principal.jpg"><span>駒込中学高等学校　校長　<strong class="fs1.2">河合孝允</strong></span></p>
          </div>

          <div class="box reverse">
            <div class="text">
              <h4>一番優れた教育とは何か？</h4>
              <p>医者になりたいというのは自己の個人的願望ですが、医者になって無医村で苦しんでいる子供や老人の命を救いたいという思いは「志」を高く立てなくては成立しません。<br>変革の時代は全ての若者に大きなチャンスが訪れます。今はすばらしい「夜明け前」の時代なのです。でも夜明け前は「最も暗くなる時間」でもあります。見通しのつかない不安な時代でもあります。大切なのはその受け止め方です。ものの見方を「未来の自分から今の自分を見る」という180度転回させた視点を生み出すことが大切です。未来の自分の姿から<ruby>演繹<rp>(</rp><rt>えんえき</rt><rp>)</rp></ruby>して今の自分が「なすべき課題」に目覚めることが大事です。そうすればどのような<ruby>艱難<rp>(</rp><rt>かんなん</rt><rp>)</rp></ruby><ruby>辛苦<rp>(</rp><rt>しんく</rt><rp>)</rp></ruby>も乗り越えていける「強い自分」を生み出せるからです。「最も優れた教育」とは、「その人間が自分で自分を教育出来るように育て上げること」なのです。人生は「七転び八起き」です。自己肯定感をしっかりと確立してこの大変革時代を戦い抜いてください。駒込学園はそういう若者達の群れ集う学舎です。</p>
            </div>
          </div>

          <div class="box">
            <div class="text">
              <h4>人類に残される仕事とは？</h4>
              <p>今ある仕事の6割は10年以内にＡＩに代替されて消滅します。最後まで残るのは、</p>
              <ol class="figure flex_pc column4 fs0.9">
                <li>ホスピタリティ</li>
                <li>マネジメント</li>
                <li>クリエイティビティ</li>
              </ol>
              <p>の3種だといわれています。しかし、高度情報時代に必要な新たな職種が600万種生まれると想定されています。皆さんはその「担い手としての自分」をスキル化して生み出さなければなりません。文学をやるにしろ、経済をやるにしろ、法律をやるにしろ、IoT時代のスキルと価値観を身につけなければ、テクノ失業時代の失業者となるしかなくなります。皆さんが持つべき一番大切な能力は未来選択能力なのです。すでに中高年（40～64歳）の引きこもり人口は61万人に及ぶのです。一方で大学を出て引きこもっている若者は30万人を超えているのです。その予備軍である中高生の登校拒否人口は44万人を超えています。さらには、10代の生徒のリストカット経験者は10人に1人、1割にも及び、その6割が10回以上のリストカット経験者なのです。もはや個人の問題ではなく社会現象です。このリストカットをする最大要因は「自己肯定感」を持てなくなった子どもが、<em>自分を救うため</em>に行う最終行為なのです。切った瞬間の痛みに全神経が集中した時、ダメな自分を責めていた悪感情が瞬間的に消えて「ああ助かった！すっきりした！」と思えるから繰り返すのです。悲しい現実ですね。本校は仏教の学校ですから「<ruby>坐禅<rp>(</rp><rt>ざぜん</rt><rp>)</rp></ruby><ruby>止観<rp>(</rp><rt>しかん</rt><rp>)</rp></ruby>」を教えます。止観とは「無・<ruby>空<rp>(</rp><rt>くう</rt><rp>)</rp></ruby>の私」を生み出す所作です。リストカットしなくても自分を救い出せるのです。皆さんは大切な国の宝です。伸びやかに広やかにそして自分らしく好きな世界に夢をもって羽ばたいてもらいたいと念願しています。今までの自分に少し疲れた自分がいるのであれば、どうでしょうか、駒込の門を叩いてみませんか！私が私であるために！<br>ぜひ駒込の文化祭や説明会に「遊び」に来てください！次の言葉を贈ります。</p>
            </div>
          </div>
          <div class="tac w100">
            <h5>遊びをせんとや生まれけん　戯れせんとや生まれけん<br>遊ぶ子供の声聴けば　わが身さえこそゆるがるれ　（梁塵秘抄）</h5>
          </div>
        </div>

			</section>


<section class="pagetop">
  <p>Page Top</p>
  <span class="arrow"></span>
</section>

    </main>

    <?php include '../footer.php'; ?>
<?php /*    <script src="../js/jquery.min.js"></script>
    <script src="../js/flexibility.js"></script>
    <script src="../js/common2.js" type="text/javascript"></script> */?>
  </body>
</html>
