<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <title>学校沿革｜駒込中学校・高等学校</title>
    <meta content="駒込中学校・高等学校" name="description">
    <meta http-equiv="Pragma" content="no-store">
    <meta http-equiv="Cache-Control" content="no-store">
    <meta http-equiv="Expires" content="0">
		<meta name="format-detection" content="telephone=no">

    <link href="../images/common/favicon.ico" rel="shortcut icon">
    <link href="../images/common/favicon.ico" rel="apple-touch-icon">
    <link href="../css/common.css" rel="stylesheet" type="text/css">
<?php //    <link href="../css/sub.css" rel="stylesheet" type="text/css"> ?>
    <link href="../css/sub2.css" rel="stylesheet" type="text/css">

  </head>

  <body class="history">
    <?php include '../header.php'; ?>
    <main>
      <section class="mv header-title">
        <h1>学校沿革</h1>
        <p>History</p>
      </section>
      <section class="article-main">
        <article>
          <h2>伝統が創り上げる新しい駒込学園で<br>一人ひとりの色、輝く。</h2>
          <p class="mincho tac">それぞれの生徒が「一隅を照らす」ことの尊さを知り、<br class="pc">かけがえのない存在であることを認め合い、<br class="pc">未来に向かって向上心を持って生きてほしい。<br>駒込学園は330年をこえる長き伝統を守りながら、新たな300年に向けて歩み続けます。</p>
        </article>
      </section>
			<section class="article-main">
<h3>学園の歩み</h3>
  <div class="two-column">
    <div class="box">
      <div class="text">
<dl class="flex_pc wrap history">
  <dt>天和2年（1682）</dt>
  <dd>了翁禅師により「勧学講院」が上野不忍池の畔に設立され、これが本学園の濫觴となる。</dd>
  <dt>明治6年（1873）</dt>
  <dd>校名を「天台宗東部総黌」と改め、校地は上野寛永寺境内に定められる。</dd>
  <dt>明治18年（1885）</dt>
  <dd>校名を「天台宗東部大中総黌」と改め、正式に天台宗立となる。</dd>
  <dt>明治37年（1904）</dt>
  <dd>校名を「天台宗中学」および「天台宗大学校」と改め、現在の場所に移される。</dd>
  <dt>大正14年（1925）</dt>
  <dd>12月12日付、宗門外からの生徒も加え普通課程の教育を行う。駒込中学校として文部省から認可される。</dd>
  <dt>大正15年（1926）</dt>
  <dd>４月１日より「駒込中学校」が開校する。</dd>
  <dt>昭和20年（1945）</dt>
  <dd>戦災で本館を残して校舎を焼失する。</dd>
  <dt>昭和22年（1947）</dt>
  <dd>学制改革に伴い新制の「駒込中学校」並びに「駒込高等学校」に改組する。この頃より校舎の復興すすむ。</dd>
  <dt>昭和27年（1952）</dt>
  <dd>従来の男子教育に加え、４月１日より新たに「女子部」を開設する。</dd>
  <dt>昭和37年（1962）</dt>
  <dd>四階建ての新校舎が落成する。</dd>
  <dt>昭和41年（1966）</dt>
  <dd>男女共学制をしき、建学の伝統の現代的適応をはかる。</dd>
  <dt>昭和49年（1974）</dt>
  <dd>服装を自由化する。</dd>
  <dt>昭和51年（1976）</dt>
  <dd>創立50周年記念として、格技場（主に柔道）が完成する。</dd>
  <dt>昭和53年（1978）</dt>
  <dd>新生徒ホール･新クラブ室が完成する。</dd>
  <dt>昭和56年（1981）</dt>
  <dd>４月に新体育館兼講堂が完成する。</dd>
  <dt>平成元年（1989）</dt>
  <dd>制服を新たに制定する。</dd>
  <dt>平成3年（1991）</dt>
  <dd>比叡山研修（高校１年生）を開始する。学習合宿（高校２年生）を開始する。</dd>
  <dt>平成4年（1992）</dt>
  <dd>昭和47年以来、募集停止中であった中学校を共学で再開する。</dd>
  <dt>平成5年（1993）</dt>
  <dd>中学学習合宿を開始する（中学２年生）。海外修学旅行（高校２年生）を実施する。</dd>
  <dt>平成7年（1995）</dt>
  <dd>創立70周年記念式典を挙行する。</dd>
  <dt>平成9年（1997）</dt>
  <dd>中学校再開第一期生が卒業する。</dd>
  <dt>平成11年（1999）</dt>
  <dd>九州・沖縄修学旅行を実施する（高校２年生）。</dd>
  <dt>平成15年（2003）</dt>
  <dd>地上６階・地下１階の、IT時代に対応した高校新校舎が落成する。</dd>
  <dt>平成24年（2012）</dt>
  <dd>日光山研修を開始する。</dd>
  <dt>平成27年（2015）</dt>
  <dd>創立90周年記念式典を挙行する。</dd>
  <dt>平成28年（2016）</dt>
  <dd>セブ島語学研修（中学3年生）を開始する。ICT教育によるタブレット端末の導入。</dd>
</dl>
            </div>
          </div>
        </div>
        <p class="image"><img src="../images/top/top004.jpg" alt=""></p>
			</section>


<section class="pagetop">
  <p>Page Top</p>
  <span class="arrow"></span>
</section>

    </main>

    <?php include '../footer.php'; ?>
<?php /*    <script src="../js/jquery.min.js"></script>
    <script src="../js/flexibility.js"></script>
    <script src="../js/common2.js" type="text/javascript"></script> */?>
  </body>
</html>
