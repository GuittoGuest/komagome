<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <title>アクセス｜駒込中学校・高等学校</title>
    <meta content="駒込中学校・高等学校" name="description">
    <meta http-equiv="Pragma" content="no-store">
    <meta http-equiv="Cache-Control" content="no-store">
    <meta http-equiv="Expires" content="0">
		<meta name="format-detection" content="telephone=no">

    <link href="../images/common/favicon.ico" rel="shortcut icon">
    <link href="../images/common/favicon.ico" rel="apple-touch-icon">
    <link href="../css/common.css" rel="stylesheet" type="text/css">
    <link href="../css/sub.css" rel="stylesheet" type="text/css">

  </head>

  <body>
    <?php include '../header.php'; ?>

    <main>
      <section class="mv">
				<h1>アクセス</h1>
      </section>

			<section class="article-main">
				<article>
					<h2>アクセス</h2>
					<p>
学校法人 駒込学園 駒込中学校・高等学校
〒113-0022　東京都文京区千駄木5-6-25
TEL：03-3828-4141　FAX：03-3824-5685

電車
東京メトロ
南北線 『本駒込駅』 下車・徒歩５分（１番出口もしくはエレベーターをご利用下さい）
千代田線 『千駄木駅』 下車・徒歩７分（１番出口をご利用下さい）

都営地下鉄
三田線 『白山駅』 下車・徒歩７分（Ａ３出口をご利用下さい）

バス
都バス草63－駒込千駄木町（駒込学園前下車）</p>
				</article>
			</section>


    </main>

    <?php include '../footer.php'; ?>
<?php /*    <script src="../js/jquery.min.js"></script>
    <script src="../js/flexibility.js"></script>
    <script src="../js/common.js" type="text/javascript"></script>
    <script>
    $(document).ready(function(){
      /* タブレット用ビューポート */
      $(function(){
        var ua = navigator.userAgent;
        if((ua.indexOf('iPhone') > 0) || ua.indexOf('iPod') > 0 || (ua.indexOf('Android') > 0 && ua.indexOf('Mobile') > 0)){
            $('head').prepend('<meta name="viewport" content="width=device-width,initial-scale=1">');
        } else {
            $('head').prepend('<meta name="viewport" content="width=1320">');
        }
      });
    });
    </script> */?>
  </body>
</html>
