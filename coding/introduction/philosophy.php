<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <title>教育理念｜駒込中学校・高等学校</title>
    <meta content="駒込中学校・高等学校" name="description">
    <meta http-equiv="Pragma" content="no-store">
    <meta http-equiv="Cache-Control" content="no-store">
    <meta http-equiv="Expires" content="0">
		<meta name="format-detection" content="telephone=no">

    <link href="../images/common/favicon.ico" rel="shortcut icon">
    <link href="../images/common/favicon.ico" rel="apple-touch-icon">
    <link href="../css/common.css" rel="stylesheet" type="text/css">
<?php //    <link href="../css/sub.css" rel="stylesheet" type="text/css"> ?>
    <link href="../css/sub2.css" rel="stylesheet" type="text/css">

  </head>

  <body>
    <?php include '../header.php'; ?>

    <main>
      <section class="mv header-title">
				<h1>教育理念・教育方針</h1>
        <p>Spirit</p>
      </section>

			<section class="article-main">
        <article>
          <div class="sub-menu">
            <a href="#rinen">教育理念</a>
            <a href="#houshin">教育方針</a>
          </div>
        </article>
        <div class="wrp">
          <div class="two-column">
            <div class="box frset" id="rinen">
              <p class="image fr"><img src="../images/about/rinen.jpg" alt="十一面観世音菩薩"></p>
              <div class="text">
                <h3>教育理念<span>全力を尽くす人間こそ、国や社会の宝である。</span></h3>
                <p>駒込学園は、天和2年（1682年）に了翁禅師によって上野・寛永寺境内の不忍池のほとりに創立されました。当時は、「勧学講院」と呼ばれ、お寺の子弟のほかにも大勢の町人の子供たちが通っていました。その意味では社会に大きく開かれた「寺子屋」だったのです。 本学園は、現在から1200年前に伝教大師･最澄が「山家学生式」という書物の中に書き残した「一隅を照らす」という言葉を教育の理念として掲げています。人間にとって一番尊いものは、珠玉や金銭ではなく、「その人が置かれたその場にあって、ひたむきな向上心をもって全力を尽くす人こそ国や社会の宝である」という意味です。 その建学の精神は、現代でもいささかもあせてはいないのです。いや、むしろ今ほど「社会の宝」としての人間教育が求められている時代はありません。さきごろ、比叡山で開かれた「世界宗教サミット」では、この最澄の言葉が<strong>「Light Up Your World」</strong>と訳されて紹介され、世界に深い感銘を与えました。世界中が社会の宝となるべき人材を求めているのです。 勧学講院以来330余年－駒込学園は21世紀に向けて、いま新たな第一歩を踏み出しています。</p>
              </div>

            </div>
          </div>

          <div class="two-column">
            <div class="box frset reverse" id="houshin">
              <p class="image fl tac mincho"><img src="../images/about/president.jpg"><span>理事長&nbsp;<strong class="fs1.2">末廣 照純</strong></span></p>
              <div class="text">
                <h3>教育方針<span>生徒一人ひとりが光り輝く人間になってほしい！<br>私たちはこのように念願しています。 </span></h3>
                <p>高校ではSTEM教育（数学をベースに情報工学を加味した教育）に基盤をおいた「理系先進コース」並びにイマージョン授業とグローバル教育に基礎を置いた「国際教養コース」を立ち上げ、中学ではそのジュニアコースとして「国際先進コース」を立ちあげて皆さんをお待ちいたします。 </p>
                <p>中学高校時代の６年間は、人間形成の上で最も大切なときです。価値観が多様化し、人々の心のあり方があらためて問われている今、次世紀を担う若い世代の教育こそ私たちに課せられた使命です。 駒込学園は、伝教大師最澄の「国宝とはなにものぞ－一隅を照らす、これすなわち国宝なり」という言葉を建学の精神として、３３０余年の伝統を守り続けてきました。私たちは宇宙からみると、点のような小さなところ（一隅）で生きています。たとえ、小さくてもいい、今こそ、私たち一人ひとりがこの一隅でお互いが、かけがえのない貴い存在であることを認め合い、さらなる向上心をもって生きていく“一隅を照らす光り輝く人間”になるべきときなのです。 私たちは、このように考え、人間としてのあるべき姿を求め続けています。</p>
              </div>

            </div>
          </div>
        </div>
			</section>


<section class="pagetop">
  <p>Page Top</p>
  <span class="arrow"></span>
</section>

    </main>

    <?php include '../footer.php'; ?>
<?php /*    <script src="../js/jquery.min.js"></script>
    <script src="../js/flexibility.js"></script>
    <script src="../js/common2.js" type="text/javascript"></script> */?>
  </body>
</html>
