<?php
	$root = "";

	$dir = explode('/',$_SERVER["REQUEST_URI"]);
	$filter = array_filter( $dir, "strlen" );
	if(count($filter) == 1){
		require('./kgwp/wp-load.php');
	}else if(count($filter) == 2){
		if($filter[1]!='news' && $filter[1]!='news-cat'){
			require('../kgwp/wp-load.php');
		}
	}


?>
    <header>
      <div class="container">
        <div class="upper">
          <a href="<?=$root; ?>/top">
            <img src="<?=$root; ?>/images/common/logo.png">
            <h1>学校法人 駒込学園　<br class="sp">駒込中学校・高等学校</h1>
          </a>
        </div>
        <div class="right">
					<?php
					          $m_query = new WP_Query(
					            array('posts_per_page' => -1, 'post_type' => 'emergency', 'post_status' => array('publish'))
					          );
					            if ( $m_query->have_posts() ) :
					          ?>
          <a href="<?=$root; ?>/students-guardian/emergency.php"><div class="active">緊急連絡</div></a>
<?php else: ?>
					<a href="<?=$root; ?>/students-guardian/emergency.php"><div>緊急連絡</div></a>
<?php endif; ?>
					<div class="document">資料請求・お問い合わせ</div>
          <a href="<?=$root; ?>/graduates/office.php#graduates"><div>卒業生の方へ</div></a>
        </div>
        <nav class="lower">
					<div class="close sp"><span></span><span></span></div>
          <div class="block">
            <p>学園の紹介</p>
            <ul>
              <a href="<?=$root; ?>/introduction/philosophy.php"><li>教育理念・教育方針</li></a>
              <a href="<?=$root; ?>/introduction/message.php"><li>校長挨拶</li></a>
              <a href="<?=$root; ?>/introduction/history.php"><li>学校沿革</li></a>
            </ul>
          </div>

          <div class="block">
            <p>駒込の教育</p>
            <ul>
							<a href="<?=$root; ?>/education/global.php"><li>グローバル教育</li></a>
              <a href="<?=$root; ?>/education/ict.php"><li>ICT教育</li></a>
							<a href="<?=$root; ?>/education/support.php"><li>学習支援</li></a>
							<a href="<?=$root; ?>/education/stem.php"><li>STEM教育</li></a>
              <a href="<?=$root; ?>/education/human.php"><li>人間教育</li></a>
            </ul>
          </div>

          <div class="block">
            <p>中学校</p>
            <ul>
              <a href="<?=$root; ?>/junior-high-school/program.php"><li>6ヵ年一貫教育プログラム</li></a>
            </ul>
          </div>

          <div class="block">
            <p>高等学校</p>
            <ul>
              <a href="<?=$root; ?>/high-school/course.php"><li>3コース制</li></a>
            </ul>
          </div>

          <div class="block">
            <p>学校生活</p>
            <ul>
              <a href="<?=$root; ?>/school-life/uniform.php"><li>制服紹介</li></a>
              <a href="<?=$root; ?>/school-life/facility.php"><li>施設紹介</li></a>
              <a href="<?=$root; ?>/school-life/event.php"><li>学校行事</li></a>
              <a href="<?=$root; ?>/school-life/club.php"><li>クラブ紹介</li></a>
            </ul>
          </div>

          <div class="block">
            <p>進路情報</p>
            <ul>
              <a href="<?=$root; ?>/course/data.php"><li>進路データ</li></a>
              <a href="<?=$root; ?>/course/succeed.php"><li>卒業生の活躍</li></a>
            </ul>
          </div>

          <div class="block">
            <p style="border-right: none;">受験生の方へ</p>
            <ul>
              <a href="<?=$root; ?>/examinee/guidance.php"><li>入試説明会案内</li></a>
              <a href="<?=$root; ?>/examinee/j-application.php"><li>中学募集要項</li></a>
              <a href="<?=$root; ?>/examinee/h-application.php"><li>高校募集要項</li></a>
              <a href="<?=$root; ?>/examinee/internet.php"><li>インターネット出願</li></a>
              <a href="<?=$root; ?>/examinee/movie.php"><li>KOMAGOME CHANNEL</li></a>
              <a href="<?=$root; ?>/examinee/kakomon.php"><li>過去問題紹介</li></a>
            </ul>
          </div>
				<div class="block map"><a href="<?=$root; ?>/access.php"><img src="<?=$root; ?>/images/common/map.png"></div>
				<div class="block fb"><a href="https://www.facebook.com/komagomegakuen/" target="_blank"><img src="<?=$root; ?>/images/common/fb-red.png"></a></div>
        <div class="right sp">
					<a href="<?=$root; ?>/students-guardian/emergency.php"><div>緊急連絡</div></a>
					<div class="document">資料請求・お問い合わせ</div>
          <a href="<?=$root; ?>/graduates/office.php#graduates"><div>卒業生の方へ</div></a>
        </div>
        </nav>

<?php
				$m_query = new WP_Query(
					array('posts_per_page' => -1, 'post_type' => 'emergency', 'post_status' => array('publish'))
				);
if ( $m_query->have_posts() ) :
				?>
				<a href="/students-guardian/emergency.php" id="lamp"><span>緊急連絡</span></a>
<?php endif; ?>
				<div class="sp-btn sp">
          <span class="bar"></span>
					<span class="bar"></span>
					<span class="bar"></span>
					<span class="text">MENU</span>
        </div>
      </div>
			<div class="modal">
			<div id="document-modal">
				<h4>資料請求・お問い合わせ</h4>
				<div class="flex">
					<a href="https://mirai-compass.net/usr/komagmj/request/reqIndex.jsf" target="_blank">資料請求（中学）</a>
					<a href="https://mirai-compass.net/usr/komagmh/request/reqIndex.jsf" target="_blank">資料請求（高校）</a>
				</div>
				<p><em>その他、お問い合わせは下記よりご連絡ください。</em><br>
					学校法人　駒込学園　駒込中学校・高等学校<br><br>
					〒113-0022<br>
					東京都文京区千駄木5-6-25<br>
					TEL: 03-3828-4141<br>
					FAX: 03-3824-5685<br>
	<br>
					企画広報室<br>
					TEL: 03-3828-4366<br>
					FAX: 03-3822-6833<br>
					MAIL: kikaku@komagome.ed.jp<br>
	<br>
					<strong>個人情報保護について</strong><br>
					本学園は、電話等によりお問合わせいただきました、<br>
					氏名、住所、電話番号等の個人情報をお問合わせの対応・連絡のためにのみ利用いたします。
				</p>
			</div>
			</div>
    </header>
