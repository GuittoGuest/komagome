<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <title>インターネット出願｜駒込中学校・高等学校</title>
    <meta content="駒込中学校・高等学校" name="description">
    <meta http-equiv="Pragma" content="no-store">
    <meta http-equiv="Cache-Control" content="no-store">
    <meta http-equiv="Expires" content="0">
		<meta name="format-detection" content="telephone=no">

    <link href="../images/common/favicon.ico" rel="shortcut icon">
    <link href="../images/common/favicon.ico" rel="apple-touch-icon">
    <link href="../css/common.css" rel="stylesheet" type="text/css">
		<link href="../css/sub2.css" rel="stylesheet" type="text/css">
<?php //		<link href="../css/sub.css" rel="stylesheet" type="text/css"> ?>
  </head>

  <body id="internet">
    <?php include '../header.php'; ?>

    <main>
      <section class="mv header-title">
				<h1>インターネット<br class="sp">出願</h1>
        <p>Web Apply</p>
      </section>

			<section class="article-main">
				<h3>インターネット出願手続きの流れ<span style="color: red;">※インターネット出願は中学入試・帰国生入試のみです。</span></h3>

        <div class="two-column">
					<div class="box flow">
						インターネット出願サイト
					</div>
          <div class="box">
            <div class="text">
    					<p>募集期間が近くなりましたら、本校ホームページに出願サイトへリンクするバナーを設置します。<br>そちらから出願サイトへ移動してください。</p>
            </div>
          </div>
				</div>

				<div class="two-column">
					<div class="box flow">
						ID(メールアドレス）登録
					</div>
          <div class="box">
            <div class="text">
    					<p>メールアドレスをIDとして登録してください。緊急時にもすぐ確認いただけるメールアドレスを登録してください。イベント予約で既にメールアドレスを登録済みの方は、そちらのIDをお使いください。</p>
            </div>
          </div>
				</div>


				<div class="two-column">
					<div class="box flow">
						ログイン/マイページ
					</div>
          <div class="box">
            <div class="text">
    					<p>登録したメールアドレスを使ってログインしてください。<br>マイページが表示されます。</p>
            </div>
          </div>
				</div>


				<div class="two-column">
					<div class="box flow">
						出願情報入力
					</div>
          <div class="box">
            <div class="text">
    					<p>志望者情報を入力し、出願する試験を選択してください。</p>
            </div>
          </div>
				</div>


				<div class="two-column">
					<div class="box flow">
						支払方法選択
					</div>
          <div class="box">
            <div class="text">
    					<p>受験料のお支払いには、クレジットカード、コンビニ、ペイジー（金融機関ATM）をご利用いただけます。<br>
									また、お支払いには別途手数料が発生します。
							</p>
            </div>
          </div>
				</div>


				<div class="two-column">
					<div class="box flow">
						受験票印刷
					</div>
          <div class="box">
            <div class="text">
    					<p>受験料お支払い完了後、マイページから受験票を取得し、受験生の顔写真画像を取り込んで印刷してください。写真の取り込みができない場合は、顔写真を貼付してください。</p>
            </div>
          </div>
				</div>


        <div class="two-column">
          <div class="box flow">必要書類送付（帰国生入試）</div>
          <div class="box">
            <div class="text">
              <p>必要書類を郵送する際は、受験票と一緒に印刷される宛名状を封筒に貼付し、郵送してください。</p>
            </div>
          </div>
        </div>

				<p class="caution">※試験当日、受験票と志願表（学校控）を必ず持参してください。</p>
				<div class="flex_pc column2 tac">
					<h4><a href="safety.pdf" target="_blank">インターネット出願について</a></h4>
				</div>
				</section>

				<section class="article-main">
					<h3>中学入試　合格発表・入学手続について</h3>
					<div class="flex_pc column2 tac">
					  <h4><a href="safety.pdf" target="_blank">中学合格発表・入学手続きについて</a></h4>
					  <h4><a href="https://mirai-compass.net/ent/komagmj/common/login.jsf" target="_blank">中学入学金決済サイト</a></h4>
					</div>
				</section>

				<section class="article-main">
					<h3>帰国生入試について</h3>
					<div class="flex_pc column2 tac">
					  <h4><a href="https://mirai-compass.net/ent/komagmh/common/login.jsf" target="_blank">帰国生（高校）入学金決済サイト</a></h4>
					</div>
				</section>


<section class="pagetop">
  <p>Page Top</p>
  <span class="arrow"></span>
</section>

    </main>

    <?php include '../footer.php'; ?>
<?php /*    <script src="../js/jquery.min.js"></script>
    <script src="../js/flexibility.js"></script>
    <script src="../js/common2.js" type="text/javascript"></script>*/?>
  </body>
</html>
