<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <title>入試案内｜駒込中学校・高等学校</title>
    <meta content="駒込中学校・高等学校" name="description">
    <meta http-equiv="Pragma" content="no-store">
    <meta http-equiv="Cache-Control" content="no-store">
    <meta http-equiv="Expires" content="0">
		<meta name="format-detection" content="telephone=no">

    <link href="../images/common/favicon.ico" rel="shortcut icon">
    <link href="../images/common/favicon.ico" rel="apple-touch-icon">
    <link href="../css/common.css" rel="stylesheet" type="text/css">
<?php //    <link href="../css/sub.css" rel="stylesheet" type="text/css"> ?>
    <link href="../css/sub2.css" rel="stylesheet" type="text/css">

  </head>

  <body id="admissions">
    <?php include '../header.php'; ?>

    <main>
      <section class="mv header-title">
				<h1>入試説明会案内</h1>
        <p>Admissions</p>
      </section>


			<section class="article-main" id="junior-high-school">
        <div class="sub-menu">
          <a href="#junior-high-school">中学</a>
          <a href="#high-school">高校</a>
        </div>
				<article>
					<h2>中学入試説明会</h2>
					<p>すべての説明会は約１ケ月前より予約を開始いたします。<br>説明会終了後、毎回個別相談会があります（希望者）<br>※説明会日程などは４月現在のものであり、変更になる場合があります。</p>
        </article>
      </section>

      <section class="article-main">
          <p class="entory_btn"><a href="https://mirai-compass.net/usr/komagmj/event/evtIndex.jsf" target="_blank">WEB予約はこちらから</a></p>
          <table class="table_2b">
            <thead>
              <tr>
                <th>日付</th>
                <th>時間</th>
                <th>説明会＆イベント</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <th>6月1日（土）</th>
                <td>14:00～15:30 </td>
                <td> 【駒込中学ってどんな学校？】 -和caféでおもてなし-<br>
茶道体験 ＆ 駒込中学校の先生たちとの座談会<br></td>
              </tr>
              <tr>
                <th>6月29日（土） </th>
                <td>10:00～12:30 </td>
                <td>【毎年大好評！】<br>
                給食試食会 と 高１内進生の体験談<br><a href="//komagome.ed.jp/exam/data/j-flyer20190629.pdf" target="_blank"><span class="fa fa-file-pdf-o" aria-hidden="true"></span>詳細チラシ</a><br><a href="//komagome.ed.jp/exam/data/j-flyer201906293.pdf" target="_blank"><span class="fa fa-file-pdf-o" aria-hidden="true"></span>アレルギー対象⾷品について</a></td>
              </tr>
              <tr>
                <th>7月28日（日） </th>
                <td>9:30～12:00<br>
                13:30～16:00 </td>
                <td>【夏休み体験会】 （午前・午後共に同じ内容）<br>
                おもしろ理科実験教室 / 楽しいクッキング教室</td>
              </tr>
              <tr>
                <th>8月25日（日） </th>
                <td><p>10:00～12:00<br>
                </p></td>
                <td><p>【からだを動かして英語を楽しもう！】</p>
                  <p>わくわく英語アクティビティ ＆ やさしい坐禅体験<br>
                </p></td>
              </tr>
              <tr>
                <th>9月16日（月・祝） </th>
                <td>10:00～12:00 <br>
                (13:30～15:30)<br></td>
                <td>【人気のクラブが大集合】クラブ体験会<br>
                ※午後：STEM入試 / 自己表現入試 体験会実施<br></td>
              </tr>
              <tr>
                <th>9月27日（金） </th>
                <td>18:30～19:30<br></td>
                <td>【お仕事帰りでも大丈夫】 -駒込caféへようこそ-<br>
                イブニング説明会 駒込中学校の先生たちとの座談会<br></td>
              </tr>
              <tr>
                <th>10月26日（土） </th>
                <td>10:00～12:30 </td>
                <td>【毎年大好評！その２】<br>
                秋の給食試食会</td>
              </tr>
              <tr>
                <th>11月16日（土） </th>
                <td>14:00～15:30 </td>
                <td>【在校生による学校説明会】<br>
                  駒込中学校を生徒目線でご紹介！英語スピーチも披露<br>
                国語と算数について入試ポイントもお伝えします<br></td>
              </tr>
              <tr>
                <th>12月15日（日） </th>
                <td>10:00～12:00<br>
                14:00～16:00</td>
                <td>【受験生体験型】 過去問体験＆解説<br>
                  午前：私立型 午後：適性検査型<br>
                ※午前・午後ともSTEM入試 / 自己表現入試 体験会実施<br></td>
              </tr>
              <tr>
                <th>1月12日（日） </th>
                <td>10:00～12:30<br></td>
                <td>【受験生体験型】<br>
                  合格力UP入試ﾄﾗｲｱﾙ ― これをやれば10点UP ! ―<br>
                保護者の方には傾向と対策をお伝えします<br></td>
              </tr>
            </tbody>
        </table>
      </section>

      <section class="article-main">
        <div class="wrap">
          <h3>個別相談会<span>11/9、11/16、12/7以外予約不要</span></h3>
          <div class="two-column">
            <div class="box">
              <div class="text">
                <dl class="figure_circle_w flex_pc history wrap">
                  <dt>11月2日（土）</dt><dd>9:00～15:00</dd>
                  <dt>11月9日（土）</dt><dd>9:00～11:00 【※要予約 先着100名】</dd>
                  <dt>11月16日（土）</dt><dd>9:00～11:00 【※要予約 先着100名】</dd>
                  <dt>11月23日（土・祝）</dt><dd>9:00～15:00</dd>
                  <dt>11月30日（土）</dt><dd>9:00～15:00</dd>
                  <dt>12月7日（土）</dt><dd>9:00～11:00　 【※要予約 先着100名】</dd>
                </dl>
              </div>
            </div>
          </div>
          <h3>文化祭入試個別相談会<span>予約不要</span></h3>
          <div class="two-column">
            <div class="box">
              <div class="text">
                <dl class="figure_circle_w flex_pc history wrap">
                  <dt>9月21日（土）</dt><dd>10:30～15:00</dd>
                  <dt>9月22日（日）</dt><dd>9:00～15:00</dd>
                </dl>
              </div>
            </div>
          </div>
        </div>
      </section>

<section class="pagetop">
  <p>Page Top</p>
  <span class="arrow"></span>
</section>

      <section class="article-main mt10" id="high-school">
        <article>
          <h2>高校入試説明会</h2>
          <p>すべての説明会は約１ケ月前より予約を開始いたします。<br>説明会終了後、毎回個別相談会があります（希望者）<br>※説明会日程などは４月現在のものであり、変更になる場合があります。</p>
        </article>
      </section>

      <section class="article-main">
        <p class="entory_btn"><a href="https://mirai-compass.net/usr/komagmh/event/evtIndex.jsf" target="_blank">WEB予約はこちらから</a></p>
          <table class="table_2b">
            <thead>
              <tr>
                <th>日付</th>
                <th>時間</th>
                <th>説明会＆イベント</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <th>7月6日（土） </td>
                <td>14:30～16:00 </td>
                <td>【駒込高校を知ろう！】<br>
                  特S・Sコース/理系先進コース/国際教養コース<br>
                ３つのコース説明と入試について </td>
              </tr>
              <tr>
                <th>7月21日（日） </td>
                <td> 9:30～｜14:00～ </td>
                <td>【在校生は見た！駒込高校】<br>
                ３つのコースの説明と『在校生による学校紹介』 </td>
              </tr>
              <tr>
                <th>8月11日（日・祝） </td>
                <td>9:30～｜14:00～ </td>
                <td>【午前の部】<br>
                  特S・Sコースと国際教養コースの教育と入試について<br>
                  【午後の部】<br>
                特S・Sコースと理系先進コースの教育と入試について </td>
              </tr>
              <tr>
                <th>9月7日（土） </td>
                <td>14:30～16:00
                </td>
                <td><p>【こんな先生がいるよ！】</p>
                <p>先生が説明する駒込高校の魅力ある授業&amp;ICT教育</p></td>
              </tr>
              <tr>
                <th>10月5日（土） </td>
                <td>14:30～16:00 </td>
                <td>【在校生は見た！駒込高校 ~再び~】<br>
                駒込のグローバル教育とSTEM教育 </td>
              </tr>
              <tr>
                <th>11月9日（土） </td>
                <td>14:30～16:00 </td>
                <td>受験生必見！過去問解説 </td>
              </tr>
              <tr>
                <th>12月7日（土） </td>
                <td>14:30～16:00 </td>
                <td>５教科（英数国理社）直前対策 </td>
              </tr>
            </tbody>
        </table>
      </section>

      <section class="article-main">
        <div class="wrap">
          <h3>個別相談会<span>11/9、11/16、12/7以外予約不要</span></h3>
          <div class="two-column">
            <div class="box">
              <div class="text">
                <dl class="figure_circle_w flex_pc history wrap">
                  <dt>11月2日（土）</dt><dd>9:00～15:00</dd>
                  <dt>11月9日（土）</dt><dd>9:00～11:00 【※要予約 先着100名】</dd>
                  <dt>11月16日（土）</dt><dd>9:00～11:00 【※要予約 先着100名】</dd>
                  <dt>11月23日（土・祝）</dt><dd>9:00～15:00</dd>
                  <dt>11月30日（土）</dt><dd>9:00～15:00</dd>
                  <dt>12月7日（土）</dt><dd>9:00～11:00　 【※要予約 先着100名】</dd>
                </dl>
              </div>
            </div>
          </div>
          <h3>文化祭入試個別相談会<span>予約不要</span></h3>
          <div class="two-column">
            <div class="box">
              <div class="text">
                <dl class="figure_circle_w flex_pc history wrap">
                  <dt>9月21日（土）</dt><dd>10:30～15:00</dd>
                  <dt>9月22日（日）</dt><dd>9:00～15:00</dd>
                </dl>
              </div>
            </div>
          </div>
        </div>
      </section>

<section class="pagetop">
  <p>Page Top</p>
  <span class="arrow"></span>
</section>

    </main>

    <?php include '../footer.php'; ?>
<?php /*     <script src="../js/jquery.min.js"></script>
    <script src="../js/flexibility.js"></script>
    <script src="../js/common2.js" type="text/javascript"></script>*/?>
  </body>
</html>
