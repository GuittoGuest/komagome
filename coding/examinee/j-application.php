<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <title>中学募集要項｜駒込中学校・高等学校</title>
    <meta content="駒込中学校・高等学校" name="description">
    <meta http-equiv="Pragma" content="no-store">
    <meta http-equiv="Cache-Control" content="no-store">
    <meta http-equiv="Expires" content="0">
    <meta name="format-detection" content="telephone=no">

    <link href="../images/common/favicon.ico" rel="shortcut icon">
    <link href="../images/common/favicon.ico" rel="apple-touch-icon">
    <link href="../css/common.css" rel="stylesheet" type="text/css">
<?php //    <link href="../css/sub.css" rel="stylesheet" type="text/css"> ?>
    <link href="../css/sub2.css" rel="stylesheet" type="text/css">

  </head>

  <body>
    <?php include '../header.php'; ?>

    <main>
      <section class="mv header-title">
        <h1>中学募集要項</h1>
        <p>Junior High School Application</p>
      </section>

      <section class="article-main">
        <article>
          <h3>中学募集要項</h3>
          <p>2020年度入試の募集要項は詳細が決まり次第掲載いたします。</p>
        </article>
<?php /*
      </section>

      <section class="article-main">
        <h3>個別相談会（11/9、11/16、12/7以外予約不要）</h3>
          <div class="two-column">
            <div class="box">
              <div class="text">
                <ol class="figure_circle_w">
                  <li>11月2日（土）　9:00～15:00</li>
                  <li>11月9日（土）　9:00～11:00 【※要予約 先着100名】</li>
                  <li>11月16日（土）　9:00～11:00 【※要予約 先着100名】</li>
                  <li>11月23日（土・祝）　9:00～15:00</li>
                  <li>11月30日（土）　9:00～15:00</li>
                  <li>12月7日（土）　9:00～11:00　 【※要予約 先着100名】</li>
                </ol>
              </div>
            </div>
          </div>
      </section>

      <section class="article-main">
          <h3>文化祭入試個別相談会（予約不要）</h3>
          <div class="two-column">
            <div class="box">
              <div class="text">
                <ol class="figure_circle_w">
                  <li>9月21日（土）　10:30～15:00</li>
                  <li>9月22日（日）　9:00～15:00</li>
                </ol>
              </div>
            </div>
          </div>
*/?>
      </section>


<section class="pagetop">
  <p>Page Top</p>
  <span class="arrow"></span>
</section>

    </main>

    <?php include '../footer.php'; ?>
<?php /*    <script src="../js/jquery.min.js"></script>
    <script src="../js/flexibility.js"></script>
    <script src="../js/common2.js" type="text/javascript"></script>*/?>
  </body>
</html>
