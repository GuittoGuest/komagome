<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <title>サイトマップ | 駒込中学校・高等学校</title>
    <meta content="駒込中学校・高等学校" name="description">
    <meta http-equiv="Pragma" content="no-store">
    <meta http-equiv="Cache-Control" content="no-store">
    <meta http-equiv="Expires" content="0">
		<meta name="format-detection" content="telephone=no">

    <link href="/images/common/favicon.ico" rel="shortcut icon">
    <link href="/images/common/favicon.ico" rel="apple-touch-icon">
    <link href="/css/common.css" rel="stylesheet" type="text/css">
    <link href="/css/sub2.css" rel="stylesheet" type="text/css">
  <body id="access">
    <?php include './header.php'; ?>

    <main>
    <section class="mv header-title">
				<h1>サイトマップ</h1>
        <p>site map</p>
      </section>

      <section class="article-main">
          <h3>トップページ</h3>
          <div class="two-column">
            <div class="box">
              <div class="text">
                  <ul class="disc">
                    <li><a href="<?=$root; ?>/">Top of top</a></li>
                    <li><a href="<?=$root; ?>/top">総合トップページ</a></li>
                  </ul>
              </div>
            </div>
          </div>

          <h3>ニュース</h3>
          <div class="two-column">
            <div class="box">
              <div class="text">
                  <ul class="disc">
                    <li><a href="<?=$root; ?>/news/">ニュース記事一覧</a></li>
                  </ul>
              </div>
            </div>
          </div>


          <h3>学園の紹介</h3>
          <div class="two-column">
            <div class="box">
              <div class="text">
                  <ul class="disc">
                    <li><a href="<?=$root;?>/introduction/philosophy.php">教育理念・教育方針</a></li>
                    <li><a href="<?=$root;?>/introduction/message.php">校長挨拶</a></li>
                    <li><a href="<?=$root;?>/introduction/history.php">学校沿革</a></li>
                  </ul>
              </div>
            </div>
          </div>

          <h3>駒込の教育</h3>
          <div class="two-column">
            <div class="box">
              <div class="text">
                  <ul class="disc">
                    <li><a href="<?=$root;?>/education/global.php">グローバル教育</a></li>
                    <li><a href="<?=$root;?>/education/ict.php">ICT教育</a></li>
                    <li><a href="<?=$root;?>/education/support.php">学習支援</a></li>
                    <li><a href="<?=$root;?>/education/stem.php">STEM教育</a></li>
                    <li><a href="<?=$root;?>/education/human.php">人間教育</a></li>
                  </ul>
              </div>
            </div>
          </div>

          <h3>中学校</h3>
          <div class="two-column">
            <div class="box">
              <div class="text">
                  <ul class="disc">
                    <li><a href="<?=$root;?>/junior-high-school/program.php">6ヵ年一貫教育プログラム</a></li>
                  </ul>
              </div>
            </div>
          </div>

          <h3>高等学校</h3>
          <div class="two-column">
            <div class="box">
              <div class="text">
                  <ul class="disc">
                    <li><a href="<?=$root;?>/high-school/course.php">3コース制</a></li>
                  </ul>
              </div>
            </div>
          </div>

          <h3>学校生活</h3>
          <div class="two-column">
            <div class="box">
              <div class="text">
                  <ul class="disc">
                    <li><a href="<?=$root;?>/school-life/uniform.php">制服紹介</a></li>
                    <li><a href="<?=$root;?>/school-life/facility.php">施設紹介</a></li>
                    <li><a href="<?=$root;?>/school-life/event.php">学校行事</a></li>
                    <li><a href="<?=$root;?>/school-life/club.php">クラブ紹介</a></li>
                  </ul>
              </div>
            </div>
          </div>

          <h3>進路情報</h3>
          <div class="two-column">
            <div class="box">
              <div class="text">
                  <ul class="disc">
                    <li><a href="<?=$root;?>/course/data.php">進路データ</a></li>
          					<li><a href="<?=$root;?>/course/succeed.php">卒業生の活躍（準備中）</a></li>
                  </ul>
              </div>
            </div>
          </div>

          <h3>受験生の方へ</h3>
          <div class="two-column">
            <div class="box">
              <div class="text">
                  <ul class="disc">
                    <li><a href="<?=$root;?>/examinee/guidance.php">入試説明会案内</a></li>
          					<li><a href="<?=$root; ?>/examinee/j-application.php">中学募集要項</a></li>
          					<li><a href="<?=$root; ?>/examinee/h-application.php">高校募集要項</a></li>
          					<li><a href="<?=$root;?>/examinee/internet.php">インターネット出願</a></li>
          					<li><a href="<?=$root;?>/examinee/movie.php">KOMAGOME CHANNEL</a></li>
          					<li><a href="<?=$root;?>/examinee/kakomon.php">過去問題紹介（準備中）</a></li>
                  </ul>
              </div>
            </div>
          </div>

          <h3>その他</h3>
          <div class="two-column">
            <div class="box">
              <div class="text">
                  <ul class="disc">
                    <li><a href="<?=$root;?>/graduates/office.php">事務室</a></li>
            				<li><a href="<?=$root;?>/students-guardian/dispensary.php">保健室</a></li>
            				<li><a href="<?=$root;?>/employment//">採用情報</a></li>
            				<li><a href="<?=$root;?>/access.php">アクセス</li>
							<!-- 公開当初プライバシーポリシーは非公開のため一旦コメント	
                            <li><a href="<?=$root;?>/privacy.php">プライバシーポリシー</a></li> -->
            				<li><a href="<?=$root;?>/sitemap.php">サイトマップ</a></li>
                  </ul>
              </div>
            </div>
          </div>
			</section>

<section class="pagetop">
  <p>Page Top</p>
  <span class="arrow"></span>
</section>

    </main>

    <?php include './footer.php'; ?>
 <?php /*   <script src="./js/jquery.min.js"></script>
    <script src="./js/flexibility.js"></script>
    <script src="./js/common.js" type="text/javascript"></script>
    <script>
    $(document).ready(function(){
      $(function(){
        var ua = navigator.userAgent;
        if((ua.indexOf('iPhone') > 0) || ua.indexOf('iPod') > 0 || (ua.indexOf('Android') > 0 && ua.indexOf('Mobile') > 0)){
            $('head').prepend('<meta name="viewport" content="width=device-width,initial-scale=1">');
        } else {
            $('head').prepend('<meta name="viewport" content="width=1320">');
        }
      });
    });
    </script>*/?>
  </body>
</html>
