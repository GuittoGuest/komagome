<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <title>駒込中学・高等学校</title>
    <meta content="駒込中学校・高等学校" name="description">
    <meta http-equiv="Pragma" content="no-store">
    <meta http-equiv="Cache-Control" content="no-store">
    <meta http-equiv="Expires" content="0">
		<meta name="format-detection" content="telephone=no">

    <link href="./images/common/favicon.ico" rel="shortcut icon">
    <link href="./images/common/favicon.ico" rel="apple-touch-icon">
    <link href="./css/common.css" rel="stylesheet" type="text/css">
    <link href="./css/sub.css" rel="stylesheet" type="text/css">

  </head>

  <body>
    <?php include './header.php'; ?>

    <main>
      <section class="mv">
				<h1>h1タイトル</h1>
      </section>
			<section class="sub-menu">
				<ul>
					<a href=""><li class="-current">英語４技能</li></a>
					<a href=""><li>ICT教育</li></a>
					<a href=""><li>グローバル</li></a>
					<a href=""><li>キャリアデザイン</li></a>
					<a href=""><li>学習支援</li></a>
					<a href=""><li>STEM教育</li></a>
				</ul>
			</section>

			<section class="article-main">
				<article>
					<h2>英検4技能</h2>
					<p>理系先進コースでは、世界の理数教育の主流である「STEM教育」を基盤に、到来するAI時代に即した「考える」授業を展開し、数学と理科の専門的な力を伸ばしていきます。</p>
					<h3>理系のリーダーの育成</h3>
					<p>科学技術が進歩し社会が発展するに伴い、社会の抱える問題も高度かつ複雑になっていきます。これからの「理系のリーダー」には問題を俯瞰的に捉え、様々な切り口で考えて解決策を練る姿勢が求められます。そこで本校では、STEM教育【Science（ 科学）、Technology（技術）、Engineering（工学）、Mathematics（数学）を統合した教育】を導入し、国内はもとより、世界の抱える諸問題を解決できる人材の育成に努めていきます。</p>
					<h3>STEM教育とは</h3>
					<p>科学、技術、工学、数学の各分野を「相互横断的」に学び、学んだことを「実際の生活に応用」することを目的とした教育です。各科目を並列的に相互に関連したものとして学び、実践することで、明確な答えのない問題を多角的に考え、解決法を探る力を養います。</p>
					<h3>STEM教育実践</h3>
					<p>国内のSTEM教育の総本山である、埼玉大学STEM教育研究センターとの共同授業を行います。高校3年間通してのSTEM教育は日本初の試みです。その授業を軸としつつも理系科目はもちろん、文系科目においてもSTEM的な思考を取り入れ、様々な分野に関する深い見識とそれらを組み合わせる応用力、および社会的な倫理観を持った人材を育成します。</p>
				</article>
			</section>


    </main>

    <?php include './footer.php'; ?>
    <script src="./js/jquery.min.js"></script>
    <script src="./js/flexibility.js"></script>
    <script src="./js/common.js" type="text/javascript"></script>
    <script>
    $(document).ready(function(){
      /* タブレット用ビューポート */
      $(function(){
        var ua = navigator.userAgent;
        if((ua.indexOf('iPhone') > 0) || ua.indexOf('iPod') > 0 || (ua.indexOf('Android') > 0 && ua.indexOf('Mobile') > 0)){
            $('head').prepend('<meta name="viewport" content="width=device-width,initial-scale=1">');
        } else {
            $('head').prepend('<meta name="viewport" content="width=1320">');
        }
      });
    });
    </script>
  </body>
</html>
