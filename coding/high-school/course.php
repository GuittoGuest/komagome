<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <title>3コース制｜駒込中学校・高等学校</title>
    <meta content="駒込中学校・高等学校" name="description">
    <meta http-equiv="Pragma" content="no-store">
    <meta http-equiv="Cache-Control" content="no-store">
    <meta http-equiv="Expires" content="0">
		<meta name="format-detection" content="telephone=no">

    <link href="../images/common/favicon.ico" rel="shortcut icon">
    <link href="../images/common/favicon.ico" rel="apple-touch-icon">
    <link href="../css/common.css" rel="stylesheet" type="text/css">
<?php //    <link href="../css/sub.css" rel="stylesheet" type="text/css"> ?>
    <link href="../css/sub2.css" rel="stylesheet" type="text/css">

  </head>

  <body id="h-application">
    <?php include '../header.php'; ?>

    <main>
      <section class="mv header-title">
				<h1>3コース制</h1>
        <p>High School Briefing</p>
      </section>

      <section class="article-main">
        <article>
          <h2>次世代育成教育の実践</h2>
          <p>生徒たちそれぞれがこれからの時代に対応したライフデザインを描いていけるよう、<br class="pc">本校では「国際教養コース」「理系先進コース」「特S・Sコース」を設置し、<br class="pc">のコースでも「ディープラーニング」を積極的に取り入れた教育を行っています。</p>
        </article>
        <div class="two-column">
          <div class="box">
            <div class="text">
              <h4>Sコース<span>（医薬獣農法経社系/国公立難関私大）高2より文理選択</span></h4>
              <p>各分野に興味があり、これから自分の適性をみつけたい生徒さんにおすすめです。高1では文理科目双方を広く学び、高2からは選択となるカリキュラム。多彩なゼミ演習で力を蓄え、思い描く未来を実現できます。</p>
            </div>
            <p class="image fr"><img src="../images/high-school/h-application1.jpg"></p>
          </div>
        </div>

        <div class="two-column">
          <div class="box reverse">
            <div class="text">
              <h4>国際教養コース<span>（法経商社人文系/海外代・東外大・国際教養大・早慶上理・ICUほか）</span></h4>
              <p>国際教養コースでは、世界標準カリキュラム（IB的要素を兼ね備えた国際カリキュラム）によって論理的思考力とプレゼンテーション能力を高める新時代対応の教育を目指した授業を展開しています。</p>
            </div>
            <p class="image"><img src="../images/high-school/h-application2.jpg"></p>
          </div>
        </div>

        <div class="two-column">
          <div class="box">
            <div class="text">
              <h4>理系先進コース<span>（理工医薬農系/東工大・筑波大・千葉大・早慶理・私大医学部ほか）</span></h4>
              <p>理系先進コースでは、世界の理数教育の主流である「STEM教育」を基盤に、到来するAI時代に即した「考える」授業を展開し、数学と理科の専門的な力を伸ばしていきます。</p>
            </div>
            <p class="image"><img src="../images/high-school/h-application3.jpg"></p>
          </div>
        </div>
			</section>

<section class="pagetop">
  <p>Page Top</p>
  <span class="arrow"></span>
</section>

    </main>

    <?php include '../footer.php'; ?>
<?php /*    <script src="../js/jquery.min.js"></script>
    <script src="../js/flexibility.js"></script>
    <script src="../js/common.js" type="text/javascript"></script>*/?>
    <script>
    $(document).ready(function(){
      /* タブレット用ビューポート */
      $(function(){
        var ua = navigator.userAgent;
        if((ua.indexOf('iPhone') > 0) || ua.indexOf('iPod') > 0 || (ua.indexOf('Android') > 0 && ua.indexOf('Mobile') > 0)){
            $('head').prepend('<meta name="viewport" content="width=device-width,initial-scale=1">');
        } else {
            $('head').prepend('<meta name="viewport" content="width=1320">');
        }
      });
    });
    </script>
  </body>
</html>
