<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <title>駒込中学校・高等学校</title>
    <meta content="駒込中学校・高等学校" name="description">
    <meta http-equiv="Pragma" content="no-store">
    <meta http-equiv="Cache-Control" content="no-store">
    <meta http-equiv="Expires" content="0">
		<meta name="format-detection" content="telephone=no">

    <link href="./images/common/favicon.ico" rel="shortcut icon">
    <link href="./images/common/favicon.ico" rel="apple-touch-icon">
    <link href="./css/common.css?ver0708" rel="stylesheet" type="text/css">
    <link href="./css/index2.css" rel="stylesheet" type="text/css">
    <link href="./owl/owl.carousel.min.css" rel="stylesheet" type="text/css">
  </head>

  <body>
    <header>
      <img src="./images/common/header.png">
    </header>

    <main>
      <section class="slider">
        <div class="flame" id="s4">
          <div class="catch"><img src="./images/index/catch4.png"></div>
          <div class="circle"><img src="./images/index/circle4.png"></div>
          <div class="image"></div>
          <div class="tab"><span>学習支援</span></div>
        </div>
        <div class="flame" id="s3">
          <div class="catch"><img src="./images/index/catch3.png"></div>
          <div class="circle"><img src="./images/index/circle3.png"></div>
          <div class="tab"><span>STEM教育</span></div>
          <div class="image"></div>
        </div>
        <div class="flame" id="s2">
          <div class="catch"><img src="./images/index/catch2.png"></div>
          <div class="circle"><img src="./images/index/circle2.png"></div>
          <div class="image"></div>
          <div class="tab"><span>ICT教育</span></div>
        </div>
        <div class="flame" id="s1">

          <div class="catch"><img src="./images/index/catch1.png"></div>
          <div class="circle"><img src="./images/index/circle1.png"></div>
          <div class="image"></div>
          <div class="tab"><span>グローバル教育</span></div>
        </div>
      </section>
      <section class="sp-slider">
        <ul class="owl-carousel">
          <li><img src="./images/index/s1-sp.png"></li>
          <li><img src="./images/index/s2-sp.png"></li>
          <li><img src="./images/index/s3-sp.png"></li>
          <li><img src="./images/index/s4-sp.png"></li>
        </ul>
      </section>
      <nav>
        <div class="left">
          <a href="">
            <div class="strip">
              中学入試説明会
            </div>
          </a>
          <a href="">
            <div class="strip">
              高校入試説明会
            </div>
          </a>
        </div>
        <div class="right">
          <a href="./top">
            <div class="strip">
              TOPページはこちら
            </div>
          </a>
        </div>
      </nav>
    </main>


    <script src="./js/jquery.min.js"></script>
    <script src="./js/flexibility.js"></script>
    <script src="./js/common.js?0708" type="text/javascript"></script>
    <script src="./owl/owl.carousel.min.js"></script>
    <script>
    $(document).ready(function(){
      /* タブレット用ビューポート */
      $(function(){
        var ua = navigator.userAgent;
        if((ua.indexOf('iPhone') > 0) || ua.indexOf('iPod') > 0 || (ua.indexOf('Android') > 0 && ua.indexOf('Mobile') > 0)){
            $('head').prepend('<meta name="viewport" content="width=device-width,initial-scale=1">');
        } else {
            $('head').prepend('<meta name="viewport" content="width=1320">');
        }
      });

      var userAgent = window.navigator.userAgent.toLowerCase();
      var isIE = (userAgent.indexOf('msie') >= 0 || userAgent.indexOf('trident') >= 0 || userAgent.indexOf('edge') != -1);


      var time ="2500"

      if(isIE) {
        var width = $(window).innerWidth();
        var s1current = $('#s1').position().left;
        var s2current = $('#s2').position().left;
        var s3current = $('#s3').position().left;
        var s1swipe = 100 - width;
        var s2swipe = 125 - width;
        var s3swipe = 150 - width;
        var slide = function(time){
          $('#s1').addClass('-current -scale');
          setTimeout(function(){$('#s1').css('left', s1swipe+'px')},time);
          setTimeout(function(){$('#s1').removeClass('-current')},time);
          setTimeout(function(){$('#s2').addClass('-scale -current')},time);
          setTimeout(function(){$('#s2').css('left', s2swipe+'px')},time*3);
          setTimeout(function(){$('#s2').removeClass('-current')},time*3);
          setTimeout(function(){$('#s1').removeClass('-scale')},time*3);
          setTimeout(function(){$('#s3').addClass('-scale -current')},time*3);
          setTimeout(function(){$('#s3').css('left', s3swipe+'px')},time*5);
          setTimeout(function(){$('#s3').removeClass('-current')},time*5);
          setTimeout(function(){$('#s2').removeClass('-scale')},time*5);
          setTimeout(function(){$('#s4').addClass('-scale -current')},time*5);
          setTimeout(function(){$('#s3').removeClass('-scale')},time*6);
          setTimeout(function(){$('#s1').css('left', s1current+'px')},time*7);
          setTimeout(function(){$('#s2').css('left', s2current+'px')},time*7);
          setTimeout(function(){$('#s3').css('left', s3current+'px')},time*7);
          setTimeout(function(){$('#s4').removeClass('-current')},time*7);
          setTimeout(function(){$('#s4').removeClass('-scale')},time*8);
          setTimeout(function(){},time*9);
        };

      }else{

        var slide = function(time){
          $('#s1').addClass('-current -scale');
          setTimeout(function(){$('#s1').addClass('-swiped')},time);
          setTimeout(function(){$('#s1').removeClass('-current')},time);
          setTimeout(function(){$('#s2').addClass('-scale -current')},time);
          setTimeout(function(){$('#s2').addClass('-swiped')},time*3);
          setTimeout(function(){$('#s2').removeClass('-current')},time*3);
          setTimeout(function(){$('#s1').removeClass('-scale')},time*3);
          setTimeout(function(){$('#s3').addClass('-scale -current')},time*3);
          setTimeout(function(){$('#s3').addClass('-swiped')},time*5);
          setTimeout(function(){$('#s3').removeClass('-current')},time*5);
          setTimeout(function(){$('#s2').removeClass('-scale')},time*5);
          setTimeout(function(){$('#s4').addClass('-scale -current')},time*5);
          setTimeout(function(){$('#s3').removeClass('-scale')},time*6);
          setTimeout(function(){$('#s1,#s2,#s3').removeClass('-swiped')},time*7);
          setTimeout(function(){$('#s4').removeClass('-current')},time*7);
          setTimeout(function(){$('#s4').removeClass('-scale')},time*8);
          setTimeout(function(){},time*9);
        };
      }
      slide(time);
      setInterval(slide, time*8, time);

      /* SPスライダー用記述 */
      $(".owl-carousel").owlCarousel({
        loop: true,
        nav: true,
        items : 1,
        autoplay: true,
        slideSpeed : 1000,
        dots : true,
        rewind: false,
        autoplayHoverPause: true,
        center: true,
        navigationText: [
    '<i class="icon-chevron-left icon-white"><img src="./images/index/prev.png"></i>',
    '<i class="icon-chevron-left icon-white"><img src="./images/index/next.png"></i>',
    ]
      });
    });
    </script>
  </body>
</html>
