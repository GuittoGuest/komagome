<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <title>STEM教育｜駒込中学校・高等学校</title>
    <meta content="駒込中学校・高等学校" name="description">
    <meta http-equiv="Pragma" content="no-store">
    <meta http-equiv="Cache-Control" content="no-store">
    <meta http-equiv="Expires" content="0">
		<meta name="format-detection" content="telephone=no">

    <link href="../images/common/favicon.ico" rel="shortcut icon">
    <link href="../images/common/favicon.ico" rel="apple-touch-icon">
    <link href="../css/common.css" rel="stylesheet" type="text/css">
<?php //    <link href="../css/sub.css" rel="stylesheet" type="text/css"> ?>
    <link href="../css/sub2.css" rel="stylesheet" type="text/css">

  </head>

  <body id="stem">
    <?php include '../header.php'; ?>

    <main>
      <section class="mv header-title">
				<h1>STEM教育</h1>
        <p>STEM</p>
      </section>

			<section class="article-main">
				<article>
					<h2>STEM教育とは</h2>
<?php //					<p>科学、技術、工学、数学の各分野を「相互横断的」に学び、<br class="pc">学んだことを「実際の生活に応用」することを目的とした教育です。<br>各科目を並列的に相互に関連したものとして学び、実践することで、<br class="pc">明確な答えのない問題を多角的に考え、解決法を探る力を養います。</p>?>
          <p>日本の代表的なSTEM教育研究機関である埼玉大学STEM教育研究センターとの共同授業を行います。<br>生徒たちは、オリジナルキットとソフトを使ったプログラミングで、身近な課題を実践的に解決していきます。<br>外部コンテストにも積極的に参加し、2018年には有志生徒が<br class="pc">「世界の人口増加に伴う食糧不足を解決する『農業支援ロボット』」を制作し、全国大会に出場。<br>また、1年生は「国際STEM CAMP」というフィールドワークを通じて、<br class="pc">海外の学生たちとともに被災地の課題を解決するロボットプログラミングに取り組んでいます。</p>
        </article>
      </section>
      <section class="article-main" id="program">
        <div class="two-column">

          <div class="box">
            <div class="text">
              <h4>STEM教育実践</h4>
 <?php //             <p>国内のSTEM教育の総本山である、埼玉大学STEM教育研究センターとの共同授業を行います。高校3年間通してのSTEM教育は日本初の試みです。<br>その授業を軸としつつも理系科目はもちろん、文系科目においてもSTEM的な思考を取り入れ、様々な分野に関する深い見識とそれらを組み合わせる応用力、および社会的な倫理観を持った人材を育成します。</p>?>
              <p>日本の代表的なSTEM教育研究機関である埼玉大学STEM教育研究センターとの共同授業を行います。生徒たちは、オリジナルキットとソフトを使ったプログラミングで、身近な課題を実践的に解決していきます。外部コンテストにも積極的に参加し、2018年には有志生徒が「世界の人口増加に伴う食糧不足を解決する『農業支援ロボット』」を制作し、全国大会に出場。また、1年生は「国際STEM CAMP」というフィールドワークを通じて、海外の学生たちと共に被災地の課題を解決するロポットプログラミングに取り組んでいます。</p>
            </div>

            <div class="image">
              <img src="../images/education/stem1.jpg">
            </div>
          </div>
        </div>

        <div class="voice">
          <div class="two-column">
            <div class="box">
              <div class="media_image">
                <img src="../images/education/stem2.jpg">
              </div>
              <div class="media">
                <p class="see"><a href="#movie1" class="modal">Watch the Movie</a></p>
              </div>
            </div>
          </div>
        </div>
      </section>



<section class="pagetop">
  <p>Page Top</p>
  <span class="arrow"></span>
</section>


    </main>

    <?php include '../footer.php'; ?>
    <script src="../js/jquery.min.js"></script>
    <script src="../js/flexibility.js"></script>
    <script src="../js/common2.js" type="text/javascript"></script>

    <div class="modal_contents">
      <!- modal表示内容2（離れた場所に置くバージョン） -->
      <div id="movie1"><video src="../images/education/stem_mv1.mp4"></video><p>Movie</p></div>
      <!- /modal表示内容2（離れた場所に置くバージョン） -->
    </div>
  </body>
</html>
