<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <title>クラブ活動｜駒込中学校・高等学校</title>
    <meta content="駒込中学校・高等学校" name="description">
    <meta http-equiv="Pragma" content="no-store">
    <meta http-equiv="Cache-Control" content="no-store">
    <meta http-equiv="Expires" content="0">
		<meta name="format-detection" content="telephone=no">

    <link href="../images/common/favicon.ico" rel="shortcut icon">
    <link href="../images/common/favicon.ico" rel="apple-touch-icon">
    <link href="../css/common.css" rel="stylesheet" type="text/css">
<?php //    <link href="../css/sub.css" rel="stylesheet" type="text/css"> ?>
    <link href="../css/sub2.css" rel="stylesheet" type="text/css">
    <link href="../owl/owl.carousel.min.css" rel="stylesheet" type="text/css">

  </head>

  <body id="club">
    <?php include '../header.php'; ?>

    <main>
		<section class="mv header-title">
		<h1>クラブ活動</h1>
		<p>Clubs</p>
		</section>

		<section class="article-main">
			<article>
				<h2>努力の先にあるのは、勝利だけじゃない。<br>仲間と喜怒哀楽を分かち合いながら<br>自分らしいフィールドで輝く。</h2>
				<div class="sub-menu mt5"><a href="#sports">運動部</a><a href="#culture">文化部</a></div>
			</article>
		</section>

<section class="article-main" id="sports">
  <div class="club-column">
    <h3>運動部</h3>
	<div class="box">
		<div class="text">
			<h4>高校野球部</h4>
			<p>都ベスト8進出も果たし、甲子園を夢に勇ましくさわやかに活動しています。</p>
			<p><a href="https://ameblo.jp/komagomebaseball/" target="_blank">ブログ</a><a href="https://mobile.twitter.com/kmgmbaseball?lang=ja" target="_blank">Twitter</a></p>
		</div>
		<img src="../images/school-life/sports1.png">
		<span>目指すは甲子園出場。<br>全員が一丸となって正々堂々と勝負します！</span>
	</div>

	<div class="box">
		<div class="text">
  			<h4>中学野球部</h4>
  			<p>一人ひとりが熱い気持ちを常に持ち、チーム一丸となった全員野球をモットーに一瞬の輝きを求めて活動しています。</p>
  			<p><a href="https://komagomejhs-bb.jimdo.com/" target="_blank">ホームページ </a></p>
		</div>
		<img src="../images/school-life/sports2.png">
		<span>野球が好きな人はぜひ入部してください！<br>女子も歓迎です！</span>
	</div>

	<div class="box">
		<div class="text">
  			<h4>柔道部</h4>
  			<p>部員は非常に仲良く、先輩はとても面倒見がいいです。是非一緒に楽しく練習し、強くなりましょう！</p>
  			<p><a href="https://www.youtube.com/embed/noSTZfG-rxM" class="modal">夏合宿2015</a><a href="https://www.youtube.com/embed/Hym4GURjRc0" class="modal">紹介ムービー</a></p>
      	</div>
		<img src="../images/school-life/sports3.png">
			<span>柔道を通じて心と体を強くしたい人、優しい先輩たちと頑張りましょう！</span>
		</div>

		<div class="box">
      	<div class="text">
			  <h4>剣道部</h4>
  			<p>経験者だけでなく初心者も大歓迎。剣道に興味のある人は地下1階の勧学ホールを覗いてください。</p>
  			<ul class="disc fs0.8">
				<li><p><a href="https://www.komagome.ed.jp/schoollife/data/2018kendo-02.pdf" target="_blank">駒込学園杯争奪少年剣道大会 日程</a></p></li>
				<li><p><a href="https://www.komagome.ed.jp/schoollife/data/2018kendo-03.pdf" target="_blank">フリーオーダー制による試合方法について</a></p></li>
				<li><p><a href="https://www.komagome.ed.jp/schoollife/data/2018-001.xlsx" target="_blank">選手登録・参加申込用紙</a></p></li>
				<li><p><a href="https://www.komagome.ed.jp/schoollife/data/2017kendo-04.pdf" target="_blank">第7回大会結果 </a></p></li>
			</ul>
      	</div>
		<img src="../images/school-life/sports4.png">
			<span>礼儀を大切にし、互いに切磋琢磨しながら日々稽古に励んでいます。</span>
		</div>

		<div class="box">
      	<div class="text">
  			<h4>陸上部</h4>
  			<p>
  				個々人を尊重した団結に価値があります。実力上昇中。
  			</p>
      	</div>
		<img src="../images/school-life/sports5.png">
			<span>自己ベスト更新を目指して、毎日練習しています！</span>
		</div>

		<div class="box">
      	<div class="text">
  			<h4>卓球部</h4>
  			<p>
  				卓球が好きな人なら誰でも入部OK。一度練習を見に来てください。
  			</p>
      	</div>
		<img src="../images/school-life/sports6.png">
			<span>みんなで和気あいあいと楽しい雰囲気です！</span>
		</div>

		<div class="box">
      	<div class="text">
  			<h4>バスケットボール部（男子）</h4>
  			<p></p>
      	</div>
		<img src="../images/school-life/sports7_3.png">
			<span>一歩一歩勝利に結び付けるようにがんばっています！</span>
		</div>

		<div class="box">
      	<div class="text">
  			<h4>バスケットボール部（女子）</h4>
  			<p>
  				コーチの指導の下、公式戦に向けて一つでも多く勝ち進むことが出来るよう練習しています。
  			</p>
      	</div>
		<img src="../images/school-life/sports7.png">
			<span>部員が増えてきて、同級生同士だけでなく先輩後輩の仲もいい部活です。</span>
		</div>

		<div class="box">
      	<div class="text">
  			<h4>中学バスケットボール部（男子）</h4>
  			<p></p>
      	</div>
		<img src="../images/school-life/sports7_1.png">
			<span>限られた時間の中で効率よく練習をできるように心がけています。</span>
		</div>

		<div class="box">
      	<div class="text">
  			<h4>中学バスケットボール部（女子）</h4>
  			<p></p>
      	</div>
		<img src="../images/school-life/sports7_2.png">
			<span>基本から応用までそれぞれの技量に合わせて練習を行っています。</span>
		</div>

		<div class="box">
      	<div class="text">
  			<h4>バレーボール部（男子）</h4>
  			<p>
  				少人数ですが日々の練習を重ね、大会で勝てるよう頑張っています。
  			</p>
      	</div>
		<img src="../images/school-life/sports8.png">
			<span>初心者、経験者問わずバレーを楽しんでいます。</span>
		</div>

		<div class="box">
      	<div class="text">
  			<h4>バレーボール部（女子）</h4>
  			<p></p>
      	</div>
		<img src="../images/school-life/sports8_1.png">
			<span>運動が好きな人、バレーが好きな人、大歓迎です！</span>
		</div>

		<div class="box">
      	<div class="text">
  			<h4>硬式テニス部（男子）</h4>
  			<p>
  				経験者はもちろん、初心者でも丁寧に教えます。ぜひ、一緒に汗を流しましょう。
  			</p>
      	</div>
		<img src="../images/school-life/sports9.png">
			<span>一緒にテニスでいい汗かきましょう！</span>
		</div>

		<div class="box">
      	<div class="text">
  			<h4>硬式テニス部（女子）</h4>
  			<p></p>
      	</div>
		<img src="../images/school-life/sports9_2.png">
			<span>基本練習から試合まで、皆で仲良く練習しています。</span>
		</div>

		<div class="box">
      	<div class="text">
  			<h4>中学硬式テニス部</h4>
  			<p></p>
      	</div>
		<img src="../images/school-life/sports9_1.png">
			<span>夏は合宿もあり、部員同士の仲も深まります。</span>
		</div>

		<div class="box">
      	<div class="text">
  			<h4>登山・スキー部</h4>
  			<p>毎月の日帰り登山と夏や秋の縦走を通して、自然とふれあい自己と向きあい友情の絆を深めます。</p>
  			<p><a href="http://www.yamareco.com/modules/yamareco/grpinfo.php?cid=323" target="_blank">ヤマレコ</a></p>
      	</div>
		<img src="../images/school-life/sports10.png">
			<span>兼部もOK。山ボーイ、山ガール集まれ！</span>
		</div>

		<div class="box">
      	<div class="text">
  			<h4>バドミントン部</h4>
  			<p>
  技術を獲得する歓びと、協力し合って活動する歓びを感じられます。
  			</p>
      	</div>
		<img src="../images/school-life/sports11_1.png">
			<span>男女ともに仲良く楽しく活動しています。</span>
		</div>

		<div class="box">
      	<div class="text">
  			<h4>中学バドミントン部</h4>
  			<p></p>
      	</div>
		<img src="../images/school-life/sports11.png">
			<span>私たちとバドミントンに燃える青春を過ごしませんか？</span>
		</div>

		<div class="box">
      	<div class="text">
  			<h4>アメリカン<br>フットボール部</h4>
  			<p>
  都内でも珍しいアメフト部。格闘スポーツで心身を鍛えてみては。
  			</p>
      	</div>
		<img src="../images/school-life/sports12.png">
			<span>ほとんどの人が高校から始めるので、スタートラインはみんな同じです。</span>
		</div>

		<div class="box">
      	<div class="text">
  			<h4>高校サッカー部</h4>
  			<p>初心者でも大丈夫。チームワークを学び共に楽しみましょう。</p>
  			<p><a href="http://komagomefc-u18.jimbo.com" target="_blank">ホームページ</a></p>
      	</div>
		<img src="../images/school-life/sports13.png">
			<span>サッカーが好きな人はぜひ一度見学に来てください！</span>
		</div>

		<div class="box">
      <div class="text">
  			<h4>中学サッカー部</h4>
  			<p>地区大会優勝を目標にし、支部大会や都大会出場を果たす。【一生懸命】【不撓不屈】の精神で頑張っています。未経験者も歓迎します。</p>
  			<p><a href="http://komagomefc-u18.jimdo.com/ｔｅａｍ-ｕ-15/" target="_blank">ホームページ</a></p>
      </div>
		<img src="../images/school-life/sports14.png">
			<span>技術面だけでなく、精神面も成長できるよう日々練習を積んでいます。</span>
		</div>

		<div class="box">
      <div class="text">
  			<h4>ラグビー部</h4>
  			<p>
  スポーツ未経験者や運動に自信がない人、勇気や仲間を作りたい人など大歓迎です。
  			</p>
      </div>
		<img src="../images/school-life/sports15.png">
			<span>私たちは仲間を大切にすることをモットーとしています。</span>
		</div>

		<div class="box">
      <div class="text">
  			<h4>中学ラグビー部</h4>
  			<p><br><br></p>
      </div>
		<img src="../images/school-life/sports15_1.png">
			<span>ぜひ私たちとラグビーをやりましょう！</span>
		</div>

		<div class="box">
      <div class="text">
  			<h4>ハンドボール部</h4>
  			<p></p>
      </div>
		<img src="../images/school-life/sports17.png">
			<span>チーム一丸となって勝利を目指し、練習を積み重ねています。</span>
		</div>

		<div class="box">
      <div class="text">
  			<h4>ダンス同好会</h4>
  			<p></p>
      </div>
		<img src="../images/school-life/sports16.png">
			<span>みんなで楽しくダンスをしています！</span>
		</div>

		<div class="box">
      <div class="text">
  			<h4>軟式野球同好会</h4>
  			<p></p>
      </div>
			<!--<img src="../images/school-life/clubdummy.png">-->
			<span>気になる方は一度ぜひ見学へ！</span>
		</div>
	  </div>
	</section>

<section class="pagetop">
  <p>Page Top</p>
  <span class="arrow"></span>
</section>

	<section class="article-main" id="culture">
	  <div class="club-column">
	  <h3>文化部</h3>
		<div class="box">
      <div class="text">
  			<h4>吹奏楽部</h4>
  			<p>コンサートやコンクールに向け、日々練習を重ねています。<br>音楽好きな人、共に演奏しよう！
  			</p>
      </div>
			<img src="../images/school-life/culture1.png">
			<span>入学式から卒業式まで様々な場面で活躍しています。</span>
		</div>

		<div class="box"><div class="text">
			<h4>演劇部</h4>
			<p>スポットライトを浴びたい人も、音響・照明や台本つくりに興味のある人も、みんな舞台に集まれ！
			</p></div>
			<img src="../images/school-life/culture2.png">
			<span>みんなで一つの舞台を創り上げたときの喜びは一入です。</span>
		</div>

		<div class="box"><div class="text">
			<h4>書道部</h4>
			<p>全国レベルの展覧会に多数出品、校内・校外で書道パフォーマンスも行っています。初心者でも大丈夫です。
			</p></div>
			<img src="../images/school-life/culture3.png">
			<span>私たちと一緒に書道をしませんか？</span>
		</div>

		<div class="box"><div class="text">
			<h4>美術部</h4>
			<p>想像力を広げ、作品作りを楽しむ場です。絵画やデザイン、立体作品などを作っています。</p></div>
			<img src="../images/school-life/culture4.png">
			<span>ものづくりや絵を描くことが好きな人たちが集まっています。</span>
		</div>

		<div class="box"><div class="text">
			<h4>華道部</h4>
			<p>花と向き合うことで、自分の内面を見つめることができます。心が安らぎます。</p></div>
			<img src="../images/school-life/culture5.png">
			<span>私たちは花の美を追求しています。</span>
		</div>

		<div class="box"><div class="text">
			<h4>茶道部</h4>
			<p><br></p></div>
			<img src="../images/school-life/culture5_1.png">
			<span>私たちとお茶のお作法を身につけましょう。</span>
		</div>

		<div class="box"><div class="text">
			<h4>パソコン部</h4>
			<p>タイピング検定やゲーム作成、動画編集を通してPCに詳しくなれます。</p></div>
			<img src="../images/school-life/culture6.png">
			<span>大会にも順次参加しています！</span>
		</div>

		<div class="box"><div class="text">
			<h4>自然科学部</h4>
			<p>
目標はありません。ここにあるのは、抑えきれない衝動を持った探究心です。
			</p></div>
			<img src="../images/school-life/culture7.png">
			<span>理科が得意でなくても楽しめる部活です！</span>
		</div>

		<div class="box"><div class="text">
			<h4>英語部</h4>
			<p>
英語は世界共通語です。さらに関心を高め、楽しい時間を作りましょう。
			</p></div>
			<img src="../images/school-life/culture8.png">
			<span>World Englishes「世界の英語たち」を調べる活動を行っています。</span>
		</div>

		<div class="box"><div class="text">
			<h4>漫画研究部</h4>
			<p>
イラストや漫画を描いた部誌を発行しています。文化祭では自分たちの絵を
アクリルキィホルダーや缶バッチにして加工して販売したりしています。
			</p></div>
			<img src="../images/school-life/culture9.png">
			<span>絵を描くのが好きな部員はもちろん、苦手な部員もいます。</span>
		</div>

		<div class="box"><div class="text">
			<h4>和太鼓部</h4>
			<p>部員が心を一つにして太鼓を打ち込む時、聴いている人が感動する時、心はうち震えるのです。</p>
			<p><a href="http://hayatekomagome.jimdo.com/" target="_blank">ホームページ</a></p>
		</div>
			<img src="../images/school-life/culture10.png">
			<span>公演で聴いてくださった方々から拍手をいただいた時は充実感があります。</span>
		</div>

		<div class="box"><div class="text">
			<h4>合唱部</h4>
			<p>音楽、歌、ピアノが好きな人、一緒に合唱を楽しみましょう！初心者の方でも大丈夫ですよ♪</p>
			<p><a href="http://komagomechor.blogspot.jp/" target="_blank">ブログ</a></p>
		</div>
			<img src="../images/school-life/culture11.png">
			<span>個性豊かでにぎやかな部活です。あなたの入部をお待ちしています！</span>
		</div>

		<div class="box"><div class="text">
			<h4>ボランティア部</h4>
			<p>
ボランティア募集の区広報などで参加可能な活動に協力しています。
			</p></div>
			<img src="../images/school-life/culture12.png">
			<span>部員自ら楽しみながらボランティア活動をすることを心がけています。</span>
		</div>
<?php /*
		<div class="box"><div class="text">
			<h4>仏教研究会</h4>
			<p>
他校には無いめずらしいクラブですが、誰でも入れます。
生活の中の仏教を文化祭でのテーマにしています。
夏は「秩父巡礼」の合宿があります。
			</p></div>
			<img src="../images/school-life/clubdummy.png">
			<span>
				<br>
				
			</span>
		</div>*/?>

		<div class="box"><div class="text">
			<h4>写真同好会</h4>
			<p></p></div>
			<img src="../images/school-life/culture14.png">
			<span>写真好きな人、まだそうでもない人、共に歓迎です！</span>
		</div>

		<div class="box"><div class="text">
			<h4>かるた同好会</h4>
			<p></p></div>
			<!--<img src="../images/school-life/clubdummy.png">-->
			<span>ほとんどの人が初心者から始めるので、興味がある人は気軽に来てください！</span>
		</div>

		<div class="box"><div class="text">
			<h4>家庭科クラブ</h4>
			<p></p></div>
			<img src="../images/school-life/culture16.png">
			<span>部員全員が仲の良いクラブで楽しく活動しています。</span>
		</div>
	  </div>
	</section>


<section class="pagetop">
  <p>Page Top</p>
  <span class="arrow"></span>
</section>

    </main>

    <?php include '../footer.php'; ?>
<?php /*    <script src="../js/jquery.min.js"></script>
    <script src="../js/flexibility.js"></script>
    <script src="../js/common2.js" type="text/javascript"></script>
    <script src="../owl/owl.carousel.min.js"></script> */?>
    <script>
    	var $src;
		$(".club-column .box").hover(function() {
			$src = $(this).children("img").attr("src");
			$src2 = $src.split("\.png").join("a\.png");
			$(this).children("img").attr("src",$src2);
		}, function() {
			$(this).children("img").attr("src",$src);
		});
    </script>
  </body>
</html>
