// *******************************************************************************************************
// # About : Function JS
// *******************************************************************************************************


(function($) {

	/* アクセスマップ
	----------------------------------- */
	function initialize() {
		var latlng = new google.maps.LatLng(35.7123345,　139.7070998);/*グーグルマップから取得の数値入れる*/

		var myOptions = {
			zoom: 17,/*拡大比率*/
			center: latlng,/*表示枠内の中心点*/
			mapTypeControlOptions: { mapTypeIds: ['sample', google.maps.MapTypeId.ROADMAP] },/*表示タイプの指定*/
      disableDefaultUI: true
		};

		var map = new google.maps.Map(document.getElementById('map_canvas'), myOptions);

		/*アイコン設定*/
		var icon = new google.maps.MarkerImage('./images/top/icn_map.png',
			new google.maps.Size(119,125),/*アイコンサイズ設定*/
			new google.maps.Point(0,0)/*アイコン位置設定*/
			);
		var markerOptions = {
			position: latlng,
			map: map,
			icon: icon,
			title: '介護のとびら'
		};
		var marker = new google.maps.Marker(markerOptions);

		/*Styled Maps Wizardから取得スタイルの貼り付け*/
		var styleOptions = [
		{
			"stylers": [
			{
			}
			],
			"elementType": "all",
			"featureType": "all"
		}
		];

		var styledMapOptions = { name: 'GIOVANNI LIVE' }
		var sampleType = new google.maps.StyledMapType(styleOptions, styledMapOptions);
		map.mapTypes.set('sample', sampleType);
		map.setMapTypeId('sample');
	}


	/* 実行
	----------------------------------- */
	$(function() {
		// 共通
		google.maps.event.addDomListener(window, 'load', initialize);
		initialize();

		// Webfont Callback
		// ※要検証
		// fontComplete = function() {
		// }
		// Ts.onComplete(fontComplete);

	});

})(jQuery);
