//k切り替えサイズ
var $min_pc = 980;
$(document).ready(function(){
  //例外処理 TOP頁（非推奨）
  var $pathname = $(location).attr("pathname");
  $pathname = $pathname.replace(/\//g,"")
  console.log($pathname);
  if ($pathname.indexOf("top") > -1) {
    $("body").attr("id", $pathname);
  }
  /* タブレット用ビューポート */
  $(function(){
    var ua = navigator.userAgent;
    if((ua.indexOf('iPhone') > 0) || ua.indexOf('iPod') > 0 || (ua.indexOf('Android') > 0 && ua.indexOf('Mobile') > 0)){
        $('head').prepend('<meta name="viewport" content="width=device-width,initial-scale=1">');
    } else {
        $('head').prepend('<meta name="viewport" content="width=1320">');
    }
  });
  /* IE9FLEX対応*/
  var userAgent = window.navigator.userAgent.toLowerCase();
  if( userAgent.match(/(msie|MSIE)/)) {
    var isIE = true;
    var ieVersion = userAgent.match(/((msie|MSIE)\s|rv:)([\d\.]+)/)[3];
    ieVersion = parseInt(ieVersion);
    if(ieVersion<10){
      $(function(){
        flexibility(document.documentElement);
      });
    }
  }
});
/*　アンカーリンクアニメーション（同一ページ） */
$('area[href^="#"],a[href^="#"]').not(".modal").click(function(){
   pageup(this,500);
});


setTimeout( function () {//頁が開くとき先頭にもってくる設定
  var $hash = $(location).attr("hash");
  if ($hash) {
      pageup($hash,0);
  } else {
    pageup(0,0);
  }
},200);
function pageup($id,speed) {
//  var speed = 500;
  var height = $("header").height();
  var href;
  if (speed<1) {
    href = $id;
  } else if ($id == 0 && speed < 1) {
    href = 0;
  } else {
    href = $($id).attr("href");
  }
  var target = $(href == "#" || href == "" ? 'html' : href);
  var position = target.offset().top - height - 36;
  console.log(position);
  $("html, body").animate({scrollTop:position}, speed, "swing");
  return false;
}

/* gotopボタン */
$('.pagetop').click(function(){
  var speed = 500;

  $("html, body").animate({scrollTop:0}, speed, "swing");
  return false;
});

/* spnav */
$('.sp-btn').click(function(){
  $('.lower').addClass('-open');
});
$('.close').click(function(){
  $('.lower').removeClass('-open');
});

$('.lower .block').click(function(){
  $('.lower .block').removeClass('-current');
  $(this).addClass('-current');
});

$(window).scroll(function(){
  var scrollValue = $(window).scrollTop();
  if(scrollValue > 500){
    $('.gotop').addClass('visible');
    $('.conversion').removeClass('hide');
  } else {
    $('.gotop').removeClass('visible');
    $('.conversion').addClass('hide');
  }
});



/* modal_win */
//.画像のMAXサイズと送り番号の設定
function modalin($href, $number) {
  setTimeout(function(){
    var w = $(window).width();
    var h = $(window).height();
    var img = new Image();
    img.src = $href;
    var width  = img.width;
    var height = img.height;
    if (width > 0 || height > 0) {
      var $i = 0;
      while ($i > 0) {
        width  = img.width;
        height = img.height;
        if (width> 0 && height>0) {
          $i++;
        }
      }
    }
    var $prev = $number - 1;
    var $next = $number + 1;
    var styleset = "";
    if (w > $min_pc && width >0 && height >0 ) {
      if (width>w) {
        styleset = 'style=" width:'+ (w-100)+'px; height: auto;"';
      } else if (height>h) {
        styleset = 'style=" height:'+ (h-100)+'px; width: auto;"';
      } else {
        styleset = 'style=" width:'+ width +'px; height:'+ height +'px;"';
      }
    }
    if ($prev < 0) {
      $prev = ($modal_img.length - 1);
    }
    if ($next >= $modal_img.length) {
      $next = 0;
    }
    $(".modal_win").html( '<p><img src="'+$href+'"'+styleset+'><a class="close"></a></p>' );
    if ($modal_img_caption[$number]) {
      $(".modal_win").children("p").append("<span class=\"caption\">" + $modal_img_caption[$number] + "</span>");
    }
    if ($modal_img_caption.length>0) {
      $(".modal_win").children("p").append("<a rel=\"" + $prev + "\" class=\"change prev\"></a>");
      $(".modal_win").children("p").append("<a rel=\"" + $next + "\" class=\"change next\"></a>");
    }
  },300);
}


$(function(){
  $('<div class="modal_win"></div>').prependTo('body');
  $("a.modal").click(function() {
    var w = $(window).width();
    var h = $(window).height();
    var $number = $('.modal').index(this);
    var $href = $(this).attr('href');
    $(".modal_win").fadeIn("fast");
    $(".modal_win").css("display","flex");
    if ($href.indexOf(".jpg") > 0 || $href.indexOf(".jepg") > 0 || $href.indexOf(".gif") > 0 || $href.indexOf(".png") > 0 || $href.indexOf(".svg") > 0) {
    //画像の場合
      modalin($href,$number);
    } else if ($href.indexOf("#") >= 0 )  {
    //ページ内のデータを読み込み
      var $html = $($href).html();
      //video 自動スタートを代入
      if ($html.indexOf("<video") > -1) {
        $html = $html.split("\<video").join("\<video controls autoplay");
      }
      $(".modal_win").html( "<div><div>" + $html + "</div><a class=\"close\"></a></div>" );
    } else if ($href) {
    //iframeの場合
      $(".modal_win").html( "<div><iframe width=\"100%\" height=\"" + (h*0.75) + "\" src=\""+$href+"\"></iframe><a class=\"close\"></a></div>" );
    }
    return false;
  });
//  $modal_img = [];
//  $modal_img_caption = [];
  $(".modal_win").click(function() {
      //呼び出し画像の変更
    if ($(event.target).closest('a.change').length) {
      var $rel = $(event.target).closest('a.change').attr('rel') - 0;
      modalin($modal_img[$rel], $rel);
    } else if(!$(event.target).closest('p').length || !$(event.target).closest('div').length || $(event.target).closest('.close').length) {
      $(".modal_win").fadeOut("fast");
      $(".modal_win").html(" ");
    }
  });
});


setTimeout( function () {
  //modalのpreloadおよび画像有無の確認
  $modal_img = [];
  $modal_img_caption = [];
  var $i = 0;
  $("main .modal").each(function(index, element) {
    var $href = $(this).attr('href');
    if ($href.indexOf(".jpg") > 0 || $href.indexOf(".jepg") > 0 || $href.indexOf(".gif") > 0 || $href.indexOf(".png") > 0 || $href.indexOf(".svg") > 0) {
      $("<img>").attr("src", $href);
      $modal_img[$i] = $href;
      $modal_img_caption[$i] = $(this).attr('rel');
      $i++;
    }
  });
  /* タブレット用imgにmaxsize挿入 */
  $(".voice .image img").each(function(index, element) {
    var img = new Image();
    img.src = $(this).attr('src');
    var $img_w = img.width;
    var $img_h = img.height;
    var $calc =  $img_h - 60;
    $(this).css({"max-width" : $img_w, "max-height" : $img_h});
    $(this).parent(".image").css({"width" : $img_w, "height" : $calc, "margin": "auto"});
  });
  // スマートフォン専用
  var w = $(window).width();
  if (w < $min_pc) {
    //[.table_2b]見出しを代入
    var table_2_thead = [];
    var $i=0;
    $(".table_2b thead>tr>*").each(function(index, element) {
      table_2_thead[$i] = $(this).html();
      $i++;
    });
    var $i=0;
    $(".table_2b tbody>tr>*").each(function(index, element) {
      var $td = $(this).html();
      $(this).prepend("");
      $(this).html("<div class=\"th\">" + table_2_thead[$i] + "</div><div class=\"td\">"+ $td +"</div>");
      $i++;
      if ($i >= table_2_thead.length) {
        $i=0;
      }
    });
  }

} , 500 );




//common.jsより移行

/* ヘッダーのモーダル
$('.document').on('click',function(){
    $('.modal').addClass('-active');
    $('#document-modal').addClass('-current');
  });
  $('.modal').on('click',function(){
    var selStr = document.getSelection();
    if(selStr.focusOffset == 0){
      $('.modal').removeClass('-active');
      $('.modal div').removeClass('-current');
    }
  }); */

//ニュース系ページサイドバー
  $('aside .pulldown').on('click',function(){
    $(this).next('.sub').slideToggle();
    $(this).toggleClass('-open');
  });
