<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <title>6ヵ年一貫教育プログラム｜駒込中学校・高等学校</title>
    <meta content="駒込中学校・高等学校" name="description">
    <meta http-equiv="Pragma" content="no-store">
    <meta http-equiv="Cache-Control" content="no-store">
    <meta http-equiv="Expires" content="0">
		<meta name="format-detection" content="telephone=no">

    <link href="../images/common/favicon.ico" rel="shortcut icon">
    <link href="../images/common/favicon.ico" rel="apple-touch-icon">
    <link href="../css/common.css" rel="stylesheet" type="text/css">
<?php //    <link href="../css/sub.css" rel="stylesheet" type="text/css"> ?>
    <link href="../css/sub2.css" rel="stylesheet" type="text/css">
    <link href="../owl/owl.carousel.min.css" rel="stylesheet" type="text/css">

  </head>

  <body id="j-application">
    <?php include '../header.php'; ?>

    <main>
      <section class="mv header-title">
				<h1>6ヵ年一貫教育プログラム</h1>
        <p>Junior High School Briefing</p>
      </section>

			<section class="article-main">
				<article>
					<h2>理想の未来へ着実に、確実に。<br>併設型中高一貫校の特色を生かした計画的な教育プログラム。</h2>
					<p>駒込中学は中高一貫教育の利点を生かして、6年間を通じた特色あるカリキュラムを編成していきます。
          <br>本科コースでは、中学と高校の各教科の中で相互に関連する内容を中高と言う枠を取り払うことで、
          <br>先取り指導や内容を入れ替えての指導が可能となり、柔軟性のある学習が出来ます。
          <br>国際先進コースでは高１からはじまる「国際教養」「理系先進」の
          <br>２系統のスペシャルコースの何れかに連結して、高い専門性を身につけていきます。
          </p>
				</article>
			</section>

<section class="article-main">
  <p class="tac strong sc_color">「国際先進コース」と「本科コース」の2本柱</p>
  <div class="flex_pc column2">
    <div class="box">
      <h3>国際先進コース</h3>
      <div class="color_box">
        <p>次期学習指導要領がもたらすICT授業に基盤を置く「数理探求」と、「英語4技能指導」をベースとした「グローバル教育」を一体化した「文理融合型コース」として設定します。中学段階では、新しい大学入試改革に基づいて、理系文系に分けず、リベラルな基礎学力を身につけ、高校段階から各自の進路を支える「国際教養コース」または「理系先進コース」でさらに高レベルな指導を行っていきます。</p>
        <ul class="disc fs0.8 mt2">
          <li>このコースの英語力のある生徒には、「スーパーイングリッシュコース」として、中1で中2の内容までスピード学習する英文法の「特別指導」を行います。</li>
        </ul>
      </div>
    </div>
    <div class="box">
      <h3>本科コース</h3>
      <p class="color_box">生徒に「自立のためのアカデミックな深い教養」をじっくり身につけてもらうと共に、問題発見、解決学習、体験学習、グループワーク等を幅広く導入した「アクティブラーニング」を行います。一貫校の特徴を生かして高校2年次までに6カ年の学びを修了させ、さらに「大学入試改革」にも対応して、GMARCHや難関国公立私大、そして芸術系難関大合格を狙います。こうした様々なスキルをもって来るべき「AI（人工知能）時代・IoT時代」に対応できる「21世紀型のリーダー」の育成を目的としています。</p>
    </div>
  </div>
</section>

<section class="article-main">
  <h3>6ヵ年を3ステージに分割した体系的段階プログラム</h3>
  <div class="three-column">
    <div class="box">
      <div class="title">The 1st Stage</div>
      <div class="sub-title">
        前期過程（中1・中2）
      </div>
      <p>この時期は6年間の学習姿勢や学力を占う最も大切な時期で、ここで6年間の学力が決定付けられると言っても過言ではありません。アクティブラーニングに向かう前に、基礎学力の徹底を図り、ゆるぎない土台を完成させていきます。
      </p>
    </div>

    <div class="box">
      <div class="title">The 2nd Stage</div>
      <div class="sub-title">
        中期過程（中3・高1）
      </div>
      <p>中2後半からは学力の振り幅はだんだんと小さくなり、学力が安定して伸びていく時期です。<br>前期過程で獲得した基礎学力を土台にして、より高い学力水準に到達できるように、生徒のモチベーションを支援して、実力の更なる向上を図っていきます。
      </p>
    </div>

    <div class="box">
      <div class="title">The 3rd Stage</div>
      <h4 class="sub-title">
        後期過程（高2・高3）
      </h4>
      <p>新しい大学入試を意識して、これまで培った力を合格力につなげる大切な時期です。<br>この新入試の方式にも十分に対応できる準備を済ませ、第一志望校という目標実現への軌跡にしっかり乗って進んでいけるよう、徹底した支援と指導を行っていきます。
      </p>
    </div>
  </div>
  <!--<img src="../images/junior-high-school/list.jpg" class="img-list">-->
  <div class="ofx_s">
  	<div class="flow">
  		<div class="left">
  			<div class="large">前期課程</div>
  			<div class="small">
          <div>中学１年</div>
          <div>中学２年</div>
        </div>
  			<div class="large">中期課程</div>
        <div class="small">
          <div>中学３年</div>
          <div>高校１年</div>
        </div>
        <div class="large">後期課程</div>
        <div class="small">
          <div>高校２年</div>
          <div>高校３年</div>
        </div>
  		</div>
      <div class="lane sixyear">
        <div class="head">6年間のコース</div>
        <div class="block">
          <div class="upper">
            <div>本科コース
              <div class="subwin">
                <h5>本科コース</h5>
                <p>6年一貫の特徴を生かして、基礎学力を4年間徹底して身につけ、高2・高3の2年間で文系、理系にそれぞれ分かれるコース。様々な分野に興味関心を広げられます。（最後の2年間は一部の授業が高入生と混合となる場合もあります）</p>
<?php //                <p class="image"><img src="../images/dammy.jpg"></p> ?>
              </div>
            </div>
            <div>国際先進コース
              <div class="subwin">
                <h5>国際先進コース</h5>
                <p>3年後の高校1年生から国際教養コース、または、理系先進コースのどちらかにドッキングして進級するコースも高校からの入学生との混合クラスになります。「自分はこれが好きだ」「得意だ」と強く言える生徒を大歓迎します。</p>
<?php //                <p class="image"><img src="../images/dammy.jpg"></p> ?>
              </div>
            </div>
            <p class="arrow"><i></i></p>
          </div>
          <div class="lower">
            <div>Ｓコース<span>（医薬獣農法経社系／国公立難関私大）高２より文理選択</span>
              <div class="subwin">
                <h5>Sコース</h5>
                <p>「各分野に興味があり、これから自分の適性を見つけたい」「国公立医薬、難関私立大を目指したい」「個性が発揮できる力を身につけたい」という生徒に適した学びやキャリア選択が可能なコースです。</p>
<?php //                <p class="image"><img src="../images/dammy.jpg"></p> ?>
              </div>
            </div>
            <div>国際教養コース<span>（法経商社人文系／海外大・東外大・国際教養大・早慶上理・ＩＣＵほか）</span>
              <div class="subwin">
                <h5>国際教養コース</h5>
                <p>「社会と英語が好きで、世界に飛び出していきたい」「将来は海外大学も目指したい」「世界を舞台にして、様々な人々と交流したい」という生徒のために、ALL ENGLISH授業が充実している海外大学を目指せるコースです。</p>
<?php //                <p class="image"><img src="../images/dammy.jpg"></p> ?>
              </div></div>
            <div>理系先進コース<span>（理工医薬農系／東工大・筑波大・千葉大・電通大・早慶理・私大医学部ほか）</span>
              <div class="subwin">
                <h5>理系先進コース</h5>
                <p>「数学や理科が好きで、研究者になりたい」「技術やモノづくりに興味がある」「時代の最先端教育に興味がある」という生徒に向けて、STEM教育を基盤としたプログラミングなどの体験・探求型の授業が充実したコースです。</p>
<?php //                <p class="image"><img src="../images/dammy.jpg"></p> ?>
              </div>
            </div>
         </div>
        </div>
      </div>
      <div class="lane global-education">
        <div class="head">Global教育</div>
        <div class="block">
          <div class="upper">
            <div>スーパーイングリッシュコース
              <div class="subwin">
                <h5>スーパーイングリッシュコース</h5>
                <p>国際先進コースの英語力のある生徒には、「スーパーイングリッシュコース」として、中1で中2の内容までスピード学習する英文法の「特別指導」を行います。</p>
<?php //                <p class="image"><img src="../images/dammy.jpg"></p> ?>
              </div>
            </div>
            <div class="grade">
              <ul>
                <li>英検４級～３級</li>
                <li>英検３級～準２級</li>
                <li>英検準２級～２級</li>
              </ul>
              <div class="subwin">
                <h5>英語検定対策</h5><!--英検・CEFRは一つのくくりにしてください -->
                <p>大学入試に役立つだけでなく、日々の英語学習に目標と自信を持って臨めるため、英語検定の取得を推奨し、対策を実施しています。</p>
<?php //                <p class="image"><img src="../images/dammy.jpg"></p> ?>
              </div>
            </div>
            <div>習熟度別クラス編成・少人数英会話授業・イマ―ジョン授業<span>（授業内）</span>
              <div class="subwin">
                <h5>習熟度別クラス編成・少人数英会話授業・イマ―ジョン授業（授業内）</h5>
                <p>ネイティブ教員との授業を中心に、特にスピーキングとリスニングの力を伸ばすことを主軸に、ティームティーチングによる参加型授業を少人数で実施したり、中1からイマージョン授業を取り入れています。</p>
<?php //                <p class="image"><img src="../images/dammy.jpg"></p> ?>
              </div>
            </div>
            <div class="jh2-3">オンライン英会話<span>（希望者／校内／週１回）</span>
              <div class="subwin">
                <h5>オンライン英会話（希望者／校内／週1回）</h5>
                <p>高校生は毎週30分好きな時間に、中学2・3年生は放課後週1回30分間、海外にいるネイティブとマンツーマンオンライン英会話授業を行います。自分のレベルに合わせてスタートが切れるのがマンツーマンのメリット。楽しい会話を通じて無理なく英語4技能の力を伸ばします。</p>
<?php //                <p class="image"><img src="../images/dammy.jpg"></p> ?>
              </div>
            </div>
            <div class="jh1-5">ハワイセミナー短期語学研修<span>（希望者）</span>
              <div class="subwin">
                <h5>ハワイセミナー短期語学研修</h5>
                <p>「生きた英語」を学び、現地の独特な文化に触れる体験をします。ハワイ大学大学院生による英会話のレベル別語学レッスンのほか、「英語お買い物体験」、スーパーマーケットでの食材見学、そして真珠湾見学など、ハワイだからこそできるセミナーを提供します。</p>
<?php //                <p class="image"><img src="../images/dammy.jpg"></p> ?>
              </div>
            </div>
            <div>レシテーションコンテスト<span>（他校私立の代表生徒とのコンテスト）</span>
              <div class="subwin">
                <h5>レシテーションコンテスト（他校私立の代表生徒とのコンテスト）</h5>
                <p>本校を会場に、高校生を含む多くの私立学校の代表生徒が集結して行うコンテストです。与えられた英語の文章を暗記・再生するだけでなく、その文の内容に即した表情やジェスチャーまでを総合判定される、とてもハイレベルなコンテストです。</p>
<?php //                <p class="image"><img src="../images/dammy.jpg"></p> ?>
              </div>
           </div>
            <div>スピーチコンテスト<span>（学内実施）</span></div>
            <div class="blank"></div>
          </div>
          <div class="lower">
            <div class="blank"></div>
            <div>ＣＥＦＲ　Ａ２～Ｂ１<span>（英検２級程度～英検準１級程度）</span>
            </div>
            <div class="blank"></div>
            <div class="hs1">イマージョン講座<span>週１回</span>
              <div class="subwin">
                <h5>イマージョン講座</h5>
                <p>英語のみで、「数学」「理科」「社会」「音楽」「地理」などの教科を学習する講座です。ネイティブの教員と本校教員とのティームティーチングで、週に1回、25人程度の少人数で実施しています。</p>
<?php //                <p class="image"><img src="../images/dammy.jpg"></p> ?>
              </div>
            </div>
            <div class="blank"></div>
            <div class="hs1-2">マルタ島短期留学<span>準２級取得者以上の希望者</span>
              <div class="subwin">
                <h5>マルタ島短期留学　準2級取得者以上の希望者</h5>
                <p>地中海に浮かぶマルタ島（マルタ共和国）で英語を学ぶ上級プログラム。治安が良くヨーロッパ各地からの留学生が多いので多国籍な学生と交流できます。午後はアクティビティが用意され、地中海文化を肌で感じ取ることができます。</p>
<?php //                <p class="image"><img src="../images/dammy.jpg"></p> ?>
              </div>
           </div>
            <div class="blank"></div>
            <div class="hs1-2">模擬国連</div>
          </div>
        </div>
      </div>
      <div class="lane stem">
        <div class="head">STEM<br>教育</div>
        <div class="block">
          <div class="upper">
            <div>ＳＴＥＭ講座<span>（国際先進コース）</span>・プログラミング講座<span>（国際先進希望者）</span></div>
          </div>
          <div class="lower">
            <div>ＳＴＥＭ講座<span>（理系先進コース）</span></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<!--    <img src="../images/junior-high-school/slider1.png">
    <img src="../images/junior-high-school/slider2.png">
    <img src="../images/junior-high-school/slider3.png">-->
<section class="comment">
  <div class="title">生徒コメント</div>
  <img src="../images/junior-high-school/title-comment.png">
  <div class="slider owl-carousel">
    <div class="voice">
      <div class="two-column">
        <div class="box">
          <div class="text">
            <h3>理系先進コース</h3>
            <p>理系先進にはたくさんの独自授業があるのですが、私が一番好きな授業は「STEM」です。この授業では埼玉大学の先生がLEGOと専用のアプリを使いながらプログラミングを教えてくれます。これまでには扇風機や自動ドアなどを作りました。私はこの授業を通して様々な視点から物事を考えることの大切さを学んでいます。以前、自分では完璧だと思ったのですが、先生や友達に見てもらったらいくつも問題点や改善点が見つかった、ということがありました。それからは作った物の苦手な部分を自ら探し出し、それを一つ一つ改善していくよう心がけています。<br>このように理系先進コースでは、AIの時代にも通用する「問題を発見し解決する」力を養うことができます。</p>
          </div>
          <div class="detail">
            <p class="name">長久保さん</p>
          </div>
          <div class="image">
            <img src="../images/junior-high-school/j-application1.png">
          </div>
        </div>
      </div>
    </div>
    <div class="voice">
      <div class="two-column">
        <div class="box">
          <div class="text">
            <h3>特S・Sコース</h3>
            <p>特Sコースに入学して良かったと思う一番の点は全教科を幅広く勉強でき、将来の選択肢を様々な角度から探ることができる点です。苦手な科目は得意な子に教えてもらい、反対に得意な科目は苦手な子に教えるなど、みんなで切磋琢磨しています。また、特別講習や夏期講習、勉強合宿や英語キャンプなど希望者参加型のプログラムが多いのも魅力のひとつです。それぞれのプログラムを通して自ずと目標を持ち、目標に向かって自主的に行動する力が身につきます。私は留学に行くことを目標に日々努力を重ねています。<br>駒込学園は、友人たちとお互いを刺激し合いながら、それぞれの目標や夢を目指せる学校です。</p>
          </div>
          <div class="detail">
            <p class="name">七海さん</p>
          </div>
          <div class="image">
            <img src="../images/junior-high-school/j-application2.png">
          </div>
        </div>
      </div>
    </div>

    <?php // 都合上重複 3つ以上の時は不要 ?>
    <div class="voice">
      <div class="two-column">
        <div class="box">
          <div class="text">
            <h3>国際教養コース</h3>
            <p>私は国際教養コースに入ってから勉強に対しての姿勢が変わっていったと思います。中学時代の私は、どちらかというと受け身で授業を受けてしまいがちでした。<br>しかし、国際教養コースに入って問題提起されたことに対して自ら考え、その考えを共有するという機会ができたので、色々なことを自分で考えるようになりました。<br>また、「異文化理解」や「日本文化概論」などの授業によって自分の宗教観や文化的な物事に対する意識というものが高まっていきました。<br>レポートの提出なども多いので文章能力やタイピング力も上げることができました。</p>
          </div>
          <div class="detail">
            <p class="name">加藤くん</p>
          </div>
          <div class="image">
            <img src="../images/junior-high-school/j-application3.png">
          </div>
        </div>
      </div>
    </div>
<?php /*    <div class="voice">
      <div class="two-column">
        <div class="box">
          <div class="text">
            <h3>高校生のコメント</h3>
            <p>ダミー文がいれてあります。ダミー文がいれてあります。ダミー文がいれてあります。ダミー文がいれてあります。ダミー文がいれてあります。ダミー文がいれてあります。ダミー文がいれてあります。ダミー文がいれてあります。ダミー文がいれてあります。ダミー文がいれてあります。ダミー文がいれてあります。ダミー文がいれてあります。</p>
          </div>
          <div class="detail">
            <p class="name">中学二年生<br>○○　○○</p>
          </div>
          <div class="image">
            <img src="../images/education/voice1.png">
          </div>
        </div>
      </div>
    </div> */?>

  </div>
</section>

<section class="pagetop">
  <p>Page Top</p>
  <span class="arrow"></span>
</section>

    </main>

    <?php include '../footer.php'; ?>
<?php /*    <script src="../js/jquery.min.js"></script>
    <script src="../js/flexibility.js"></script>
    <script src="../js/common.js" type="text/javascript"></script>
    <script src="../js/common2.js" type="text/javascript"></script>*/?>
    <script src="../owl/owl.carousel.min.js"></script>
    <script>
    $(document).ready(function(){
      /* タブレット用ビューポート */
      $(function(){
        var ua = navigator.userAgent;
        if((ua.indexOf('iPhone') > 0) || ua.indexOf('iPod') > 0 || (ua.indexOf('Android') > 0 && ua.indexOf('Mobile') > 0)){
            $('head').prepend('<meta name="viewport" content="width=device-width,initial-scale=1">');
        } else {
            $('head').prepend('<meta name="viewport" content="width=1320">');
        }
      });

      $("main .upper>*,main .lower>*").not('.blank, .allow, .subwin').hover(
        function() {
          $(this).addClass('-hover');
        },
        function() {
          $(this).removeClass('-hover');
        }

      );
      /* スライダー用記述 */
      $(".owl-carousel").owlCarousel({
        enter: true,
        loop: true,
        nav: true,
        items : 1,
        autoplay: true,
        slideSpeed : 1000,
        dots : true,
        rewind: false,
        autoplayHoverPause: true,
        center: true,
        responsive : {　//レスポンシブ対応
           // ブレイクポイント 0以上
           0 : { items: 1},
//           1270 : { items: 2}
           800 : { items: 2},
           1600 : { items: 3}
        }
      });
    });


    </script>
  </body>
</html>
