<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <title>ニュース | 駒込高校・中学校</title>

    <meta content="" name="description">
    <meta http-equiv="Pragma" content="no-store">
    <meta http-equiv="Cache-Control" content="no-store">
    <meta http-equiv="Expires" content="0">

    <link href="/images/common/favicon.ico" rel="shortcut icon">
    <link href="/images/common/favicon.ico" rel="apple-touch-icon">
    <link href="/css/common.css" rel="stylesheet" type="text/css">
    <link href="/css/sub.css" rel="stylesheet" type="text/css">
    <style>
      .employ a{
        font-size: 20px;
        color: #337ab7 !important;
        display: inline;
      }
      .employ a:hover{
        border-bottom: 1px solid #337ab7;
      }
    </style>
  </head>

  <body>
  <?php include './header.php'; ?>

    <main>
      <section class="mv header-title">
				<h1>採用情報</h1>
        <p>Employment</p>
      </section>

<section class="article-main">
<?php while ( have_posts() ) : the_post(); ?>
<?php if( have_rows('saiyou') ): ?>
				<article>
<?php while ( have_rows('saiyou') ) : the_row(); ?>
          <p class="employ"><a href="<?php the_sub_field('pdf');?>" target="_blank"><?php the_sub_field('title');?></a></p>
<?php endwhile; ?>
        </article>
<?php else : ?>
  <article>
    <p>現在採用は行っておりません。</p>
  </article>
<?php endif; ?>
<?php endwhile; ?>


			</section>


    </main>

  <?php include './footer.php'; ?>
</body>
</html>
