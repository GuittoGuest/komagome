<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <title>駒込中学・高等学校</title>
    <meta content="駒込中学・高等学校" name="description">
    <meta http-equiv="Pragma" content="no-store">
    <meta http-equiv="Cache-Control" content="no-store">
    <meta http-equiv="Expires" content="0">
		<meta name="format-detection" content="telephone=no">

    <link href="./images/common/favicon.ico" rel="shortcut icon">
    <link href="./images/common/favicon.ico" rel="apple-touch-icon">
    <link href="./css/common.css?ver0708" rel="stylesheet" type="text/css">
    <link href="./css/index.css" rel="stylesheet" type="text/css">
    <link href="./owl/owl.carousel.min.css" rel="stylesheet" type="text/css">
  </head>

  <body>
    <header>
      <img src="./images/common/header.png">
    </header>

    <main>
      <section class="slider">
<?php
  $tab = array("学習支援","STEM教育","ICT教育","グローバル教育");
  $i = count($tab);
  foreach ($tab as $value) {?>
        <div class="flame" id="s<?php echo $i; ?>">
          <div class="catch"><img src="./images/index/catch<?php echo $i; ?>.png"></div>
          <div class="circle"><img src="./images/index/circle<?php echo $i; ?>.png"></div>
          <div class="image"></div>
          <div class="tab"><span><?php echo $value; ?></span></div>
        </div>
<?php
    $i--;
  } ?>
      </section>
<?php /*      <section class="sp-slider">
        <ul class="owl-carousel">
          <li><img src="./images/index/s1-sp.png"></li>
          <li><img src="./images/index/s2-sp.png"></li>
          <li><img src="./images/index/s3-sp.png"></li>
          <li><img src="./images/index/s4-sp.png"></li>
        </ul>
      </section>*/ ?>
    </main>
      <nav>
        <div class="left">
          <a href="">
            <div class="strip">
              中学入試説明会
            </div>
          </a>
          <a href="">
            <div class="strip">
              高校入試説明会
            </div>
          </a>
        </div>
        <div class="right">
          <a href="./top">
            <div class="strip">
              TOPページはこちら
            </div>
          </a>
        </div>
      </nav>


    <script src="./js/jquery.min.js"></script>
    <script src="./js/flexibility.js"></script>
    <script src="./js/common.js?0708" type="text/javascript"></script>
    <script src="./owl/owl.carousel.min.js"></script>
    <script>
    $(document).ready(function(){
      $mode = 0;
      /* タブレット用ビューポート */
      //$(function(){
        var ua = navigator.userAgent;
        if((ua.indexOf('iPhone') > 0) || ua.indexOf('iPod') > 0 || (ua.indexOf('Android') > 0 && ua.indexOf('Mobile') > 0)){
          $mode++;
          $('head').prepend('<meta name="viewport" content="width=device-width,initial-scale=1">');
        } else {
          $('head').prepend('<meta name="viewport" content="width=1320">');
        }
      //});

      var userAgent = window.navigator.userAgent.toLowerCase();
      var isIE = (userAgent.indexOf('msie') >= 0 || userAgent.indexOf('trident') >= 0 || userAgent.indexOf('edge') != -1);


      var time ="2500"

      $(".slider .flame").css("transition-duration", "0s");
<?php //      if(isIE) {?>
        var width = $(window).innerWidth();
        var s1current;
        var s2current;
        var s3current;
        var s1swipe;
        var s2swipe;
        var s3swipe;
        if ($mode < 1) {
          s1current = $('#s1').position().left;
          s2current = $('#s2').position().left;
          s3current = $('#s3').position().left;
          s1swipe = 100 - width;
          s2swipe = 125 - width;
          s3swipe = 150 - width;
        }
        var slide = function(time){
          setTimeout(function(){$('#s1').addClass('-current -scale')},time/20);
          setTimeout(function(){$(".slider .flame").css("transition-duration", "3s")},time);
          setTimeout(function(){$('#s1').removeClass('-current')},time);
          setTimeout(function(){$('#s2').addClass('-scale -current')},time);
          setTimeout(function(){$('#s2').removeClass('-current')},time*3);
          setTimeout(function(){$('#s1').removeClass('-scale')},time*3);
          setTimeout(function(){$('#s3').addClass('-scale -current')},time*3);
          setTimeout(function(){$('#s3').removeClass('-current')},time*5);
          setTimeout(function(){$('#s2').removeClass('-scale')},time*5);
          setTimeout(function(){$('#s4').addClass('-scale -current')},time*5);
          setTimeout(function(){$('#s3').removeClass('-scale')},time*6);
          setTimeout(function(){$('#s4').removeClass('-current')},time*7);
          setTimeout(function(){$('#s4').removeClass('-scale')},time*8);
          setTimeout(function(){},time*9);
          if ($mode > 0) {
            setTimeout(function(){$('#s1').css('left', "-100vw")},time);
            setTimeout(function(){$('#s2').css('left', "-100vw")},time*3);
            setTimeout(function(){$('#s3').css('left', "-100vw")},time*5);
            setTimeout(function(){$('#s1').css('left', "0")},time*7);
            setTimeout(function(){$('#s2').css('left', "0")},time*7);
            setTimeout(function(){$('#s3').css('left', "0")},time*7);
          } else {
            setTimeout(function(){$('#s1').css('left', s1swipe+'px')},time);
            setTimeout(function(){$('#s2').css('left', s2swipe+'px')},time*3);
            setTimeout(function(){$('#s3').css('left', s3swipe+'px')},time*5);
            setTimeout(function(){$('#s1').css('left', s1current+'px')},time*7);
            setTimeout(function(){$('#s2').css('left', s2current+'px')},time*7);
            setTimeout(function(){$('#s3').css('left', s3current+'px')},time*7);
          }
        };

<?php /*      }else{

        var slide = function(time){
          $('#s1').addClass('-current -scale');
          setTimeout(function(){$('#s1').addClass('-swiped')},time);
          setTimeout(function(){$('#s1').removeClass('-current')},time);
          setTimeout(function(){$('#s2').addClass('-scale -current')},time);
          setTimeout(function(){$('#s2').addClass('-swiped')},time*3);
          setTimeout(function(){$('#s2').removeClass('-current')},time*3);
          setTimeout(function(){$('#s1').removeClass('-scale')},time*3);
          setTimeout(function(){$('#s3').addClass('-scale -current')},time*3);
          setTimeout(function(){$('#s3').addClass('-swiped')},time*5);
          setTimeout(function(){$('#s3').removeClass('-current')},time*5);
          setTimeout(function(){$('#s2').removeClass('-scale')},time*5);
          setTimeout(function(){$('#s4').addClass('-scale -current')},time*5);
          setTimeout(function(){$('#s3').removeClass('-scale')},time*6);
          setTimeout(function(){$('#s1,#s2,#s3').removeClass('-swiped')},time*7);
          setTimeout(function(){$('#s4').removeClass('-current')},time*7);
          setTimeout(function(){$('#s4').removeClass('-scale')},time*8);
          setTimeout(function(){},time*9);
        };
      }*/ ?>
      slide(time);
      setInterval(slide, time*8, time);

 <?php      /* SPスライダー用記述
      $(".owl-carousel").owlCarousel({
        loop: true,
        nav: true,
        items : 1,
        autoplay: true,
        slideSpeed : 1000,
        dots : true,
        rewind: false,
        autoplayHoverPause: true,
        center: true,
        navigationText: [
    '<i class="icon-chevron-left icon-white"><img src="./images/index/prev.png"></i>',
    '<i class="icon-chevron-left icon-white"><img src="./images/index/next.png"></i>',
    ]
      });*/?>
    });
    </script>
  </body>
</html>
