<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <title>ニュース | 駒込高校・中学校</title>

    <meta content="" name="description">
    <meta http-equiv="Pragma" content="no-store">
    <meta http-equiv="Cache-Control" content="no-store">
    <meta http-equiv="Expires" content="0">

    <link href="/images/common/favicon.ico" rel="shortcut icon">
    <link href="/images/common/favicon.ico" rel="apple-touch-icon">
    <link href="/css/common.css" rel="stylesheet" type="text/css">
    <link href="/css/sub.css" rel="stylesheet" type="text/css">
    <link href="/css/news.css" rel="stylesheet" type="text/css">
  </head>

  <body class="archive-news">
    <?php include './header.php'; ?>

    <main>

      <section class="mv header-title">
      	<h1>ニュース記事一覧</h1>
        <p>News List</p>
      </section>

      <div class="body-wrapper">
			<section class="news-list">
        <ul>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
          <a href="<?php the_permalink(); ?>">
<?php
//新着判定
$days = 3;
$today = date_i18n('U');
$entry_day = get_the_time('U');
$keika = date('U',($today - $entry_day)) / 86400;
if ( $days > $keika ):
    echo '<li class="new">';
else:
		echo '<li>';
endif;
$imgurl = catch_first_image();
$imgclass = '';
if(strpos($imgurl,'noimage.png') !== false){
  $imgclass = "noimage";
}
?>

            <div class="images">
              <img src="<?php echo catch_first_image(); ?>" alt="<?php the_title(); ?>" class="<?= $imgclass; ?>" />
            </div>
            <div class="detail">
<?php
	$postid = get_the_ID();
	$terms = get_the_terms( $postid, 'news-cat' );
	$cname = $terms[0]->name;
	$cslug = $terms[0]->slug;

	$parent = (int)$terms[1]->parent;

	if(isset($terms[1])&& $parent>=1){
		$cattmp = $cname;
		$cname = $cattmp.'－ '.$terms[1]->name;
	}
?>
              <p class="date"><?php the_time('Y.m.d'); ?></p><div class="cat cat-<?= $cslug ?>"><?= $cname; ?></div>
              <h3 class="title"><?php the_title(); ?></h3>
            </div>
          </li>
          </a>
<?php endwhile; endif; ?>

          </li>

        </ul>
					<?php
					if ( function_exists( 'pagination' ) ) :
					?>
					  <div class="pager pc">
					    <?php pagination( $wp_query->max_num_pages, get_query_var( 'paged' ) ); ?>
					  </div>

					  <div class="pager sp">
					    <?php pagination( $wp_query->max_num_pages, get_query_var( 'paged' ) ); ?>
					  </div>
					<?php endif; ?>
      </section>
      <?php include get_stylesheet_directory().'/sidebar.php'; ?>
    </div>

    </main>

    <?php include './footer.php'; ?>
</body>
</html>
