<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <title>ニュース | 駒込高校・中学校</title>

    <meta content="" name="description">
    <meta http-equiv="Pragma" content="no-store">
    <meta http-equiv="Cache-Control" content="no-store">
    <meta http-equiv="Expires" content="0">

    <link href="/images/common/favicon.ico" rel="shortcut icon">
    <link href="/images/common/favicon.ico" rel="apple-touch-icon">
    <link href="/css/common.css" rel="stylesheet" type="text/css">
    <link href="/css/sub.css" rel="stylesheet" type="text/css">
    <link href="/css/news.css" rel="stylesheet" type="text/css">
  </head>

  <body class="single-news">
  <?php include './header.php'; ?>

    <main>

      <section class="mv header-title">
      	<h1>ニュース記事一覧</h1>
        <p>News List</p>
      </section>

      <div class="body-wrapper">
			<section class="news">
        <div class="head">
          <p class="date"><?php the_time('Y.m.d'); ?></p>
<?php
	$postid = get_the_ID();
	$terms = get_the_terms( $postid, 'news-cat' );
	$cname = $terms[0]->name;
	$cslug = $terms[0]->slug;

	$parent = (int)$terms[1]->parent;

	if(isset($terms[1])&& $parent>=1){
		$cattmp = $cname;
		$cname = $cattmp.'－ '.$terms[1]->name;
	}
?>
          <div class="cat cat-<?= $cslug ?>"><?=$cname; ?></div>
          <h3 class="title"><?php the_title(); ?></h3>
        </div>

        <article>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
        <?php the_content(); ?>
<?php endwhile;endif; ?>
        </article>

        <div class="single-pager">
            <?php if (get_previous_post()):?><?php previous_post_link('%link','<div class="prev"><span>%title</span></div>'); ?>
            <?php else: ?>
            <div class="prev dummy"></div>
            <?php endif; ?>
            <a href="<?php echo home_url(); ?>/news/"><div class="center">記事一覧へ</div></a>
            <?php if (get_next_post()):?><?php next_post_link('%link','<div class="next"><span>%title</span></div>'); ?>
            <?php else: ?>
            <div class="next dummy"></div>
            <?php endif; ?>
        </div>
      </section>
    <?php include get_stylesheet_directory().'/sidebar.php'; ?>
    </div>

    </main>

  <?php include './footer.php'; ?>
</body>
</html>
