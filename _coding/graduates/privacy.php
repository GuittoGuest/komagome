<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <title>個人情報保護について｜駒込中学・高等学校</title>
    <meta content="駒込中学・高等学校" name="description">
    <meta http-equiv="Pragma" content="no-store">
    <meta http-equiv="Cache-Control" content="no-store">
    <meta http-equiv="Expires" content="0">
		<meta name="format-detection" content="telephone=no">

    <link href="../images/common/favicon.ico" rel="shortcut icon">
    <link href="../images/common/favicon.ico" rel="apple-touch-icon">
    <link href="../css/common.css" rel="stylesheet" type="text/css">
    <link href="../css/sub2.css" rel="stylesheet" type="text/css">

  </head>

  <body id="privacy">
    <?php include '../header.php'; ?>

    <main>
      <section class="mv header-title">
				<h1>個人情報保護について</h1>
        <p>Privacy Policy</p>
      </section>

			<section class="article-main">
        <article>
          <h2>本学園は、電話等によりお問合せいただきました、氏名、住所、電話番号等の個人情報をお問合せの対応・連絡のためにのみ利用いたします。</h2>
        </article>
			</section>

<section class="pagetop">
  <p>Page Top</p>
  <span class="arrow"></span>
</section>

    </main>

    <?php include '../footer.php'; ?>
    <script src="../js/jquery.min.js"></script>
    <script src="../js/flexibility.js"></script>
    <script src="../js/common2.js" type="text/javascript"></script>
  </body>
</html>
