<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <title>事務室｜駒込中学・高等学校</title>
    <meta content="駒込中学・高等学校" name="description">
    <meta http-equiv="Pragma" content="no-store">
    <meta http-equiv="Cache-Control" content="no-store">
    <meta http-equiv="Expires" content="0">
		<meta name="format-detection" content="telephone=no">

    <link href="../images/common/favicon.ico" rel="shortcut icon">
    <link href="../images/common/favicon.ico" rel="apple-touch-icon">
    <link href="../css/common.css" rel="stylesheet" type="text/css">
    <link href="../css/sub2.css" rel="stylesheet" type="text/css">

  </head>

  <body id="office">
    <?php include '../header.php'; ?>

    <main>
      <section class="mv header-title">
				<h1>事務室</h1>
        <p>Office</p>
      </section>

			<section class="article-main">
        <article>
          <h3>在校生の各種証明書などの発行について</h3>
        </article>
        <div class="two-column">
          <div class="box">
            <div class="text">
              <h4>窓口受付</h4>
              <dl class="flex_pc history wrap">
                <dt>交付取扱時間</dt>
                <dd class="flex_pc column2">
                  <p>月～金 8:00～15:30</p>
                  <p>土 8:00～13:00</p>
                </dd>
                <dt>申請取扱時間</dt>
                <dd class="flex_pc column2">
                  <p>月～金 8:00～13:00</p>
                  <p>(土曜受付なし)</p>
                </dd>
              </dl>
              <ul class="asterisk mt3">
                <li>日曜・祝祭日・学校の特別休日はお休みです。</li>
                <li>夏・冬・春休みは毎週月曜日のみ受付します。</li>
                <li>身分証明書・生徒手帳の再度交付は翌日発行。</li>
              </ul>
            </div>
          </div>
        </div>
        <table class="table_2b">
          <thead>
            <tr>
              <th>証明書種類</th>
              <th>料金(1通につき)</th>
              <th>交付所要日数</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th>調査書</th>
              <td>300円</td>
              <td>1週間</td>
            </tr>
            <tr>
              <th>推薦書</th>
              <td>無料</td>
              <td>1週間</td>
            </tr>
            <tr>
              <th>成績証明書(単位取得証明書)</th>
              <td>300円</td>
              <td>1週間</td>
            </tr>
            <tr>
              <th>卒業証明書</th>
              <td>200円</td>
              <td>1日</td>
            </tr>
            <tr>
              <th>卒業見込証明書</th>
              <td>200円</td>
              <td>1日</td>
            </tr>
            <tr>
              <th>転学書類一式</th>
              <td>500円</td>
              <td>1週間</td>
            </tr>
            <tr>
              <th>修了証明書</th>
              <td>200円</td>
              <td>1日</td>
            </tr>
            <tr>
              <th>在学証明書</th>
              <td>無料</td>
              <td>1日</td>
            </tr>
            <tr>
              <th>学割証明書</th>
              <td>無料</td>
              <td>1日</td>
            </tr>
          </tbody>
        </table>
      </section>

      <section class="article-main">
        <h3>卒業生の各種証明書などの発行について</h3>
        <div class="two-column">
          <div class="box">
            <div class="text">
              <h4>窓口で申込む場合</h4>
              <dl class="flex_pc history wrap">
                <dt>窓口受付時間</dt>
                <dd class="flex_pc column2">
                  <p>月～金 8:30～18:00</p>
                  <p>土 8:30～17:00</p>
                </dd>
              </dl>
              <ul class="asterisk mt3">
                <li>生徒登校日以外は月～土 8:30～16:30</li>
                <li>日曜・祝祭日・学校の特別休日はお休みです。</li>
              </ul>
            </div>
          </div>
          <div class="box">
            <div class="text">
              <h4>郵送で申込む場合</h4>
              <p>以下の3点を同封の上、証明書発行係あてに郵送してください。</p>
              <dl class="figure flex_pc history wrap mt3">
                <dt>申請書類(形式はありません。)</dt>
                <dd>氏名(ふりがな)、性別、生年月日、現住所、メールアドレス、電話番号、卒業年、必要な証明書の種類と数、提出先</dd>
                <dt>必要数の料金</dt>
                <dd>必要数の金額の定額小為替(郵便局で購入できます)</dd>
                <dt>返信用切手</dt>
                <dd>1通82円、2～3通92円、4～6通140円、7～10通205円、11～17通250円、18通～400円 ※速達の場合は別途280円</dd>
              </dl>
            </div>
          </div>
        </div>
        <table class="table_2b">
          <thead>
            <tr>
              <th>証明書種類</th>
              <th>料金(1通につき)</th>
              <th>交付所要日数</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th>調査書</th>
              <td>300円</td>
              <td>1週間</td>
            </tr>
            <tr>
              <th>推薦書</th>
              <td>無料</td>
              <td>1週間</td>
            </tr>
            <tr>
              <th>成績証明書(単位取得証明書)</th>
              <td>300円</td>
              <td>1週間</td>
            </tr>
            <tr>
              <th>卒業証明書</th>
              <td>200円</td>
              <td>即日</td>
            </tr>
            <tr>
              <th>その他の各種証明書</th>
              <td>200円</td>
              <td>適宜</td>
            </tr>
          </tbody>
        </table>
        <p class="asterisk">各英文証明書は1週間後の交付です。</p>
			</section>

<section class="pagetop">
  <p>Page Top</p>
  <span class="arrow"></span>
</section>

    </main>

    <?php include '../footer.php'; ?>
    <script src="../js/jquery.min.js"></script>
    <script src="../js/flexibility.js"></script>
    <script src="../js/common2.js" type="text/javascript"></script>
  </body>
</html>
