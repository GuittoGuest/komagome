<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <title>ICT教育｜駒込中学・高等学校</title>
    <meta content="駒込中学・高等学校" name="description">
    <meta http-equiv="Pragma" content="no-store">
    <meta http-equiv="Cache-Control" content="no-store">
    <meta http-equiv="Expires" content="0">
		<meta name="format-detection" content="telephone=no">

    <link href="../images/common/favicon.ico" rel="shortcut icon">
    <link href="../images/common/favicon.ico" rel="apple-touch-icon">
    <link href="../css/common.css" rel="stylesheet" type="text/css">
<?php //    <link href="../css/sub.css" rel="stylesheet" type="text/css"> ?>
    <link href="../css/sub2.css" rel="stylesheet" type="text/css">

  </head>

  <body id="ict">
    <?php include '../header.php'; ?>

    <main>
      <section class="mv header-title">
				<h1>ICT教育</h1>
        <p>ICT</p>
      </section>
      <section class="article-main">
        <article>
          <h2>主体的な学びを推進するICT活用</h2>
          <p class="mincho tac">教師が一方的に教えるという授業ではなく、生徒がICTを使いながら主体的に学び<br>
学力を伸ばすことがICT教育を進める大きな狙いです。<br>現代社会では主体的な活動や自分の考えを発砲できる力が求められているからです。<br>こうした力を日常の授業から育成することを目指しICT教育を行っています。</p>
        </article>
      </section>
			<section class="article-main">
        <div class="two-column">
          <div class="box">
            <div class="text">
              <h4>電子黒板とタブレット</h4>
              <p>教室には電子黒板を、そして生徒それぞれにタブレット端末を導入しました。それらを連携させたライブ感のある授業や、教材の画像や図形を自在に移動・回転させたりといった多角的なアプローチができることで、より授業の理解度は深まります。</p>
            </div>

            <div class="image">
              <img src="../images/education/ict1.jpg">
            </div>
          </div>
          <div class="box reverse">
            <div class="text">
              <h4>アクティブラーニング</h4>
              <p>アクティブラーニング型授業においてICT機器は欠かせません。ペアやグループでの学習・発表・共有が瞬時に、そして効率的に行うことができ、より発展的な活動が可能になるからです。もちろん従来の黒板を使った授業や理科実験、実技系のがk集が軽視されることは決してありません。</p>
            </div>

            <div class="image">
              <img src="../images/education/ict2.jpg">
            </div>
          </div>
          <div class="box">
            <div class="text">
              <h4>スタディアプリ</h4>
              <p>授業単元の復習や課題について、字タウでPCやタブレットで学習できるスタディサプリで学習できるスタディサプリを活用しています。課題の正答率などの詳細は、教員がデータとして全て把握できますので、なぜ間違えたのか、今後どのような点に注意すべきかなど、より効率的に生徒一人ひとりに指導することができます。</p>
            </div>

            <div class="image">
              <img src="../images/education/ict3.jpg">
            </div>
          </div>
        </div>

        <div class="voice">
          <div class="two-column">
            <div class="box">
              <div class="media_image">
                <img src="../images/education/ict4.jpg">
              </div>
              <div class="media">
                <p class="see"><a href="#movie1" class="modal">See Movie1</a></p>
                <p class="see"><a href="#movie2" class="modal">See Movie2</a></p>
              </div>
            </div>
          </div>
        </div>
			</section>

<section class="pagetop">
  <p>Page Top</p>
  <span class="arrow"></span>
</section>

    </main>

    <?php include '../footer.php'; ?>
    <script src="../js/jquery.min.js"></script>
    <script src="../js/flexibility.js"></script>
    <script src="../js/common2.js" type="text/javascript"></script>

    <div class="modal_contents">
      <!- modal表示内容2（離れた場所に置くバージョン） -->
      <div id="movie1"><video src="../images/education/ict_mv1.mp4"></video><p>Movie</p></div>
      <div id="movie2"><video src="../images/education/ict_mv2.mp4"></video><p>Movie</p></div>
      <!- /modal表示内容2（離れた場所に置くバージョン） -->
    </div>

  </body>
  </body>
</html>
