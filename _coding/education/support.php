<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <title>学習支援｜駒込中学・高等学校</title>
    <meta content="駒込中学・高等学校" name="description">
    <meta http-equiv="Pragma" content="no-store">
    <meta http-equiv="Cache-Control" content="no-store">
    <meta http-equiv="Expires" content="0">
		<meta name="format-detection" content="telephone=no">

    <link href="../images/common/favicon.ico" rel="shortcut icon">
    <link href="../images/common/favicon.ico" rel="apple-touch-icon">
    <link href="../css/common.css" rel="stylesheet" type="text/css">
<?php //    <link href="../css/sub.css" rel="stylesheet" type="text/css"> ?>
    <link href="../css/sub2.css" rel="stylesheet" type="text/css">

  </head>

  <body id="supports">
    <?php include '../header.php'; ?>

    <main>
      <section class="mv header-title">
				<h1>学習支援</h1>
        <p>Supports</p>
      </section>

			<section class="article-main">
				<article>
					<h2>生徒の将来を熟慮して適切にアドバイスする進路指導</h2>
					<p>生徒との個別面談や保護者への説明会。<br>そして、大学や社会に出ている卒業生や、各大学の関係者にも来校していただいて<br class="pc">お話を伺う機会を設けながら、段階的かつ具体的に進路を明確にしていきます。<br>また、生徒のモチベーションをより高めるために内部進学した先輩の話を聞くなど、<br class="pc">短期達成目標を明らかにしながら、 最終的な長期目標を達成していく後押しを行います。</p>
<div class="sub-menu">
<a href="#junior-high-school">中学</a>
<a href="#hight-school">高校</a>
</div>


        </article>
      </section>
      <section class="article-main" id="junior-high-school">
        <div class="wrp">
          <h3>中学</span></h3>
          <div class="two-column">

            <div class="box">
              <div class="text">
                <h4>先輩の話を聞く会と授業アンケート</h4>
                <p>6カ年一貫校のメリットを生かして、内進生（高1）の先輩から中学時代の歩みと現況を聞く会を設けています。教員も授業アンケートで生徒の声を聞き、自らの授業の改善を試みて、生徒の学習支援をしていきます。</p>
              </div>
              <div class="image">
                <img src="../images/education/support1.jpg">
              </div>
            </div>

            <div class="box reverse">
              <div class="text">
                <h4>“Step by Step”</h4>
                <p>進路実現に向けて生活を自分で管理する力も身につけてほしい能力です。本校ではオリジナルの生活と学習の記録帳“Step by Step”と“Classi”を利用し、学習内容、宿題、読んだ本などを意識的に記録させています。また、学校と家庭で点検し学習が定着する生活へと導きます。</p>
              </div>
              <div class="image">
                <img src="../images/education/support2.jpg">
              </div>
            </div>
          </div>
        </div>
      </section>
      <section class="article-main" id="hight-school">
        <div class="wrp">
          <h3 id="high-school">高校</h3>
          <div class="two-column">
            <div class="box">
              <div class="text">
                <h4>卒業生の話を聞く会</h4>
                <p>誰よりも身近な「先輩」の体験談を聞くことで、先輩達も今の生徒と同じ悩みや不安があったことを知ります。そして、その解決と目標達成へのプロセスを学んで、安心と自信を得ることが出来ます。</p>
              </div>
              <div class="image">
                <img src="../images/education/support3.jpg">
              </div>
            </div>

            <div class="box reverse">
              <div class="text">
                <h4>職業を聞く会</h4>
                <p>自分の将来の１つの到達点としての社会人の方の話を早期に聞くことは、社会に対する視野を大きく広げることに役立ちます。同時に、そのプロセスの一つとなる大学進学の意義を明確にして、目指すべき方向性をはっきりさせます。</p>
              </div>
              <div class="image">
                <img src="../images/education/support4.jpg">
              </div>
            </div>
          </div>
        </div>
      </section>
      <section class="article-main mt10">
        <article>
          <h2>フォローアップ体制</h2>
          <p>「中高6カ年、学内の学習活動のみで進路実現への学習を完結したい」<br class="pc">「学力の推移を理解している本校教員から指導を受けたい」<br class="pc">そんな生徒の思いに応える授業外学習を豊富に用意しています。<br>高レベルの特別講習会や指名補習の他、個別に自宅学習を指導することなどが日常化しているので、<br class="pc">生徒たちにとっても安心感を得られる大きな強みになっています。</p>
        </article>
      </section>

      <section>
        <div class="wrp">
          <div class="two-column">
            <div class="box">
              <div class="follow flex_pc wrap column3">
                <div>
                  <h4>特別講習会</h4>
                  <p>国・数・英の3教科について、希望制で授業進度の枠をこえて上級レベルの学習指導を行います。</p>
                  <img src="../images/education/support5.jpg">
                </div>
                <div>
                  <h4>指名補習</h4>
                  <p>定期テスト結果から振り返りが必要な場合、国・数・英を中心に、基礎学力の確認・向上を目指して指導します。</p>
                  <img src="../images/education/support6.jpg">
                </div>
                <div>
                  <h4>夏期講習会</h4>
                  <p>中学生全体対象に、夏休み中の5日間を利用して、講習会を行います。一学期で学習した内容を再確認します。</p>
                  <img src="../images/education/support7.jpg">
                </div>
                <div>
                  <h4>学習サポートシステム</h4>
                  <p>毎日の放課後（平日）16:00～18:30・土曜13:00～17:00）に、英国数の講義・自主学習のサポートを実施します。</p>
                  <img src="../images/education/support8.jpg">
                </div>
                <div>
                  <h4>スタディサプリ</h4>
                  <p>自宅でもタブレットやPCでの学習ができるスタディサプリを導入し、日々の授業の振り返りや基礎学力向上に役立てています。</p>
                  <img src="../images/education/support9.jpg">
                </div>
            </div>

          </div>
        </div>

			</section>



<section class="pagetop">
  <p>Page Top</p>
  <span class="arrow"></span>
</section>


    </main>

    <?php include '../footer.php'; ?>
    <script src="../js/jquery.min.js"></script>
    <script src="../js/flexibility.js"></script>
    <script src="../js/common2.js" type="text/javascript"></script>
  </body>
</html>
