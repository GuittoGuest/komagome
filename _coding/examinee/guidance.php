<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <title>入試案内｜駒込中学・高等学校</title>
    <meta content="駒込中学・高等学校" name="description">
    <meta http-equiv="Pragma" content="no-store">
    <meta http-equiv="Cache-Control" content="no-store">
    <meta http-equiv="Expires" content="0">
		<meta name="format-detection" content="telephone=no">

    <link href="../images/common/favicon.ico" rel="shortcut icon">
    <link href="../images/common/favicon.ico" rel="apple-touch-icon">
    <link href="../css/common.css" rel="stylesheet" type="text/css">
<?php //    <link href="../css/sub.css" rel="stylesheet" type="text/css"> ?>
    <link href="../css/sub2.css" rel="stylesheet" type="text/css">

  </head>

  <body id="admissions">
    <?php include '../header.php'; ?>

    <main>
      <section class="mv header-title">
				<h1>入試案内</h1>
        <p>Admissions</p>
      </section>

			<section class="article-main" id="junior-high-school">
				<article>
					<h2>中学入試説明会</h2>
					<p>すべての説明会は約１ケ月前より予約を開始いたします。<br>説明会終了後、毎回個別相談会があります（希望者）<br>※説明会日程などは４月現在のものであり、変更になる場合があります。</p>
          <div class="sub-menu">
            <a href="#junior-high-school">中学</a>
            <a href="#high-school">高校</a>
          </div>
        </article>
      </section>
      <section class="article-main">
        <div class="wrap">
          <h3>個別相談会<span>11/9、11/16、12/7以外予約不要</span></h3>
          <div class="two-column">
            <div class="box">
              <div class="text">
                <dl class="figure_circle_w flex_pc history wrap">
                  <dt>11月2日（土）</dt><dd>9:00～15:00</dd>
                  <dt>11月9日（土）</dt><dd>9:00～11:00 【※要予約 先着100名】</dd>
                  <dt>11月16日（土）</dt><dd>9:00～11:00 【※要予約 先着100名】</dd>
                  <dt>11月23日（土・祝）</dt><dd>9:00～15:00</dd>
                  <dt>11月30日（土）</dt><dd>9:00～15:00</dd>
                  <dt>12月7日（土）</dt><dd>9:00～11:00　 【※要予約 先着100名】</dd>
                </dl>
              </div>
            </div>
          </div>
          <h3>文化祭入試個別相談会<span>予約不要</span></h3>
          <div class="two-column">
            <div class="box">
              <div class="text">
                <dl class="figure_circle_w flex_pc history wrap">
                  <dt>9月21日（土）</dt><dd>10:30～15:00</dd>
                  <dt>9月22日（日）</dt><dd>9:00～15:00</dd>
                </dl>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section class="article-main mt10" id="high-school">
        <article>
          <h2>高校入試説明会</h2>
          <p>すべての説明会は約１ケ月前より予約を開始いたします。<br>説明会終了後、毎回個別相談会があります（希望者）<br>※説明会日程などは４月現在のものであり、変更になる場合があります。</p>
          <div class="sub-menu">
            <a href="#junior-high-school">中学</a>
            <a href="#high-school">高校</a>
          </div>
        </article>
      </section>
      <section class="article-main">
        <div class="wrap">
          <h3>個別相談会<span>11/9、11/16、12/7以外予約不要</span></h3>
          <div class="two-column">
            <div class="box">
              <div class="text">
                <dl class="figure_circle_w flex_pc history wrap">
                  <dt>11月2日（土）</dt><dd>9:00～15:00</dd>
                  <dt>11月9日（土）</dt><dd>9:00～11:00 【※要予約 先着100名】</dd>
                  <dt>11月16日（土）</dt><dd>9:00～11:00 【※要予約 先着100名】</dd>
                  <dt>11月23日（土・祝）</dt><dd>9:00～15:00</dd>
                  <dt>11月30日（土）</dt><dd>9:00～15:00</dd>
                  <dt>12月7日（土）</dt><dd>9:00～11:00　 【※要予約 先着100名】</dd>
                </dl>
              </div>
            </div>
          </div>
          <h3>文化祭入試個別相談会<span>予約不要</span></h3>
          <div class="two-column">
            <div class="box">
              <div class="text">
                <dl class="figure_circle_w flex_pc history wrap">
                  <dt>9月21日（土）</dt><dd>10:30～15:00</dd>
                  <dt>9月22日（日）</dt><dd>9:00～15:00</dd>
                </dl>
              </div>
            </div>
          </div>
        </div>
      </section>

<section class="pagetop">
  <p>Page Top</p>
  <span class="arrow"></span>
</section>

    </main>

    <?php include '../footer.php'; ?>
    <script src="../js/jquery.min.js"></script>
    <script src="../js/flexibility.js"></script>
    <script src="../js/common2.js" type="text/javascript"></script>
  </body>
</html>
