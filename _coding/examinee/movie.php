<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <title>学園紹介MOVIE｜駒込中学・高等学校</title>
    <meta content="駒込中学・高等学校" name="description">
    <meta http-equiv="Pragma" content="no-store">
    <meta http-equiv="Cache-Control" content="no-store">
    <meta http-equiv="Expires" content="0">
		<meta name="format-detection" content="telephone=no">

    <link href="../images/common/favicon.ico" rel="shortcut icon">
    <link href="../images/common/favicon.ico" rel="apple-touch-icon">
    <link href="../css/common.css" rel="stylesheet" type="text/css">
<?php //    <link href="../css/sub.css" rel="stylesheet" type="text/css"> ?>
    <link href="../css/sub2.css" rel="stylesheet" type="text/css">
    <style>
      .two-column{
        margin-bottom: 50px;
      }
      .two-column .title{
        background: #fff;
        padding: 0 10px;
      }
    </style>
  </head>

  <body class="movie">
    <?php include '../header.php'; ?>

    <main>
      <section class="mv header-title">
				<h1>駒込中高チャンネル</h1>
        <p>Movie</p>
      </section>

			<section class="article-main">
        <div class="voice">
<?php if( have_rows('movie_field',48) ): ?>
<?php $counta = 0; ?>
<?php while ( have_rows('movie_field', 48) ) : the_row(); ?>
<?php $counta++; ?>
          <div class="two-column">
            <div class="box">
              <div class="media_image">
                <?php $thumb = get_sub_field('thumb');?>
                <?php if($thumb):?>
                <img src="<?php the_sub_field('thumb');?>">
                <?php else: ?>
                <img src="../images/examinee/movie1.png">
                <?php endif;?>
              </div>
              <div class="media">
                <p class="title"><?php the_sub_field('title');?></p>
                <p class="see"><a href="#movie<?=$counta;?>" class="modal">Watch the Movie</a></p>
              </div>
            </div>
          </div>
<?php endwhile; ?>
<?php endif; ?>
        </div>
 <?php /*       <div class="flex_pc column2 tac">
          <div>
            <h3>中学校</h3>
            <iframe src="https://www.youtube.com/embed/djDcVqkLw5M?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen="" w=""></iframe>
          </div>
          <div>
            <h3>高校</h3>
            <iframe src="https://www.youtube.com/embed/et0X-UIafVI?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen=""></iframe>
          </div>
        </div>*/ ?>
			</section>

<section class="pagetop">
  <p>Page Top</p>
  <span class="arrow"></span>
</section>

    </main>

    <?php include '../footer.php'; ?>
    <script src="../js/jquery.min.js"></script>
    <script src="../js/flexibility.js"></script>
    <script src="../js/common2.js" type="text/javascript"></script>

    <div class="modal_contents">
      <!--- modal表示内容2（離れた場所に置くバージョン） ---->
      <?php if( have_rows('movie_field',48) ): ?>
      <?php $countb = 0; ?>
      <?php while ( have_rows('movie_field', 48) ) : the_row(); ?>
      <?php
        $countb++;
      ?>
      <div id="movie<?=$countb;?>"><video src="<?php the_sub_field('movie');?>" controls muted></video></video><p><?php the_sub_field('title');?></p></div>
      <?php endwhile;?>
      <?php endif; ?>
      <!--- /modal表示内容2（離れた場所に置くバージョン） ---->
    </div>
  </body>
</html>
