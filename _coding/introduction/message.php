<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <title>校長挨拶｜駒込中学・高等学校</title>
    <meta content="駒込中学・高等学校" name="description">
    <meta http-equiv="Pragma" content="no-store">
    <meta http-equiv="Cache-Control" content="no-store">
    <meta http-equiv="Expires" content="0">
		<meta name="format-detection" content="telephone=no">

    <link href="../images/common/favicon.ico" rel="shortcut icon">
    <link href="../images/common/favicon.ico" rel="apple-touch-icon">
    <link href="../css/common.css" rel="stylesheet" type="text/css">
<?php //    <link href="../css/sub.css" rel="stylesheet" type="text/css"> ?>
    <link href="../css/sub2.css" rel="stylesheet" type="text/css">

  </head>

  <body>
    <?php include '../header.php'; ?>

    <main>
      <section class="mv header-title">
				<h1>校長挨拶</h1>
        <p>Greeting</p>
      </section>

      <section class="article-main">
        <article>
          <h2>年頭にあたってのご挨拶と御礼</h2>
        </article>
      </section>

			<section class="article-main">

        <div class="two-column">
          <div class="box">
            <div class="text">
              <h4>冬来りなば春遠からじ！</h4>
              <p>旧年中は関係各位より多くのご指導ご鞭撻を賜り、心より御礼申し上げます。12月15日より東京私立高校の「事前相談」が解禁となり、多くの公立中学の先生方が受験生名簿をもって来校されています。お陰様で120名募集の推薦入試に、まだ途中段階ですが、1400余名の受験志願者をいただいております。これもひとえに有縁の皆様方のお力添えのお陰です。心より御礼申し上げます。</p>
            </div>
            <p class="image"><img src="../images/about/principal.jpg"></p>
          </div>

          <div class="box reverse">
            <div class="text">
              <h4>「高大接続」から「高大一貫化」へ</h4>
              <p>高校と大学の「高大接続改革」が姿を見せ始めました。この「接続」とは「一体化」と同義であり「一貫化」に繋がります。即ち、「中高一貫教育」と同じで「高大一貫教育」がスタートしています。</p>
              <p>東京農工大学は「高大接続教室」を立ち上げ、全国から30名の高校生を募集しこのほど合否を発表しました。本校の高二の生徒も応募し合格をもらって来ております。この生徒は大学が主催するIGSプログラムの合宿に参加し、農学・工学・その融合分野の研究課題に取り組んでいきます。実験や実習のレポート、グループワークで学んだことをポートフォリオシステムを用いて、今後の進路に活用していきます。</p>
              <p>政府文部科学省は11月22日、「高大一貫制度」の立ち上げを告示しました。「接続」は「一貫化」の布石でありました。この「高大一貫制度」に参加する学校の募集は12月から開始しています。3月には決定校を公表し、2019年度から大学に理数系科目の得意な高校生の進学枠を設けて「高大連携・一貫化」を開始します。参加できる大学は理数系教育に実績のある大学で、その地域の高校から「５校前後が連名」で応募する方式をとります。分かりやすく言えば、その地域の「進学実績校５校」の「理系に秀でた生徒」を大学が集めて大学の講義の履修を高校在学中から与えていくというものです。農工大の方式と同じです。大学には３千万円の予算がつきます。高大接続改革は他方で大学の再編成を生み出しています。ＡＩによる完全自動化が全産業分野で始まり、「大テクノ失業時代」がまもなく到来することに対応し、大学が全国民対象の再教育機関に変身するからです。AP・DP・CPの開示が大学に義務付けられ大学はいま「学部名称変更」の「ラッシュ」です。中央大学は情報の仕組みだけでなくＩＴ技術と法律とグローバル教育をセットにした「国際情報学部」を新設します。青山学院大学はコミュニティ活動支援とコミュニティ創生計画を中心に、地域で活躍する人材育成を図る「コミュニティ人間学部』を創設します。立命館大学はオールイングリッシュの「グローバル教養学部」を新設し、オーストラリア国立大学と提携し単位完全互換の教育プログラムを展開します。東京外国語大学は日本人学生45人と外国人学生30人で構成する「国際日本学部」を立ち上げ「文化的衝突を恐れない国際人の育成」に入ります。もはや「一大学・一固有学部名称」の時代となって来ているのです。大学の使命が、「社会人枠」で40歳・50歳の国民を受け入れてICT時代のスキルを再教育する時代に入るからです。これまでの一括的名称の「文学部」や「経済学部」の時代では無くなっているのです。</p>
              <p>本校は教育特区校として、中学入試で「STEM入試」や「自己表現入試」や「PISA型・適性検査型入試」など多様性のある入試を導入しましたが、すべては時代変革の流れを読み取った上での先行投資型改革です。着実に生徒の未来保障の出来る学校づくりを展開してまいります。ご鞭撻の程宜しくお願い申し上げます。</p>
              <p>蛇足ではありますが下記に民法改正のもたらす時代変化の特色を記載しておきました。ご一読いただければ幸いです。</p>
            </div>
          </div>

          <div class="box">
            <div class="text">
              <h4>時代変化の一大局面―民法改正と教育改革</h4>
              <p>本邦の民法はこれまでの「大陸法」から「英米法」に「大改正」されました。すでに今年の3月13日に閣議決定され、7月6日に国会で改正法が成立し、7月13日に公布されています。この民法改正は主として「相続法の改正」であって教育改革とは一見無関係に見えますが「法の持つ理念」がドラスティックに変わったと言う点で注意が必要です。過去の判例のビッグデーターを持ったAIが司法の場を左右しかねない時代がはじまるからです。</p>
              <p>米国では、トラブルのあった時、謝った方が裁判で負けます。この価値観が新たな英米法の基本です。即ち、訴訟を起こされた側が賠償責任を負う時代・「訴訟社会」が来ているということです。学校も企業も危機管理体制の具体的運用手段を持たないとわずかな瑕疵で致命的打撃を受けかねない時代となっています。</p>
              <p>教育組織として先ず第一に改革すべきは「ガバナンスの確立」にあります！そして、行うべきは「理念の構築」であり、発信すべきは魅力ある『未来像』です。そして、改革の第一は「自助」であり、次が「共助」であり、最後が「公助」です。もし、これを転倒させるなら、それはもはや私学ではなく公立でしょう。私学は「民間」の立場に立ち切って自校のレーゾンデートルを受験市場に力強く打ち出していくべき時代です。</p>
              <p>本校は今年も「昨年」を上回る多くの受験生の親子に来ていただいています。中学入試では昨年の2.5倍の受験生に来ていただいています。「サンデー毎日」では「塾の勧める私学ナンバーワン」に本校を選んでいただきました。しかし、奢ることなく謙虚に精進を重ねてまいります。「冬来たりなば　春遠からじ」と申します。新春の明るい光の中にやわらかな若芽が芽吹いています。子供たちの明日は未来です。今年も努力を重ねてしっかりとがんばってまいります！ご指導ご鞭撻の程宜しくお願い申し上げます。</p>
            </div>
          </div>
        </div>

        <p class="tar">合掌</p>
			</section>


<section class="pagetop">
  <p>Page Top</p>
  <span class="arrow"></span>
</section>

    </main>

    <?php include '../footer.php'; ?>
    <script src="../js/jquery.min.js"></script>
    <script src="../js/flexibility.js"></script>
    <script src="../js/common2.js" type="text/javascript"></script>
  </body>
</html>
