<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <title>保健室｜駒込中学・高等学校</title>
    <meta content="駒込中学・高等学校" name="description">
    <meta http-equiv="Pragma" content="no-store">
    <meta http-equiv="Cache-Control" content="no-store">
    <meta http-equiv="Expires" content="0">
		<meta name="format-detection" content="telephone=no">

    <link href="../images/common/favicon.ico" rel="shortcut icon">
    <link href="../images/common/favicon.ico" rel="apple-touch-icon">
    <link href="../css/common.css" rel="stylesheet" type="text/css">
    <link href="../css/sub2.css" rel="stylesheet" type="text/css">

  </head>

  <body>
    <?php include '../header.php'; ?>

    <main>
      <section class="mv title">
        <h1>保健室</h1>
        <p>Dispensary</p>
      </section>

      <section class="article-main">
        <article>
          <h3>学校伝染症（インフルエンザ等）の発生に伴う対応について</h3>
        </article>
      </section>

      <section class="article-main">
        <div class="two-column">
          <div class="box">
            <div class="text">
              <h4>伝染病（インフルエンザ等）と診断された場合は、速やかに学校に報告してください。</h4>
              <p>生徒が感染症と医師より診断された場合、本人の安静と集団への感染・流行を防ぐため、出席停止（欠席扱いとしない）となります。医師より診断された内容を担任に連絡し、医師の許可があるまでご家庭で安静にしてください。</p>
              <p>なお、治癒後の登校については必ず医師の診断を受け、【学校伝染病罹患報告書・登校許可証明書】を医療機関で記入していただき、登校許可をもらってから登校してください。【学校伝染病罹患報告書・登校許可証明書】担任まで提出してください。</p>
              <h5><a href="data/permit.pdf" download="【登校許可証】ダウンロード.pdf">【登校許可証】ダウンロード</a></h5>
              <p class="asterisk">許可証は学校でも配布しています。</p>
            </div>
          </div>
        </div>
        <div class="two-column">
          <div class="box">
            <div class="text">
              <h4>日常生活におけるインフルエンザ予防策</h4>
              <ul class="disc">
                <li>手洗い・うがいをこまめに行ってください。</li>
                <li>「咳エチケット」を心がけてください。</li>
                <li>インフルエンザ様症状がある場合は、マスクを使用してください。</li>
                <li>体調の悪いときは体温計で計測してください。</li>
                <li>登校前の時点で、３７．５℃以上の発熱,インフルエンザ様症状がありましたら、
                登校を控え、医療機関に受診し、適切な治療を受けてください。</li>
              </ul>
            </div>
          </div>
        </div>
        <h3>状況においては、学校長より緊急で学級閉鎖・学年閉鎖・休校措置をとる場合がございますので、ご了承下さい。 </h3>
        <div class="two-column">
          <div class="box">
            <div class="text">
              <h4>日本スポーツ振興センター災害給付金について</h4>
              <h5><a href="data/accident-benefit.pdf" download="日本スポーツ振興センター災害給付金について.pdf">日本スポーツ振興センター災害給付金について</a></h5>
              <ul class="flex_pc column2 wrap">
                <li><a href="data/accident-benefit01.pdf" download="災害報告書.pdf">災害報告書（PDF）</a></li>
                <li><a href="data/accident-benefit02.pdf" download="病院用「医療等の状況」別紙３(１).pdf">病院用「医療等の状況」別紙３(１)</a></li>
                <li><a href="data/accident-benefit03.pdf" download="整骨院用「医療等の状況」別紙３(３).pdf">整骨院用「医療等の状況」別紙３(３)</a></li>
                <li><a href="data/accident-benefit04.pdf" download="調剤報酬明細書.pdf">調剤報酬明細書</a></li>
                <li><a href="data/accident-benefit05.pdf" download="治療用装具明細書.pdf">治療用装具明細書</a></li>
                <li><a href="data/accident-benefit06.pdf" download="高額療養状況の届.pdf">高額療養状況の届</a></li>
              </ul>
            </div>
          </div>
        </div>
      </section>

<section class="pagetop">
  <p>Page Top</p>
  <span class="arrow"></span>
</section>

    </main>

    <?php include '../footer.php'; ?>
    <script src="../js/jquery.min.js"></script>
    <script src="../js/flexibility.js"></script>
    <script src="../js/common2.js" type="text/javascript"></script>
  </body>
</html>