<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <title>緊急連絡｜駒込中学・高等学校</title>
    <meta content="駒込中学・高等学校" name="description">
    <meta http-equiv="Pragma" content="no-store">
    <meta http-equiv="Cache-Control" content="no-store">
    <meta http-equiv="Expires" content="0">
		<meta name="format-detection" content="telephone=no">

    <link href="../images/common/favicon.ico" rel="shortcut icon">
    <link href="../images/common/favicon.ico" rel="apple-touch-icon">
    <link href="../css/common.css" rel="stylesheet" type="text/css">
    <link href="../css/sub2.css" rel="stylesheet" type="text/css">

  </head>

  <body>
    <?php include '../header.php'; ?>

    <main>
      <section class="mv header-title">
				<h1>緊急連絡</h1>
        <p>Emergency</p>
      </section>

			<section class="article-main">
        <article>
          <h2>現在、緊急連絡はありません</h2>
        </article>
      </section>

      <section class="article-main">
        <div class="flex_pc column2 tac">
          <h4><a href="https://ks.faircast.jp/" target="_blank">FairCast（学校連絡網サービス）</a></h4>
          <h4><a href="safety.pdf" target="_blank">災害時の対応について</a></h4>
        </div>
			</section>

<section class="pagetop">
  <p>Page Top</p>
  <span class="arrow"></span>
</section>

    </main>

    <?php include '../footer.php'; ?>
    <script src="../js/jquery.min.js"></script>
    <script src="../js/flexibility.js"></script>
    <script src="../js/common2.js" type="text/javascript"></script>
  </body>
</html>
