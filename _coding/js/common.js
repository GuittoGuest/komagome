$(document).ready(function(){
  /* IE9FLEX対応*/
  var userAgent = window.navigator.userAgent.toLowerCase();
  if( userAgent.match(/(msie|MSIE)/)) {
    var isIE = true;
    var ieVersion = userAgent.match(/((msie|MSIE)\s|rv:)([\d\.]+)/)[3];
    ieVersion = parseInt(ieVersion);
    if(ieVersion<10){
      $(function(){
        flexibility(document.documentElement);
      });
    }
  }
});
/*　アンカーリンクアニメーション（同一ページ） */
$('area[href^="#"],a[href^="#"]').click(function(){
	var speed = 500;
	var href= $(this).attr("href");
	var target = $(href == "#" || href == "" ? 'html' : href);
	var position = target.offset().top-200;
	$("html, body").animate({scrollTop:position}, speed, "swing");
	return false;
});

/* gotopボタン */
$('.pagetop').click(function(){
  var speed = 500;

  $("html, body").animate({scrollTop:0}, speed, "swing");
  return false;
});

/* spnav */
$('.sp-btn').click(function(){
  $('.lower').addClass('-open');
});
$('.close').click(function(){
  $('.lower').removeClass('-open');
});

$('.lower .block').click(function(){
	$('.lower .block').removeClass('-current');
  $(this).addClass('-current');
});

$(window).scroll(function(){
  var scrollValue = $(window).scrollTop();
  if(scrollValue > 500){
    $('.gotop').addClass('visible');
    $('.conversion').removeClass('hide');
  } else {
    $('.gotop').removeClass('visible');
    $('.conversion').addClass('hide');
  }
});

$('.document').on('click',function(){
    $('.modal').addClass('-active');
    $('#document-modal').addClass('-current');
  });
  $('.modal').on('click',function(){
		var selStr = document.getSelection();
		if(selStr.focusOffset == 0){
  		$('.modal').removeClass('-active');
  		$('.modal div').removeClass('-current');
		}
  });

//ニュース系ページサイドバー
	$('aside .pulldown').on('click',function(){
	  $(this).next('.sub').slideToggle();
		$(this).toggleClass('-open');
	});
