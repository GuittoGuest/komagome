<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <title>進路データ｜駒込中学・高等学校</title>
    <meta content="駒込中学・高等学校" name="description">
    <meta http-equiv="Pragma" content="no-store">
    <meta http-equiv="Cache-Control" content="no-store">
    <meta http-equiv="Expires" content="0">
		<meta name="format-detection" content="telephone=no">

    <link href="../images/common/favicon.ico" rel="shortcut icon">
    <link href="../images/common/favicon.ico" rel="apple-touch-icon">
    <link href="../css/common.css" rel="stylesheet" type="text/css">
<?php //    <link href="../css/sub.css" rel="stylesheet" type="text/css"> ?>
    <link href="../css/sub2.css" rel="stylesheet" type="text/css">

  </head>

  <body id="carrier">
    <?php include '../header.php'; ?>

    <main>
      <section class="mv header-title">
				<h1>進路データ</h1>
        <p>Career</p>
      </section>

			<section class="article-main">
				<article>
					<h2>国公立大学をはじめとした難関大学へのチャレンジと達成</h2>
					<p>ここ数年、生徒の3年間をかけた学習成果が大きく花開くようになっています。<br>大学全入時代と言われる時代だからこそ、妥協しない進路選択をする生徒が多く、<br>高1から具体的な目標を抱いて努力を重ねています。<br>進路実績は教員と生徒がお互いに授業の質を高めていく過程で生まれます。</p>
        </article>
      </section>

      <section class="article-main">
        <div class="wrap tac">
          <h3>2019年度 主な大学合格者数</span></h3>
          <h5>GMARCH以上205名合格(国公立+早慶上理ICU+GMARCH)</h5>
          <div class="two-column">
            <div class="box">
              <div class="text">
                <div class="flex_pc column3 middle">
                  <h4>国公立大学 27名</h4>
                  <p class="grow2">東京外国語大学1名東京学芸大学1名 千葉大学2名 神戸大学1名 東京農工大学1名  防衛大学校1名 茨城大学1名 宇都宮大学1名 横浜市立大学1名 他</p>
                </div>
              </div>
            </div>

            <div class="box">
              <div class="text">
                <div class="flex_pc column3 middle">
                  <h4>早慶上理ICU 47名</h4>
                  <p class="grow2">慶應義塾大学6名 早稲田大学11名 上智大学9名 国際基督教大学2名 東京理科大学19名</p>
                </div>
              </div>
            </div>

            <div class="box">
              <div class="text">
                <div class="flex_pc column3 middle">
                  <h4>GMARCH 131名</h4>
                  <p class="grow2">明治大学32名 青山学院大学9名 立教大学17名 中央大学30名 法政大学32名 学習院大学11名</p>
                </div>
              </div>
            </div>

            <div class="box">
              <div class="text">
                <div class="flex_pc column3 middle">
                  <h4>関関同立24</h4>
                  <p class="grow2">関西学院大学6 関西大学6 立命館大学12</p>
                </div>
              </div>
            </div>

            <div class="box">
              <div class="text">
                <div class="flex_pc column3 middle">
                  <h4>成成明学獨國武 93名</h4>
                  <p class="grow2">成蹊大学10名 成城大学8名 明治学院大学11名 獨協大学34名 國學院大学17名 武蔵大学13名</p>
                </div>
              </div>
            </div>

            <div class="box">
              <div class="text">
                <div class="flex_pc column3 middle">
                  <h4>日東駒専 133名</h4>
                  <p class="grow2">日本大学45名 東洋大学49名 駒澤大学20名 専修大学19名</p>
                </div>
              </div>
            </div>
          </div>
        </div>
			</section>


<section class="pagetop">
  <p>Page Top</p>
  <span class="arrow"></span>
</section>

    </main>

    <?php include '../footer.php'; ?>
    <script src="../js/jquery.min.js"></script>
    <script src="../js/flexibility.js"></script>
    <script src="../js/common.js" type="text/javascript"></script>
    <script>
    $(document).ready(function(){
      /* タブレット用ビューポート */
      $(function(){
        var ua = navigator.userAgent;
        if((ua.indexOf('iPhone') > 0) || ua.indexOf('iPod') > 0 || (ua.indexOf('Android') > 0 && ua.indexOf('Mobile') > 0)){
            $('head').prepend('<meta name="viewport" content="width=device-width,initial-scale=1">');
        } else {
            $('head').prepend('<meta name="viewport" content="width=1320">');
        }
      });
    });
    </script>
  </body>
</html>
