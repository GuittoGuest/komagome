<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <title>学校行事｜駒込中学・高等学校</title>
    <meta content="駒込中学・高等学校" name="description">
    <meta http-equiv="Pragma" content="no-store">
    <meta http-equiv="Cache-Control" content="no-store">
    <meta http-equiv="Expires" content="0">
		<meta name="format-detection" content="telephone=no">

    <link href="../images/common/favicon.ico" rel="shortcut icon">
    <link href="../images/common/favicon.ico" rel="apple-touch-icon">
    <link href="../css/common.css" rel="stylesheet" type="text/css">
<?php //    <link href="../css/sub.css" rel="stylesheet" type="text/css"> ?>
    <link href="../css/sub2.css" rel="stylesheet" type="text/css">

  </head>

  <body id="events">
    <?php include '../header.php'; ?>

    <main>
      <section class="mv header-title">
				<h1>学校行事</h1>
        <p>Events</p>
      </section>

			<section class="article-main">

  <div class="two-column">
    <div class="box">
      <div class="text">
<dl class="flex_pc wrap history">
  <dt><h3>4月</h3></dt>
  <dd>
    <div class="flex_pc column2">
      <ul class="disc grow2">
        <li>入学式</li>
        <li>灌仏祭（花まつり）</li>
        <li>オリエンテーション</li>
        <li>新入生歓迎会</li>
      </ul>
      <p class="image"><img src="../images/school-life/event4.jpg"></p>
    </div>
  </dd>
</dd>
  <dt><h3>5月</h3></dt>
  <dd>
    <div class="flex_pc column2">
      <ul class="disc grow2">
        <li>了翁会</li>
        <li>中間テスト</li>
        <li>校外学習</li>
        <li>実力テスト</li>
        <li>授業参観</li>
      </ul>
      <p class="image"><img src="../images/dammy.jpg"></p>
    </div>
  </dd>
  <dt><h3>6月</h3></dt>
  <dd>
    <div class="flex_pc column2">
      <ul class="disc grow2">
        <li>山家会</li>
        <li>教育懇親会</li>
        <li>体育祭</li>
      </ul>
      <p class="image"><img src="../images/school-life/event6.jpg"></p>
    </div>
  </dd>
  <dt><h3>7月</h3></dt>
  <dd>
    <div class="flex_pc column2">
      <ul class="disc grow2">
        <li>期末テスト</li>
        <li>林間学校</li>
      </ul>
      <p class="image"><img src="../images/school-life/event7.jpg"></p>
    </div>
  </dd>
  <dt><h3>8月</h3></dt>
  <dd>
    <div class="flex_pc column2">
      <ul class="disc grow2">
        <li>クラブ合宿</li>
        <li>ハワイセミナー</li>
        <li>学習合宿</li>
        <li>夏期講習</li>
      </ul>
      <p class="image"><img src="../images/school-life/event8.jpg"></p>
    </div>
  </dd>
  <dt><h3>9月</h3></dt>
  <dd>
    <div class="flex_pc column2">
      <ul class="disc grow2">
        <li>彼岸会</li>
        <li>実力テスト</li>
        <li>玉蘭祭（文化祭）</li>
      </ul>
      <p class="image"><img src="../images/school-life/event9.jpg"></p>
    </div>
  </dd>
  <dt><h3>10月</h3></dt>
  <dd>
    <div class="flex_pc column2">
      <ul class="disc grow2">
        <li>中間テスト</li>
        <li>修学旅行</li>
        <li>日光山研修</li>
      </ul>
      <p class="image"><img src="../images/school-life/event10.jpg"></p>
    </div>
  </dd>
  <dt><h3>11月</h3></dt>
  <dd>
    <div class="flex_pc column2">
      <ul class="disc grow2">
        <li>教育懇親会</li>
        <li>合唱コンクール</li>
        <li>レシテーションコンテスト</li>
        <li>霜月会</li>
      </ul>
      <p class="image"><img src="../images/school-life/event11.jpg"></p>
    </div>
  </dd>
  <dt><h3>12月</h3></dt>
  <dd>
    <div class="flex_pc column2">
      <ul class="disc grow2">
        <li>成道会</li>
        <li>期末テスト</li>
        <li>冬季スポーツ教室</li>
        <li>視聴覚行事（芸術鑑賞）</li>
        <li>直前講習</li>
      </ul>
      <p class="image"><img src="../images/dammy.jpg"></p>
    </div>
  </dd>
  <dt><h3>1月</h3></dt>
  <dd>
    <div class="flex_pc column2">
      <ul class="disc grow2">
        <li>漢字検定</li>
        <li>スピーチコンテスト</li>
      </ul>
      <p class="image"><img src="../images/dammy.jpg"></p>
    </div>
  </dd>
  <dt><h3>2月</h3></dt>
  <dd>
    <div class="flex_pc column2">
      <ul class="disc grow2">
        <li>涅槃会</li>
        <li>中学入試</li>
        <li>実力テスト</li>
      </ul>
      <p class="image"><img src="../images/dammy.jpg"></p>
    </div>
  </dd>
  <dt><h3>3月</h3></dt>
  <dd>
    <div class="flex_pc column2">
      <ul class="disc grow2">
        <li>期末テスト</li>
        <li>修了式</li>
        <li>セブ島語学研修</li>
      </ul>
      <p class="image"><img src="../images/school-life/event3.jpg"></p>
    </div>
  </dd>
</dl>
              </div>
            </div>
          </div>
			</section>


<section class="pagetop">
  <p>Page Top</p>
  <span class="arrow"></span>
</section>

    </main>

    <?php include '../footer.php'; ?>
    <script src="../js/jquery.min.js"></script>
    <script src="../js/flexibility.js"></script>
    <script src="../js/common2.js" type="text/javascript"></script>
  </body>
</html>
