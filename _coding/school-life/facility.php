<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <title>施設紹介｜駒込中学・高等学校</title>
    <meta content="駒込中学・高等学校" name="description">
    <meta http-equiv="Pragma" content="no-store">
    <meta http-equiv="Cache-Control" content="no-store">
    <meta http-equiv="Expires" content="0">
    <meta name="format-detection" content="telephone=no">

    <link href="../images/common/favicon.ico" rel="shortcut icon">
    <link href="../images/common/favicon.ico" rel="apple-touch-icon">
    <link href="../css/common.css" rel="stylesheet" type="text/css">
<?php //    <link href="../css/sub.css" rel="stylesheet" type="text/css"> ?>
    <link href="../css/sub2.css" rel="stylesheet" type="text/css">

  </head>

  <body id="facilities">
    <?php include '../header.php'; ?>

    <main>
      <section class="mv header-title">
        <h1>施設紹介</h1>
        <p>Facilities</p>
      </section>

      <section class="article-main">
        <article>
          <h2>充実の施設環境で送る、豊かな駒込ライフ。</h2>
          <p>自然光が取り入れられるよう設計されたこだわりの校舎。<br>開放感のあるスペースはいつも光あふれ、集う生徒たちの声も自然と明るくなります。<br>リラックスできる環境だからこそ、学習意欲も高まります。</p>
        </article>
      </section>
      <section class="article-main">
        <div class="flex view">
          <p class="image">
            <img src="../images/about/view.jpg" alt="">
            <svg xmlns="http://www.w3.org/2000/svg" width="512" height="366" viewBox="0 0 512 366">
              <defs>
              </defs>
              <path data-name="sankun" class="cls-1" d="M291,251l3,10,28-2,1-10,2-2V236Z"/>
              <path data-name="shikan" class="cls-1" d="M326,248c0,7.333.417,13,7,13s6-7.333,6-13-3.833-9-7-9S326,242,326,248Z"/>
              <path data-name="hall-b" class="cls-1" d="M334,214l-18-40-58,29v16l18,39,59-30Z"/>
              <path data-name="study" class="cls-1" d="M241,204l-17,7-2,7,8,16,22-9Z"/>
              <path data-name="gym" class="cls-1" d="M211,97l-35,16,5,12,35-17Z"/>
              <path data-name="ground" class="cls-1" d="M334,166l-83,40-12-22,6-39-13-28,48-19,9,18,71-23,9,20-48,23Z"/>
            </svg>
          </p>
          <ul class="tltle">
            <li class="sankun">サンクンガーデン</li>
            <li class="kpura">Ｋプラザ</li>
            <li class="ground">グラウンド</li>
            <li class="music">音楽室</li>
            <li class="hall-b">勧学ホール</li>
            <li class="study">自習室</li>
            <li class="shikan">止観堂</li>
            <li class="tea">茶室</li>
            <li class="library">図書室</li>
            <li class="gym">体育館</li>
          </ul>
        </div>
      </section>

<!--Toggle navigation-->

<section class="pagetop">
  <p>Page Top</p>
  <span class="arrow"></span>
</section>


    </main>

    <?php include '../footer.php'; ?>
    <script src="../js/jquery.min.js"></script>
    <script src="../js/flexibility.js"></script>
    <script src="../js/common2.js" type="text/javascript"></script>
    <script>
      window.onload = function() {
        $("<div class=\"subwin\">").prependTo('.view');
      }
      $("svg path").hover(
        function(){
          var $name = $(this).attr("data-name");
          var $data= $("#" + $name).html();
          $(this).addClass("-hover");
          $(".subwin").addClass("-modal1");
          $(".subwin").html($data);
    //      alert($data)
        },
        function(){
          $(this).removeClass("-hover");
          $(".subwin").removeClass("-modal1");
          $(".subwin").html("");
        }
      );
      $(".tltle li").hover(
        function(){
          var $name = $(this).attr("class");
          var $data= $("#" + $name).html();
          $(".subwin").addClass("-modal2");
          $(".subwin").html($data);
        },
        function(){
          $(".subwin").removeClass("-modal2");
          $(".subwin").html("");
        }
      );
    </script>
<div class="modal_contents">
  <div id="sankun">
    <h5>サンクンガーデン</h5>
    <p>Kプラザとつながる日だまりの庭。<br>晴れた日の昼休みは自然と人が集まります。</p>
    <p class="image"><img src="../images/about/sankun.jpg" alt=""></p>
  </div>
  <div id="kpura">
    <h5>Ｋプラザ</h5>
    <p>サンクンガーデンに面した憩いの空間。<br>食堂としても活用され、生徒たちが楽しく過ごしています。</p>
    <p class="image"><img src="../images/about/kpura.jpg" alt=""></p>
  </div>
  <div id="ground">
    <h5>グラウンド</h5>
    <p>太陽の光溢れる、（ロングパイル）人工芝のグラウンド。<br>体育の授業や放課後のクラブ活動で活用します。</p>
    <p class="image"><img src="../images/about/ground.jpg" alt=""></p>
  </div>
  <div id="music">
    <h5>音楽室</h5>
    <p>最上階に位置する音楽室。<br>快適空間で爽やかな音色を奏でられます。</p>
    <p class="image"><img src="../images/about/music.jpg" alt=""></p>
  </div>
  <div id="hall-b">
    <h5>勧学ホール</h5>
    <p>電動で階段式の椅子を配置・収納できます。<br>学校行事、演劇鑑賞など多目的で活用します。</p>
    <p class="image"><img src="../images/about/hall-b.jpg" alt=""></p>
  </div>
  <div id="study">
    <h5>自習室</h5>
    <p>静かなブースで集中力アップ。<br>卒業生チューターが学習を支えます。</p>
    <p class="image"><img src="../images/about/study.jpg" alt=""></p>
  </div>
  <div id="shikan">
    <h5>止観堂</h5>
    <p>自分自身と対峙するために用意された静かな空間です。<br>天井からの陽射しが十一面観音菩薩像を照らします。</p>
    <p class="image"><img src="../images/about/shikan.jpg" alt=""></p>
  </div>
  <div id="tea">
    <h5>茶室</h5>
    <p>侘びさびを感じる茶室です。<br>茶道部が裏千家でお手前のお稽古をしています。</p>
    <p class="image"><img src="../images/about/tea.jpg" alt=""></p>
  </div>
  <div id="library">
    <h5>図書室</h5>
    <p>35,000冊（開架は25,000冊）の蔵書数を誇る図書室。<br>学習の参考になる資料に溢れています。</p>
    <p class="image"><img src="../images/about/library.jpg" alt=""></p>
  </div>
  <div id="gym">
    <h5>体育館</h5>
    <p>バスケットコートが2面とれる広いフロア。<br>体育の授業も充実します。</p>
    <p class="image"><img src="../images/about/gym.jpg" alt=""></p>
  </div>
</div>
  </body>
</html>
