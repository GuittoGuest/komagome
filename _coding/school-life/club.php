<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <title>クラブ活動｜駒込中学・高等学校</title>
    <meta content="駒込中学・高等学校" name="description">
    <meta http-equiv="Pragma" content="no-store">
    <meta http-equiv="Cache-Control" content="no-store">
    <meta http-equiv="Expires" content="0">
		<meta name="format-detection" content="telephone=no">

    <link href="../images/common/favicon.ico" rel="shortcut icon">
    <link href="../images/common/favicon.ico" rel="apple-touch-icon">
    <link href="../css/common.css" rel="stylesheet" type="text/css">
<?php //    <link href="../css/sub.css" rel="stylesheet" type="text/css"> ?>
    <link href="../css/sub2.css" rel="stylesheet" type="text/css">
    <link href="../owl/owl.carousel.min.css" rel="stylesheet" type="text/css">

  </head>

  <body id="club">
    <?php include '../header.php'; ?>

    <main>
		<section class="mv header-title">
		<h1>クラブ活動</h1>
		<p>Clubs</p>
		</section>

		<section class="article-main">
			<article>
				<h2>努力の先にあるのは、勝利だけじゃない。<br>仲間と喜怒哀楽を分かち合いながら<br>自分らしいフィールドで輝く。</h2>
				<div class="sub-menu mt5"><a href="#sports">運動部</a><a href="#culture">文化部</a></div>
			</article>
		</section>

<section class="article-main" id="sports">
  <div class="club-column">
    <h3>運動部</h3>
	<div class="box">
		<div class="text">
			<h4>高校野球部</h4>
			<p>都ベスト8進出も果たし、甲子園を夢に勇ましくさわやかに活動しています。</p>
		</div>
		<img src="../images/school-life/sports1.png">
		<span>
			目指すは甲子園出場。<br>全員が一丸と<br>
			なって正々堂々と<br>勝負します！
		</span>
	</div>

	<div class="box">
		<div class="text">
  			<h4>中学野球部</h4>
  			<p>一人ひとりが熱い気持ちを常に持ち、チーム一丸となった全員野球をモットーに一瞬の
  輝きを求めて活動しています。</p>
		</div>
		<img src="../images/school-life/sports2.png">
		<span>
			ダミーダミーダミーダミーダミーダミー<br>ダミーダミーダミー<br>
			ダミーダミーダミーダミーダミーダミー
		</span>
	</div>

	<div class="box">
		<div class="text">
  			<h4>柔道部</h4>
  			<p>
  				部員は非常に仲良く、先輩はとても面倒見がいいです。是非一緒に楽しく練習し、強くなりましょう！
  			</p>
      	</div>
		<img src="../images/school-life/sports3.png">
			<span>
				ダミーダミーダミーダミーダミーダミー<br>ダミーダミーダミー<br>
				ダミーダミーダミーダミーダミーダミー
			</span>
		</div>

		<div class="box">
      	<div class="text">
			  <h4>剣道部</h4>
  			<p>
  				経験者だけでなく初心者も大歓迎。剣道に興味のある人は地下1階の勧学ホールを覗いてください。
  			</p>
      	</div>
		<img src="../images/school-life/sports4.png">
			<span>
				ダミーダミーダミーダミーダミーダミー<br>ダミーダミーダミー<br>
				ダミーダミーダミーダミーダミーダミー
			</span>
		</div>

		<div class="box">
      	<div class="text">
  			<h4>陸上部</h4>
  			<p>
  				個々人を尊重した団結に価値があります。実力上昇中。
  			</p>
      	</div>
		<img src="../images/school-life/sports5.png">
			<span>
				ダミーダミーダミーダミーダミーダミー<br>ダミーダミーダミー<br>
				ダミーダミーダミーダミーダミーダミー
			</span>
		</div>

		<div class="box">
      	<div class="text">
  			<h4>卓球部</h4>
  			<p>
  				卓球が好きな人なら誰でも入部OK。一度練習を見に来てください。
  			</p>
      	</div>
		<img src="../images/school-life/sports6.png">
			<span>
				ダミーダミーダミーダミーダミーダミー<br>ダミーダミーダミー<br>
				ダミーダミーダミーダミーダミーダミー
			</span>
		</div>

		<div class="box">
      	<div class="text">
  			<h4>バスケットボール部</h4>
  			<p>
  				コーチの指導の下、公式戦に向けて一つでも多く勝ち進むことが出来るよう練習しています。
  			</p>
      	</div>
		<img src="../images/school-life/sports7.png">
			<span>
				ダミーダミーダミーダミーダミーダミー<br>ダミーダミーダミー<br>
				ダミーダミーダミーダミーダミーダミー
			</span>
		</div>

		<div class="box">
      	<div class="text">
  			<h4>バレーボール部</h4>
  			<p>
  				少人数ですが日々の練習を重ね、大会で勝てるよう頑張っています。
  			</p>
      	</div>
		<img src="../images/school-life/sports8.png">
			<span>
				ダミーダミーダミーダミーダミーダミー<br>ダミーダミーダミー<br>
				ダミーダミーダミーダミーダミーダミー
			</span>
		</div>

		<div class="box">
      	<div class="text">
  			<h4>公式テニス部</h4>
  			<p>
  				経験者はもちろん、初心者でも丁寧に教えます。ぜひ、一緒に汗を流しましょう。
  			</p>
      	</div>
		<img src="../images/school-life/sports9.png">
			<span>
				ダミーダミーダミーダミーダミーダミー<br>ダミーダミーダミー<br>
				ダミーダミーダミーダミーダミーダミー
			</span>
		</div>

		<div class="box">
      	<div class="text">
  			<h4>登山・スキー部</h4>
  			<p>
  				毎月の日帰り登山と夏や秋の縦走を通して、自然とふれあい自己と向きあい友情の絆を
  深めます。
  			</p>
      	</div>
		<img src="../images/school-life/sports10.png">
			<span>
				ダミーダミーダミーダミーダミーダミー<br>ダミーダミーダミー<br>
				ダミーダミーダミーダミーダミーダミー
			</span>
		</div>

		<div class="box">
      	<div class="text">
  			<h4>バドミントン部</h4>
  			<p>
  技術を獲得する歓びと、協力し合って活動する歓びを感じられます。
  			</p>
      	</div>
		<img src="../images/school-life/sports11.png">
			<span>
				ダミーダミーダミーダミーダミーダミー<br>ダミーダミーダミー<br>
				ダミーダミーダミーダミーダミーダミー
			</span>
		</div>

		<div class="box">
      	<div class="text">
  			<h4>アメリカン<br>フットボール部</h4>
  			<p>
  都内でも珍しいアメフト部。格闘スポーツで心身を鍛えてみては。
  			</p>
      	</div>
		<img src="../images/school-life/sports12.png">
			<span>
				ダミーダミーダミーダミーダミーダミー<br>ダミーダミーダミー<br>
				ダミーダミーダミーダミーダミーダミー
			</span>
		</div>

		<div class="box">
      	<div class="text">
  			<h4>高校サッカー部</h4>
  			<p>
  初心者でも大丈夫。チームワークを学び共に楽しみましょう。
  			</p>
      	</div>
		<img src="../images/school-life/sports13.png">
			<span>
				ダミーダミーダミーダミーダミーダミー<br>ダミーダミーダミー<br>
				ダミーダミーダミーダミーダミーダミー
			</span>
		</div>

		<div class="box">
      <div class="text">
  			<h4>中学サッカー部</h4>
  			<p>
  				地区大会優勝を目標にし、支部大会や都大会出場を果たす。【一生懸命】【不撓不屈】
  の精神で頑張っています。未経験者も歓迎します。
  			</p>
      </div>
		<img src="../images/school-life/sports14.png">
			<span>
				ダミーダミーダミーダミーダミーダミー<br>ダミーダミーダミー<br>
				ダミーダミーダミーダミーダミーダミー
			</span>
		</div>

		<div class="box">
      <div class="text">
  			<h4>ラグビー部</h4>
  			<p>
  スポーツ未経験者や運動に自信がない人、勇気や仲間を作りたい人など大歓迎です。
  			</p>
      </div>
		<img src="../images/school-life/sports15.png">
			<span>
				ダミーダミーダミーダミーダミーダミー<br>ダミーダミーダミー<br>
				ダミーダミーダミーダミーダミーダミー
			</span>
		</div>

		<div class="box">
      <div class="text">
  			<h4>ダンス同好会</h4>
  			<p>
  ダミー文です。ダミー文です。ダミー文です。ダミー文です。ダミー文です。
  ダミー文です。ダミー文です。ダミー文です。ダミー文です。ダミー文です。
  ダミー文です。ダミー文です。ダミー文です。
  			</p>
      </div>
		<img src="../images/school-life/sports16.png">
			<span>
				ダミーダミーダミーダミーダミーダミー<br>ダミーダミーダミー<br>
				ダミーダミーダミーダミーダミーダミー
			</span>
		</div>

		<div class="box">
      <div class="text">
  			<h4>軟式野球同好会</h4>
  			<p>
  ダミー文です。ダミー文です。ダミー文です。ダミー文です。ダミー文です。
  ダミー文です。ダミー文です。ダミー文です。ダミー文です。ダミー文です。
  ダミー文です。ダミー文です。ダミー文です。
  			</p>
      </div>
			<img src="../images/school-life/clubdummy.png">
			<span>
				ダミーダミーダミーダミーダミーダミー<br>ダミーダミーダミー<br>
				ダミーダミーダミーダミーダミーダミー
			</span>
		</div>
  </div>
</section>

<section class="article-main" id="culture">
  <div class="club-column">
  <h3>文化部</h3>
		<div class="box">
      <div class="text">
  			<h4>吹奏楽部</h4>
  			<p>
  				コンサートやコンクールに向け、日々練習を重ねています。
  音楽好きな人、共に演奏しよう！
  			</p>
      </div>
			<img src="../images/school-life/culture1.png">
			<span>
				ダミーダミーダミーダミーダミーダミー<br>
				ダミーダミーダミーダミーダミーダミー
			</span>
		</div>

		<div class="box"><div class="text">
			<h4>演劇部</h4>
			<p>
スポットライトを浴びたい人も、音響・照明や台本つくりに興味のある人も、
みんな舞台に集まれ！
			</p></div>
			<img src="../images/school-life/culture2.png">
			<span>
				ダミーダミーダミーダミーダミーダミー<br>
				ダミーダミーダミーダミーダミーダミー
			</span>
		</div>

		<div class="box"><div class="text">
			<h4>書道部</h4>
			<p>
全国レベルの展覧会に多数出品、校内・校外で書道パフォーマンスも行っています。
初心者でも大丈夫です。
			</p></div>
			<img src="../images/school-life/culture3.png">
			<span>
				ダミーダミーダミーダミーダミーダミー<br>
				ダミーダミーダミーダミーダミーダミー
			</span>
		</div>

		<div class="box"><div class="text">
			<h4>美術部</h4>
			<p>
想像力を広げ、作品作りを楽しむ場です。絵画やデザイン、立体作品などを作っています。
			</p></div>
			<img src="../images/school-life/culture4.png">
			<span>
				ダミーダミーダミーダミーダミーダミー<br>
				ダミーダミーダミーダミーダミーダミー
			</span>
		</div>

		<div class="box"><div class="text">
			<h4>華道部</h4>
			<p>
花と向き合うことで、自分の内面を見つめることができます。
心が安らぎます。
			</p></div>
			<img src="../images/school-life/culture5.png">
			<span>
				ダミーダミーダミーダミーダミーダミー<br>
				ダミーダミーダミーダミーダミーダミー
			</span>
		</div>

		<div class="box"><div class="text">
			<h4>パソコン部</h4>
			<p>
タイピング検定やゲーム作成、動画編集を通してPCに詳しくなれます。
			</p></div>
			<img src="../images/school-life/culture6.png">
			<span>
				ダミーダミーダミーダミーダミーダミー<br>
				ダミーダミーダミーダミーダミーダミー
			</span>
		</div>

		<div class="box"><div class="text">
			<h4>自然科学部</h4>
			<p>
目標はありません。ここにあるのは、抑えきれない衝動を持った探究心です。
			</p></div>
			<img src="../images/school-life/culture7.png">
			<span>
				ダミーダミーダミーダミーダミーダミー<br>
				ダミーダミーダミーダミーダミーダミー
			</span>
		</div>

		<div class="box"><div class="text">
			<h4>英語部</h4>
			<p>
英語は世界共通語です。さらに関心を高め、楽しい時間を作りましょう。
			</p></div>
			<img src="../images/school-life/culture8.png">
			<span>
				ダミーダミーダミーダミーダミーダミー<br>
				ダミーダミーダミーダミーダミーダミー
			</span>
		</div>

		<div class="box"><div class="text">
			<h4>漫画研究部</h4>
			<p>
イラストや漫画を描いた部誌を発行しています。文化祭では自分たちの絵を
アクリルキィホルダーや缶バッチにして加工して販売したりしています。
			</p></div>
			<img src="../images/school-life/culture9.png">
			<span>
				ダミーダミーダミーダミーダミーダミー<br>
				ダミーダミーダミーダミーダミーダミー
			</span>
		</div>

		<div class="box"><div class="text">
			<h4>和太鼓部</h4>
			<p>
部員が心を一つにして太鼓を打ち込む時、聴いている人が感動する時、心はうち震えるのです。
			</p></div>
			<img src="../images/school-life/culture10.png">
			<span>
				ダミーダミーダミーダミーダミーダミー<br>
				ダミーダミーダミーダミーダミーダミー
			</span>
		</div>

		<div class="box"><div class="text">
			<h4>合唱部</h4>
			<p>
音楽、歌、ピアノが好きな人、一緒に合唱を楽しみましょう！初心者の方でも大丈夫ですよ♪
			</p></div>
			<img src="../images/school-life/culture11.png">
			<span>
				ダミーダミーダミーダミーダミーダミー<br>
				ダミーダミーダミーダミーダミーダミー
			</span>
		</div>

		<div class="box"><div class="text">
			<h4>ボランティア部</h4>
			<p>
ボランティア募集の区広報などで参加可能な活動に協力しています。
			</p></div>
			<img src="../images/school-life/culture12.png">
			<span>
				ダミーダミーダミーダミーダミーダミー<br>
				ダミーダミーダミーダミーダミーダミー
			</span>
		</div>

		<div class="box"><div class="text">
			<h4>仏教研究会</h4>
			<p>
他校には無いめずらしいクラブですが、誰でも入れます。
生活の中の仏教を文化祭でのテーマにしています。
夏は「秩父巡礼」の合宿があります。
			</p></div>
			<img src="../images/school-life/clubdummy.png">
			<span>
				ダミーダミーダミーダミーダミーダミー<br>
				ダミーダミーダミーダミーダミーダミー
			</span>
		</div>

		<div class="box"><div class="text">
			<h4>写真同好会</h4>
			<p>
ダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミー
			</p></div>
			<img src="../images/school-life/culture14.png">
			<span>
				ダミーダミーダミーダミーダミーダミー<br>
				ダミーダミーダミーダミーダミーダミー
			</span>
		</div>

		<div class="box"><div class="text">
			<h4>かるた同好会</h4>
			<p>
ダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミー
			</p></div>
			<img src="../images/school-life/clubdummy.png">
			<span>
				ダミーダミーダミーダミーダミーダミー<br>
				ダミーダミーダミーダミーダミーダミー
			</span>
		</div>

		<div class="box"><div class="text">
			<h4>家庭科クラブ</h4>
			<p>
ダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミー
			</p></div>
			<img src="../images/school-life/culture16.png">
			<span>
				ダミーダミーダミーダミーダミーダミー<br>
				ダミーダミーダミーダミーダミーダミー
			</span>
		</div>

  </div>
</section>


<section class="pagetop">
  <p>Page Top</p>
  <span class="arrow"></span>
</section>

    </main>

    <?php include '../footer.php'; ?>
    <script src="../js/jquery.min.js"></script>
    <script src="../js/flexibility.js"></script>
    <script src="../js/common2.js" type="text/javascript"></script>
    <script src="../owl/owl.carousel.min.js"></script>
    <script>
    	var $src;
		$(".club-column .box").hover(function() {
			$src = $(this).children("img").attr("src");
			$src2 = $src.split("\.png").join("a\.png");
			$(this).children("img").attr("src",$src2);
		}, function() {
			$(this).children("img").attr("src",$src);
		});
    </script>
  </body>
</html>
