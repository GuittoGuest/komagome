<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <title>制服紹介｜駒込中学・高等学校</title>
    <meta content="駒込中学・高等学校" name="description">
    <meta http-equiv="Pragma" content="no-store">
    <meta http-equiv="Cache-Control" content="no-store">
    <meta http-equiv="Expires" content="0">
		<meta name="format-detection" content="telephone=no">

    <link href="../images/common/favicon.ico" rel="shortcut icon">
    <link href="../images/common/favicon.ico" rel="apple-touch-icon">
    <link href="../css/common.css" rel="stylesheet" type="text/css">
<?php //    <link href="../css/sub.css" rel="stylesheet" type="text/css"> ?>
    <link href="../css/sub2.css" rel="stylesheet" type="text/css">

  </head>

  <body id="uniform">
    <?php include '../header.php'; ?>

    <main>
      <section class="mv header-title">
				<h1>制服紹介</h1>
        <p>Uniform</p>
      </section>

			<section class="article-main">
				<article>
					<h2>制服紹介</h2>
					<p>身につけるのは駒込の誇り。落ちついた紺色の学園制服。<br>ネクタイやリボンの結び目ひとつにも生徒の心のありようが映しだされます。<br>オーソドックスなブレザーのスタイルに表れる駒込学園の誇りを大切にした着こなしを指導していきます。</p>
          <div class="sub-menu">
            <a href="#junior-high-school">中学</a>
            <a href="#high-school">高校</a>
          </div>
        </article>
      </section>
      <section class="article-main">
          <h3 id="junior-high-school">中学校</h3>
          <div class="two-column">
            <div class="box flex_pc column3">
              <div class="text">
                <p>ダミー文がいれてあります。ダミー文がいれてあります。ダミー文がいれてあります。ダミー文がいれてあります。ダミー文がいれてあります。ダミー文がいれてあります。ダミー文がいれてあります。ダミー文がいれてあります。ダミー文がいれてあります。ダミー文がいれてあります。ダミー文がいれてあります。ダミー文がいれてあります。</p>
              </div>
              <p class="image grow2"><img src="../images/school-life/uniform1.png" alt=""></p>
            </div>
          </div>
          <h3 id="high-school">高等学校</h3>
          <div class="two-column">
            <div class="box flex_pc column3 reverse">
              <div class="text">
                <p>ダミー文がいれてあります。ダミー文がいれてあります。ダミー文がいれてあります。ダミー文がいれてあります。ダミー文がいれてあります。ダミー文がいれてあります。ダミー文がいれてあります。ダミー文がいれてあります。ダミー文がいれてあります。ダミー文がいれてあります。ダミー文がいれてあります。ダミー文がいれてあります。</p>
              </div>
              <p class="image grow2"><img src="../images/school-life/uniform2.png" alt=""></p>
            </div>
          </div>
        </div>
        <ol class="figure">
          <li>指定のニットセーターによる自由なコーディネートが可能です。</li>
          <li>オプションでポロシャツがあります。</li>
          <li>高校はブルーのチェック柄のワイシャツ、女子はリボンネクタイがあります。</li>
        </ol>
      </section>

<section class="pagetop">
  <p>Page Top</p>
  <span class="arrow"></span>
</section>


    </main>

    <?php include '../footer.php'; ?>
    <script src="../js/jquery.min.js"></script>
    <script src="../js/flexibility.js"></script>
    <script src="../js/common2.js" type="text/javascript"></script>
  </body>
</html>
